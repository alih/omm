/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "omm-classmap.h"
#include "omm-io-utils.h"
#include "omm-btree.h"

typedef struct
{
    char *text; // owned
    int label;
} Entry;

// This is a huge KLUGE.
// I keep getting warnings about const casts otherwise...
typedef struct
{
    const char *text;
    int label;
} ConstEntry;

static int entry_cmp(const void *p1, const void *p2)
{
    const Entry *e1 = (const Entry *) p1;
    const Entry *e2 = (const Entry *) p2;
    return strcmp(e1->text, e2->text);
}

struct OmmClassMap
{
    OmmBTree *btree;
    int count;
    char **texts;
};

OmmClassMap *omm_classmap_new()
{
    OmmClassMap *classmap = (OmmClassMap *) malloc(sizeof(OmmClassMap));
    classmap->btree = omm_btree_new(entry_cmp);
    classmap->count = 0;
    classmap->texts = NULL;
    return classmap;
}

static void entry_free(void *p, void *unused_data)
{
    (void) unused_data;
    free(((Entry *) p)->text);
    free(p);
}

void omm_classmap_free(OmmClassMap *classmap)
{
    omm_btree_foreach(classmap->btree, entry_free, NULL);
    omm_btree_free(classmap->btree);
    if (classmap->texts)
        free(classmap->texts);
    free(classmap);
}

// freeze {{{

typedef struct
{
    char **texts;
    int i;
    int n;
    bool sort;
} Fridge;

static void entry_assign_label(void *p, void *data)
{
    Entry *e = (Entry *) p;
    Fridge *f = (Fridge *) data;
    assert(f->i < f->n);
    if (f->sort)
        e->label = f->i;
    f->i++;
    f->texts[e->label] = e->text;
}

void omm_classmap_freeze(OmmClassMap *classmap, bool sort)
{
    int n = classmap->count;
    if (n)
    {
        classmap->texts = (char **) malloc(n * sizeof(char *));
        Fridge f = {.texts = classmap->texts, .i = 0, .n = n, .sort = sort};
        omm_btree_foreach(classmap->btree, entry_assign_label, &f);
    }
}

bool omm_classmap_is_frozen(OmmClassMap *classmap)
{
    return classmap->texts != NULL;
}

// freeze }}}

static void *entry_dup(void *p)
{
    Entry *e = (Entry *) p;
    Entry *result = (Entry *) malloc(sizeof(Entry));
    result->text = (char *) malloc(strlen(e->text) + 1);
    strcpy(result->text, e->text);
    result->label = e->label;
    return result;
}

void omm_classmap_add(OmmClassMap *classmap, const char *text)
{
    ConstEntry query;
    query.text = text;
    query.label = classmap->count;
    Entry *e = (Entry *) omm_btree_insert_dup(classmap->btree, &query, entry_dup);
    if (e->label == query.label)
        classmap->count++;
}


int omm_classmap_get_class(OmmClassMap *classmap, const char *text)
{
    assert(omm_classmap_is_frozen(classmap));
    ConstEntry query;
    query.text = text;
    Entry *e = (Entry *) omm_btree_find(classmap->btree, &query);
    if (!e)
        return -1;
    return e->label;
}


const char *omm_classmap_get_text(OmmClassMap *classmap, int label)
{
    assert(label >= 0 && label < classmap->count);
    return classmap->texts[label];
}

int omm_classmap_get_n_classes(OmmClassMap *classmap)
{
    return classmap->count;
}


// I/O {{{

// this is like isspace(), but locale-independent.
// (I don't want to mess with Unicode issues.)
static bool omm_isspace(char c)
{
    return c == '\r' || c == '\t' || c == '\n' 
        || c == '\f' || c == '\v' || c == ' ';
}

OmmClassMap *omm_classmap_load(const char *path)
{
    size_t size;
    char *content = omm_read_entire_file(path, &size);
    if (!content)
        return NULL;
    
    OmmClassMap *classmap = omm_classmap_new();
    char *p = content;
    // add all words
    while (*p)
    {
        while (omm_isspace(*p))
            p++;
        if (!*p)
            break;
        char *word_start = p;
        while (*p && !omm_isspace(*p))
            p++;
        char *word_end = p;
        char save = *word_end;
        *word_end = '\0';
        omm_classmap_add(classmap, word_start);
        *word_end = save;
    }
    omm_classmap_freeze(classmap, false);
    free(content);
    return classmap;
}

bool omm_classmap_save(OmmClassMap *classmap, const char *path)
{
    assert(omm_classmap_is_frozen(classmap));
    FILE *f = fopen(path, "w");
    if (!f)
    {
        perror(path);
        return false;
    }

    int n = omm_classmap_get_n_classes(classmap);
    for (int i = 0; i < n; i++)
        fprintf(f, "%s\n", omm_classmap_get_text(classmap, i));

    fclose(f);
    return true;
}

// I/O }}}

// vim: set foldmethod=marker:
