/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include <stdio.h>
#include <stdlib.h>
#include "omm-io-boxtiff.h"
#include "omm-path.h"

int main(int argc, const char **argv)
{
    if (argc <= 1)
    {
        fprintf(stderr, "Usage: %s boxtiff_dir\n", argv[0]);
        return EXIT_FAILURE;
    }

    int n_pairs;
    char **boxtiff_paths = omm_boxtiff_ls(argv[1], &n_pairs);
    if (n_pairs < 0)
        return EXIT_FAILURE;

    for (int i = 0; i < 2 * n_pairs; i++)
        printf("%s\n", boxtiff_paths[i]);

    omm_ls_free(boxtiff_paths, 2 * n_pairs);

    return EXIT_SUCCESS;
}
