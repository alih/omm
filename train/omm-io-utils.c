/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */


#include "omm-io-utils.h"

#include <stdio.h>
#include <stdlib.h>

static size_t omm_file_size(/*const char *restrict path,*/ FILE *restrict file)
{
    fpos_t pos;
    fgetpos(file, &pos);
    fseek(file, 0, SEEK_END);
    long size = ftell(file);
    fsetpos(file, &pos);
    return (size_t) size;
    /*struct stat s;
    if (fstat(fileno(file), &s))
    {
        perror(path);
        exit(EXIT_FAILURE);
    }

    return (size_t) s.st_size;*/
}

char *omm_read_entire_file(
    const char *restrict path, 
    size_t *restrict out_size)
{
    FILE *file = fopen(path, "r");
    if (!file)
    {
        perror(path);
        return NULL;
    }
    size_t size = *out_size = omm_file_size(/*path,*/ file) - ftell(file);
    char *p = (char *) malloc(size + 1);
    if (!p && size)
    {
        fclose(file);
        fprintf(stderr, "%s: Out of memory\n", path);
        return NULL;
    }

    size_t read = fread(p, 1, size, file);
    fclose(file);
    if (read != size) // is this even possible?
    {
        perror(path); // XXX: does it really work?
        free(p);
        return NULL;
    }
    p[size] = '\0';
    return p;
}

void omm_look_up_and_print(
    FILE *restrict file,
    const uint32_t *restrict offsets, 
    int n, 
    const char *restrict strpool)
{
    for (int i = 0; i < n; i++)
        fputs(strpool + offsets[i], file);
}
