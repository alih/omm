/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include <stdio.h>
#include <stdlib.h>

#ifdef HAVE_CAIRO
#include "omm-visual.h"
#include "omm-io-tiff.h"
#include "omm-labeling.h"
#include "omm-layout.h"
#include "omm-time.h"
#include "omm-kruskal.h"

int main(int argc, const char *const *argv)
{
    if (argc != 3)
    {
        fprintf(stderr, "Usage: %s input.tif output.png\n", argv[0]);
        return EXIT_FAILURE;
    }

    OmmImage *image = omm_tiff_read_first_page_rle(argv[1]);
    if (!image)
        return EXIT_FAILURE;
    
    printf("File read\n");
    omm_timestamp(stdout);
    uint32_t *labels;
    OmmBlobInfo *blobs;
    int n_blobs = omm_label_and_analyze_blobs(&labels, &blobs, image);
    const int min_height = 5; // XXX
    for (int i = 0; i < n_blobs; i++)
    {
        if (blobs[i].max_y - blobs[i].min_y >= min_height)
            blobs[i].index = i;
        else
            blobs[i].index = -1;
    }

    int16_t *coords = (int16_t *) malloc(2 * n_blobs * sizeof(int16_t));
    int n_points = 0;
    for (int i = 0; i < n_blobs; i++)
    {
        if (blobs[i].index < 0)
            continue;
        coords[2 * n_points]     = blobs[i].min_x + blobs[i].max_x;
        coords[2 * n_points + 1] = blobs[i].min_y + blobs[i].max_y;
        n_points++;
    }
    int *mst = omm_planar_kruskal(coords, n_points);
    printf("MST computed\n");
    omm_timestamp(stdout);
    omm_visualize_mst_to_png(argv[2], image, mst, coords, n_points);
    free(mst);
    /*OmmTriangle *triangles = omm_delaunay(coords, n_points);
    printf("Delaunay computed\n");
    omm_timestamp(stdout);
    omm_visualize_triangulation_to_png(argv[2], image, triangles, coords, n_points);
    free(triangles);*/

    /*
    OmmLayout *layout = omm_la_by_projection_profiles(blobs, n_blobs, image->width, image->height);
    printf("Layout computed\n");
    omm_timestamp(stdout);
    omm_visualize_layout_to_png(argv[2], image, layout);
    omm_layout_free(layout);*/
    
    free(labels);
    free(blobs);
    omm_image_free(image);
    return EXIT_SUCCESS;
}

#else

int main(int argc, const char *const *argv)
{
    (void) argc;
    fprintf(stderr, "Sorry, %s needs to be built with Cairo\n", argv[0]);
    return EXIT_FAILURE;
}

#endif
