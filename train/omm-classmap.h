/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#ifndef OMM_CLASSMAP_H
#define OMM_CLASSMAP_H

#include <stdbool.h>

typedef struct OmmClassMap OmmClassMap;

OmmClassMap *omm_classmap_new(void);
void omm_classmap_add(OmmClassMap *, const char *);
void omm_classmap_freeze(OmmClassMap *, bool sort);
bool omm_classmap_is_frozen(OmmClassMap *);
void omm_classmap_free(OmmClassMap *);
int omm_classmap_get_n_classes(OmmClassMap *);
const char *omm_classmap_get_text(OmmClassMap *cm, int label);
int omm_classmap_get_class(OmmClassMap *cm, const char *text);
OmmClassMap *omm_classmap_load(const char *path);
bool omm_classmap_save(OmmClassMap *classmap, const char *path);


#endif
