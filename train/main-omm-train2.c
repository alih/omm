/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include <assert.h>
#include <ctype.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "omm-endian.h"
#include "omm-io-model.h"
#include "omm-classmap.h"
#include "omm-options.h"

static void print_usage_and_exit(const char *this_prog)
{
    fprintf(stderr, "Usage:\n");
    fprintf(stderr, "    %s [options] liblinear_model\n", this_prog);
    fprintf(stderr, "Options:\n");
    fprintf(stderr, "    -c classmap_file or --classmap classmap_file\n");
    fprintf(stderr, "    -o output_file or --output output_file\n");
    fprintf(stderr, "    -b or --big-endian\n");
    fprintf(stderr, "    -l or --little-endian\n");
    exit(EXIT_FAILURE);
}

// liblinear model loading {{{

static void skip_whitespace(FILE *f)
{
    while (true)
    {
        int c = fgetc(f);
        if (c == EOF)
            return;
        if (!isspace(c))
        {
            ungetc(c, f);
            return;
        }
    }
}

static void skip_word(FILE *f, const char *word)
{
    skip_whitespace(f); 
    for (const char *p = word; *p; p++)
    {
        int c = fgetc(f);
        if (c != *p)
        {
            fprintf(stderr, "liblinear model: '%s' expected\n", word);
            exit(EXIT_FAILURE);
        }
    }
}


static double *liblinear_model_load(const char *path, int *out_n_classes, int *out_dimension)
{
    FILE *f = fopen(path, "r");
    if (!f)
    {
        perror(path);
        exit(EXIT_FAILURE);
    }

    int n_classes;
    skip_word(f, "solver_type");
    skip_word(f, "MCSVM_CS");

    if (fscanf(f, " nr_class %d", &n_classes) != 1)
    {
        fprintf(stderr, "Couldn't read the number of classes from a liblinear model.\n");
        exit(EXIT_FAILURE);
    }
    skip_word(f, "label");
    int *labels = (int *) malloc(n_classes * sizeof(int));
    for (int i = 0; i < n_classes; i++)
    {
        if (fscanf(f, " %d", &labels[i]) != 1)
        {
            fprintf(stderr, "Unable to read the labels\n");
            exit(EXIT_FAILURE);
        }
    }

    int dim;
    if (fscanf(f, " nr_feature %d", &dim) != 1)
    {
        fprintf(stderr, "Unable to read dimension\n");
        exit(EXIT_FAILURE);
    }

    double bias;
    fscanf(f, " bias %lf", &bias);
    if (bias >= 0)
    {
        fprintf(stderr, "Bias is not supported\n");
        exit(EXIT_FAILURE);
    }

    skip_word(f, "w");
    double *w = (double *) malloc(n_classes * dim * sizeof(double));

    for (int i = 0; i < n_classes * dim; i++)
    {
        if (fscanf(f, " %lf", &w[i]) != 1)
        {
            fprintf(stderr, "Unable to read weights from a liblinear model\n");
            exit(EXIT_FAILURE);
        }
    }
    fclose(f);

    double *weights = (double *) malloc(n_classes * dim * sizeof(double));
    for (int i = 0; i < n_classes; i++)
    {
        int label = labels[i];
        for (int j = 0; j < dim; j++)
            weights[label * dim + j] = w[j * n_classes + i];
    }
    free(w);
    free(labels);

    *out_n_classes = n_classes;
    *out_dimension = dim;
    return weights;
}
// liblinear model loading }}} 

static double max_abs(double *a, int n)
{
    double m = 0;
    for (int i = 0; i < n; i++)
    {
        double x = fabs(a[i]);
        if (x > m)
            m = x;
    }
    return m;
}

/* static double dot_int(double *a, int *b, int n)
{
    double result = 0;
    for (int i = 0; i < n; i++)
        result += a[i] * b[i];
    return result;
}*/

int main(int argc, char *argv[])
{
    const char *input_path = NULL;
    const char *classmap_path = NULL;
    const char *output_path = NULL;
    int n_classes;
    int dim;
    OmmEndianness endianness = omm_endianness();
 
 
    // option parsing 
    bool no_more_options = false;
    for (int i = 1; i < argc; i++)
    {
        const char *opt;
        if (!no_more_options && argv[i][0] == '-' && argv[i][1]) // starts with '-', but is not "-"
        {
            if (!strcmp(argv[i], "--"))
                no_more_options = true;
            else if (!strcmp(argv[i], "-b") || !strcmp(argv[i], "--big-endian"))
                endianness = OMM_BIG_ENDIAN;
            else if (!strcmp(argv[i], "-l") || !strcmp(argv[i], "--little-endian"))
                endianness = OMM_LITTLE_ENDIAN;
            else if ((opt = omm_parse_string_option2(argc, argv, &i, "-o", "--output")))
                output_path = opt;
            else if ((opt = omm_parse_string_option2(argc, argv, &i, "-c", "--classmap")))
                classmap_path = opt;
            else
            {
                fprintf(stderr, "Unrecognized option: %s\n\n", argv[i]);
                print_usage_and_exit(argv[0]);
            }
        }
        else
        {
            if (input_path)
            {
                fprintf(stderr, "Only one input file allowed\n");
                print_usage_and_exit(argv[0]);
            }
            input_path = argv[i];
        }
    }
    
    if (!input_path)
    {
        fprintf(stderr, "No input files\n");
        print_usage_and_exit(argv[0]);
    }

    if (!output_path)
    {
        fprintf(stderr, "No output file specified with -o option\n");
        print_usage_and_exit(argv[0]);
    }

    if (!classmap_path)
    {
        fprintf(stderr, "Classmap is required\n");
        print_usage_and_exit(argv[0]);
    }

 
    // ----------------------------------------

    double *weights = liblinear_model_load(input_path, &n_classes, &dim); 
    
    // integer approximation
    enum { MAX_WEIGHT = 4095 };
    int n_weights = n_classes * dim;
    int32_t *w = (int *) malloc(n_weights * sizeof(int));
    double m = max_abs(weights, n_weights);
    for (int i = 0; i < n_weights; i++)
        w[i] = (int32_t) lround(weights[i] * MAX_WEIGHT / m);
    free(weights);
    
    OmmClassMap *cm = omm_classmap_load(classmap_path);

    // write the model: n_classes, dim, weights, classmap
    OmmModelWriter *writer = omm_model_writer_new(output_path, endianness);
    enum {MODEL_FILE_VERSION = 1};
    uint32_t header[] = {MODEL_FILE_VERSION, n_classes, dim};
    omm_model_writer_write(writer, header, sizeof(header)/sizeof(*header));
    omm_model_writer_write(writer, (uint32_t *) w, n_weights);
    for (int i = 0; i < n_classes; i++)
        omm_model_writer_write_string(writer, omm_classmap_get_text(cm, i));
    omm_model_writer_free(writer);

    omm_classmap_free(cm);

    /*
    DenseDataset *d = dense_dataset_load(dataset_path);
    if (dim != d->dimension)
    {
        fprintf(stderr, "Dimension mismatch: dataset has %d, model has %d\n", d->dimension, dim);
        exit(EXIT_FAILURE);
    }

    int count = 0;
    for (int sample = 0; sample < d->n_samples; sample++)
    {
        double *query = d->features + sample * dim;
        int best = -1;
        double best_score = 0;
        for (int c = 0; c < n_classes; c++)
        {
            double score = dot_int(query, w + c * dim, dim);
            if (best < 0 || score > best_score)
            {
                best = c;
                best_score = score;
            }
        }
        if (best == d->labels[sample])
            count++;
    }
    printf("Accuracy: %.2lf%%\n", 100.0 * count / d->n_samples);
    dense_dataset_free(d); */
    free(w);
}

// vim: set fdm=marker:
