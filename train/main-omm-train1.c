/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */


#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <limits.h>
#include <stdbool.h>
#include "omm-io-model.h"
#include "omm-endian.h"
#include "omm-features.h"
#include "omm-io-boxtiff.h"
#include "omm-io-arff.h"
#include "omm-classmap.h"
#include "omm-options.h"

static void print_usage_and_exit(const char *this_prog)
{
    fprintf(stderr, "Usage:\n");
    fprintf(stderr, "    %s [options] page1.box page1.tiff page2.box page2.tiff ...\n", this_prog);
    fprintf(stderr, "OR\n");
    fprintf(stderr, "    %s [options] directory\n", this_prog);
    fprintf(stderr, "Options:\n");
    fprintf(stderr, "    -c classmap_file or --classmap classmap_file\n");
    fprintf(stderr, "    -o output_file or --output output_file\n");
    fprintf(stderr, "    -f format or --format format\n");
    exit(EXIT_FAILURE);
}

typedef struct
{
    int n_samples;

    /// All features (n_samples * omm_feature_length())
    int *features;
    uint32_t *labels;
} Section;

static void section_free(Section *s)
{
    if (s->features)
        free(s->features);
    if (s->labels)
        free(s->labels);
    free(s);
}

static void fe_from_boxtiff(OmmBoxtiff *boxtiff, int i, int *features)
{
    OmmImage *image = omm_boxtiff_get_image(boxtiff, i);
    omm_extract_features_from_glyph(image, features);
    omm_image_free(image);
}

static double *get_features_double(int *features, int dim, int *out_dim)
{
    *out_dim = dim - 1;
    double *result = (double *) malloc(*out_dim * sizeof(double));
    //double coef = 1.0 / *features;
    features++;
    for (int i = 0; i < *out_dim; i++)
        result[i] = /* coef * */ features[i];

    // normalization
    double s = 0;
    for (int i = 0; i < *out_dim; i++)
        s += result[i] * result[i];
    s = 1 / sqrt(s);
    for (int i = 0; i < *out_dim; i++)
        result[i] *= s;

    return result;
}


// XXX this needs_arff_header mechanism is too complicated
static bool section_to_csv(Section *s, OmmClassMap *cm, FILE *f, bool needs_arff_header)
{
    int int_dim = omm_feature_length();
    for (int i = 0; i < s->n_samples; i++)
    {
        int *int_features = s->features + i * int_dim;
        int dim;
        double *features = get_features_double(int_features, int_dim, &dim);

        if (needs_arff_header)
        {
            needs_arff_header = false;
            omm_write_arff_header(f, dim, omm_classmap_get_n_classes(cm));
        }

        int label = s->labels[i];
        omm_write_csv(f, 1, dim, features, &label); 
        free(features);
    }
    return needs_arff_header;
}

static void section_to_libsvm(Section *s, FILE *f)
{
    int int_dim = omm_feature_length();
    for (int i = 0; i < s->n_samples; i++)
    {
        int *int_features = s->features + i * int_dim;
        int dim;
        double *features = get_features_double(int_features, int_dim, &dim);

        int label = s->labels[i];
        fprintf(f, "%d", label);
        for (int j = 0; j < dim; j++)
            if (features[j])
                fprintf(f, " %d:%lg", j + 1, features[j]);
        fprintf(f, "\n");
        free(features);
    }
}

static void section_to_table(Section *s, FILE *f)
{
    int int_dim = omm_feature_length();
    for (int i = 0; i < s->n_samples; i++)
    {
        int *int_features = s->features + i * int_dim;
        int dim;
        double *features = get_features_double(int_features, int_dim, &dim);

        int label = s->labels[i];
        fprintf(f, "%d", label);
        for (int j = 0; j < dim; j++)
            fprintf(f, " %lg", features[j]);
        fprintf(f, "\n");
        free(features);
    }
}

static Section *process_boxtiff(
    OmmBoxtiff *restrict boxtiff, 
    OmmClassMap *restrict classmap)
{
    enum {MAX_SAMPLES = INT_MAX};
    int n_samples = omm_boxtiff_n_boxes(boxtiff);
    if (n_samples > MAX_SAMPLES)
        n_samples = MAX_SAMPLES;

    Section *s = (Section *) malloc(sizeof(Section));
    s->labels = (uint32_t *) malloc(n_samples * sizeof(uint32_t));

    int dim = omm_feature_length();
    s->features = (int *) malloc(n_samples * dim * sizeof(int));
    
    int fill = 0;
    for (int i = 0; i < n_samples; i++)
    {
        const char *text = omm_boxtiff_get_text(boxtiff, i);
        int c = 0;
        if (classmap)
        {
            c = omm_classmap_get_class(classmap, text);
            if (c < 0)
                continue;
        }
        fe_from_boxtiff(boxtiff, i, s->features + dim * fill);
        s->labels[fill] = c;
        fill++;
    }
    s->n_samples = fill;
    s->features = (int *) realloc(s->features, fill * dim * sizeof(int));
    return s;
}


static void output_csv_or_arff(
    const char *model_path,
    Section **sections, 
    int n_sections,
    OmmClassMap *classmap, 
    bool needs_arff_header,
    const void *data)
{
    (void) data;
    if (!classmap)
    {
        fprintf(stderr, "A classmap is required for CSV/ARFF output\n");
        exit(EXIT_FAILURE);
    }
    if (!model_path)
    {
        fprintf(stderr, "Error: output file not specified\n");
        exit(EXIT_FAILURE);
    }
    FILE *f = fopen(model_path, "w");
    if (!f)
    {
        perror(model_path);
        exit(EXIT_FAILURE);
    }
    for (int i = 0; i < n_sections; i++)
        needs_arff_header = section_to_csv(sections[i], classmap, f, needs_arff_header);
    fclose(f);
}

static void output_csv(
    const char *model_path,
    Section **sections, 
    int n_sections,
    OmmClassMap *classmap,
    const void *data)
{
    output_csv_or_arff(model_path, sections, n_sections, classmap, false, data);
}


static void output_arff(
    const char *model_path,
    Section **sections, 
    int n_sections,
    OmmClassMap *classmap,
    const void *data)
{
    output_csv_or_arff(model_path, sections, n_sections, classmap, true, data);
}

static void output_libsvm(
    const char *model_path,
    Section **sections, 
    int n_sections,
    OmmClassMap *classmap,
    const void *data)
{
    (void) data;
    if (!classmap)
    {
        fprintf(stderr, "A classmap is required for libsvm output\n");
        exit(EXIT_FAILURE);
    }
    if (!model_path)
    {
        fprintf(stderr, "Error: output file not specified\n");
        exit(EXIT_FAILURE);
    }
    FILE *f = fopen(model_path, "w");
    if (!f)
    {
        perror(model_path);
        exit(EXIT_FAILURE);
    }
    for (int i = 0; i < n_sections; i++)
        section_to_libsvm(sections[i], f);
    fclose(f);
}

// XXX: dup from output_libsvm
static void output_table(
    const char *model_path,
    Section **sections, 
    int n_sections,
    OmmClassMap *classmap,
    const void *data)
{
    (void) data;
    if (!classmap)
    {
        fprintf(stderr, "A classmap is required for table output\n");
        exit(EXIT_FAILURE);
    }
    if (!model_path)
    {
        fprintf(stderr, "Error: output file not specified\n");
        exit(EXIT_FAILURE);
    }
    FILE *f = fopen(model_path, "w");
    if (!f)
    {
        perror(model_path);
        exit(EXIT_FAILURE);
    }
    for (int i = 0; i < n_sections; i++)
        section_to_table(sections[i], f);
    fclose(f);
}


typedef void (*OutputFunc)(const char *, Section **, int, 
                           OmmClassMap *, const void *userdata);

// _____________________________   main   ____________________________________

int main(int argc, char *argv[])
{
    char *input_files[argc];
    int n_input_files = 0;
    const char *model_path = NULL;
    const char *classmap_path = NULL;
    const char *format_spec = NULL;
    int pca_dim = 0;

    // option parsing
    bool no_more_options = false;
    for (int i = 1; i < argc; i++)
    {
        const char *opt;
        if (!no_more_options && argv[i][0] == '-' && argv[i][1]) // starts with '-', but is not "-"
        {
            if (!strcmp(argv[i], "--"))
                no_more_options = true;
            else if ((opt = omm_parse_string_option2(argc, argv, &i, "-o", "--output")))
                model_path = opt;
            else if ((opt = omm_parse_string_option2(argc, argv, &i, "-c", "--classmap")))
                classmap_path = opt;
            else if ((opt = omm_parse_string_option2(argc, argv, &i, "-f", "--format")))
                format_spec = opt;
            else if ((opt = omm_parse_string_option2(argc, argv, &i, "-d", "--dim")))
            {
                pca_dim = atoi(opt);
                if (pca_dim <= 0)
                    print_usage_and_exit(argv[0]);
            }
            else
            {
                fprintf(stderr, "Unrecognized option: %s\n\n", argv[i]);
                print_usage_and_exit(argv[0]);
            }
        }
        else
        {
            input_files[n_input_files++] = argv[i];
        }
    }

    OmmBoxtiffDataset *dataset = omm_boxtiff_dataset_new(input_files, n_input_files);
    if (!dataset)
        return EXIT_FAILURE;
    
    OutputFunc output_func;
    const void *output_func_data = NULL;
    if (!format_spec)
        output_func = output_libsvm;
    else if (!strcasecmp(format_spec, "arff"))
        output_func = output_arff;
    else if (!strcasecmp(format_spec, "csv"))
        output_func = output_csv;
    else if (!strcasecmp(format_spec, "table"))
        output_func = output_table;
    else if (!strcasecmp(format_spec, "libsvm"))
        output_func = output_libsvm;
    else
    {
        fprintf(stderr, "Unrecognized output format: %s\n", format_spec);
        fprintf(stderr, "Supported formats: omm arff csv\n");
        exit(EXIT_FAILURE);
    }
   
    if (!model_path)
    {
        fprintf(stderr, "No output file name given\n\n");
        print_usage_and_exit(argv[0]);
    }

    OmmClassMap *classmap = NULL;
    if (classmap_path)
        if (!(classmap = omm_classmap_load(classmap_path)))
            exit(EXIT_FAILURE);



    int n_pairs = omm_boxtiff_dataset_size(dataset);
    Section **sections = (Section **) malloc(n_pairs * sizeof(Section *));
    for (int pair_index = 0; pair_index < n_pairs; pair_index++)
    {
        OmmBoxtiff *boxtiff = omm_boxtiff_dataset_get(dataset, pair_index);
        if (!boxtiff)
            return EXIT_FAILURE; // error message should be printed by omm_boxtiff_open
        sections[pair_index] = process_boxtiff(boxtiff, classmap);
        omm_boxtiff_close(boxtiff);
    }

    output_func(model_path, sections, n_pairs, classmap, output_func_data);

    if (classmap)
        omm_classmap_free(classmap);
    for (int i = 0; i < n_pairs; i++)
        section_free(sections[i]);
    free(sections);
    omm_boxtiff_dataset_free(dataset);
    return 0;
}
