/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "omm-btree.h"
#include "omm-path.h"

static int pathcmp(const void *p1, const void *p2)
{
    return strcoll((const char *) p1, (const char *) p2);
}

// reimplementing strdup() because it's not turned on by _POSIX_SOURCE (why?)
char *omm_strdup(const char *s)
{
    char *result = (char *) malloc(strlen(s) + 1);
    strcpy(result, s);
    return result;
}

static void *pathdup(void *p)
{
    const char *s = (const char *) p;
    return omm_strdup(s);
}

static void store(void *string, void *cursor)
{
    char *s = (char *) string;
    char ***p_cursor = (char ***) cursor;
    *((*p_cursor)++) = s; // sorry for that
}

char **omm_ls(const char *path, int *p_n)
{
    DIR *dir = opendir(path);
    if (!dir)
    {
        *p_n = -1;
        return NULL;
    }
    
    OmmBTree *btree = omm_btree_new(pathcmp);
    struct dirent *entry;
    int n = 0;
    while ((entry = readdir(dir)))
    {
        n++;
        omm_btree_insert_dup(btree, entry->d_name, pathdup); 
    }

    closedir(dir);

    char **result = (char **) malloc(n * sizeof(char *));
    char **p = result;
    omm_btree_foreach(btree, store, &p);
    omm_btree_free(btree);
    *p_n = n;
    return result;
}

enum { DIRSEP = '/' };

static bool omm_is_abspath(const char *path)
{
    return *path == DIRSEP;
}

char *omm_path_join(const char *path1, const char *path2)
{
    size_t len1 = strlen(path1);
    size_t len2 = strlen(path2);
    
    if (omm_is_abspath(path2))
        return omm_strdup(path2);

    if (path1[len1 - 1] == DIRSEP)
        len1--;

    char *result = (char *) malloc(len1 + len2 + 2);
    strncpy(result, path1, len1);
    result[len1] = DIRSEP;
    strncpy(result + len1 + 1, path2, len2 + 1);
    return result;
}

void omm_ls_free(char **a, int n)
{
    if (!a)
        return;
    for (int i = 0; i < n; i++)
        free(a[i]);
    free(a);
}
