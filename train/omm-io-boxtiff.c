/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <assert.h>
#include <sys/stat.h>
#include <limits.h>
#include "omm-io-tiff.h"
#include "omm-io-boxtiff.h"
#include "omm-io-utils.h"
#include "omm-path.h"
#include "omm-image.h"

typedef struct
{
    int x0;
    int y0;
    int x1;
    int y1;
    int page_no;
    int cumulative_length;
} Box;

struct OmmBoxtiff
{
    char *text;
    size_t length;
    int n_boxes;
    Box *boxes;
    char **box_texts;
    OmmTIFF *tiff;
    int current_page_no;
    OmmImage *page;
};


static int skip_space(int i, char *text, int length)
{
    while (i < length && (text[i] == ' ' || text[i] == '\t'))
        i++;
    return i;
}

static int skip_nonspace(int i, char *text, int length)
{
    while (i < length 
        && text[i] != ' '
        && text[i] != '\t'
        && text[i] != '\f'
        && text[i] != '\r'
        && text[i] != '\n')
    {
        i++;
    }
    return i;
}

static int skip_newlines(int i, char *text, int length)
{
    while (i < length && (text[i] == '\f' || text[i] == '\n' || text[i] == '\r'))
        i++;
    return i;
}

static int count_nonempty_lines(char *text, size_t length)
{
    size_t i = 0;
    int count = 0;
    while (i < length)
    {
        i = skip_newlines(i, text, length);
        if (i == length)
            break;
        while (i < length && text[i] != '\f' &&  text[i] != '\n' && text[i] != '\r')
            i++;
        count++;
    }
    return count;
}

static int parse_text(
    int cursor,
    char *text,
    int length,
    char **p_box_text,
    int *p_text_length)
{
    cursor = skip_newlines(cursor, text, length);
    cursor = skip_space(cursor, text, length);
    *p_box_text = text + cursor;
    int new_cursor = skip_nonspace(cursor, text, length);
    if (new_cursor == cursor)
    {
        fprintf(stderr, "boxtiff parse error: text expected\n");
        exit(EXIT_FAILURE);
    }
    *p_text_length = new_cursor - cursor;
    cursor = new_cursor;
    if (cursor >= length)
    {
        fprintf(stderr, "boxtiff parse error: unexpected EOF\n");
        exit(EXIT_FAILURE);
    }
    text[cursor++] = '\0';
    return cursor;
}

static int parse_int(
    int cursor,
    char *text,
    int length,
    int *p_num)
{
    cursor = skip_space(cursor, text, length);
    char *endptr;
    long a = strtol(text + cursor, &endptr, 0);
    int new_cursor = endptr - text;
    if (new_cursor == cursor)
    {
        fprintf(stderr, "boxtiff parse error: integer expected\n");
        exit(EXIT_FAILURE);
    }
    if (((a == LONG_MIN || a == LONG_MAX) && errno == ERANGE)
     || a < INT_MIN || a > INT_MAX)
    {
        fprintf(stderr, "boxtiff parse error: integer out of range\n");
        exit(EXIT_FAILURE);
    }
    *p_num = (int) a;
    return new_cursor;
}

static int parse_box(
    int cursor,
    char *text,
    int length,
    Box *p_box)
{
    cursor = parse_int(cursor, text, length, &p_box->x0);
    cursor = parse_int(cursor, text, length, &p_box->y0);
    cursor = parse_int(cursor, text, length, &p_box->x1);
    cursor = parse_int(cursor, text, length, &p_box->y1);
    cursor = parse_int(cursor, text, length, &p_box->page_no);
    return cursor;
}
    

static int parse_boxtiff(OmmBoxtiff *boxtiff)
{
    int n = boxtiff->n_boxes = count_nonempty_lines(boxtiff->text, boxtiff->length);
    boxtiff->boxes = (Box *) malloc(n * sizeof(Box));
    boxtiff->box_texts = (char **) malloc(n * sizeof(char *));

    int cursor = 0;
    int cumulative_length = 0;
    for (int i = 0; i < n; i++)
    {
        int len;
        cursor = parse_text(cursor, boxtiff->text, boxtiff->length, 
                            &boxtiff->box_texts[i], &len);
        cursor = parse_box(cursor, boxtiff->text, boxtiff->length, 
                           &boxtiff->boxes[i]);
        boxtiff->boxes[i].cumulative_length = cumulative_length += len + 1;
    } 
    return 1;
}

OmmBoxtiff *omm_boxtiff_open(const char *box_path, const char *tiff_path)
{
    OmmBoxtiff *result = (OmmBoxtiff *) calloc(1, sizeof(OmmBoxtiff));
    result->current_page_no = -1;
    result->tiff = omm_tiff_open(tiff_path);
    if (!result->tiff)
    {
        omm_boxtiff_close(result);
        perror(tiff_path);
        return NULL;
    }
    result->text = omm_read_entire_file(box_path, &result->length);
    if (!result->text)
    {
        omm_boxtiff_close(result);
        return NULL;
    }

    if (!parse_boxtiff(result))
    {
        omm_boxtiff_close(result);
        return NULL;
    }
    return result;

}

int omm_boxtiff_n_boxes(OmmBoxtiff *boxtiff)
{
    return boxtiff->n_boxes;
}

int omm_boxtiff_get_width(OmmBoxtiff *boxtiff, int i)
{
    return boxtiff->boxes[i].x1 - boxtiff->boxes[i].x0;
}
int omm_boxtiff_get_height(OmmBoxtiff *boxtiff, int i)
{
    return boxtiff->boxes[i].y1 - boxtiff->boxes[i].y0;
}
void omm_boxtiff_get_size(OmmBoxtiff *boxtiff, int i, int *p_width, int *p_height)
{
    *p_width = omm_boxtiff_get_width(boxtiff, i);
    *p_height = omm_boxtiff_get_height(boxtiff, i);
}

/**
 * The return value becomes invalid after omm_boxtiff_close().
 */
const char *omm_boxtiff_get_text(OmmBoxtiff *boxtiff, int i)
{
    return boxtiff->box_texts[i];
}

void omm_boxtiff_close(OmmBoxtiff *boxtiff)
{
    if (!boxtiff)
        return;
    if (boxtiff->tiff)
        omm_tiff_close(boxtiff->tiff);
    if (boxtiff->text)
        free(boxtiff->text);
    if (boxtiff->boxes)
        free(boxtiff->boxes);
    if (boxtiff->box_texts)
        free(boxtiff->box_texts);
    if (boxtiff->page)
        omm_image_free(boxtiff->page);
    free(boxtiff);
}

void omm_boxtiff_dump(OmmBoxtiff *boxtiff)
{
    for (int i = 0; i < boxtiff->n_boxes; i++)
    {
        printf("%s %d %d %d %d %d\n",
                boxtiff->box_texts[i],
                boxtiff->boxes[i].x0,
                boxtiff->boxes[i].y0,
                boxtiff->boxes[i].x1,
                boxtiff->boxes[i].y1,
                boxtiff->boxes[i].page_no);
    }
}

static void load_page(OmmBoxtiff *boxtiff, int page_no)
{
    if (page_no == boxtiff->current_page_no)
        return;
    if (boxtiff->page)
        omm_image_free(boxtiff->page);
    boxtiff->page = omm_tiff_read_rle(boxtiff->tiff, page_no);
    boxtiff->current_page_no = page_no;
}

OmmImage *omm_boxtiff_get_image(OmmBoxtiff *boxtiff, int i)
{
    load_page(boxtiff, boxtiff->boxes[i].page_no);
    int x0 = boxtiff->boxes[i].x0; 
    int y0 = boxtiff->page->height - boxtiff->boxes[i].y1 - 1; 
    int x1 = boxtiff->boxes[i].x1; 
    int y1 = boxtiff->page->height - boxtiff->boxes[i].y0 - 1; 
    return omm_image_crop(boxtiff->page, x0, y0, x1, y1);
}

// ______________________________   filter names    ___________________________

static int common_prefix_length(const char *s1, const char *s2)
{
    int i = 0;
    while (s1[i] && s1[i] == s2[i])
        i++;
    return i;
}

static int box_file_basename_length(const char *name, int len)
{
    int offset = len - 4;
    if (offset >= 0 && !strcasecmp(name + offset, ".box"))
        return offset;
    return 0;
}

static int tiff_file_basename_length(const char *name, int len)
{
    int offset = len - 4;
    if (offset >= 0 && !strcasecmp(name + offset, ".tif"))
        return offset;
    offset = len - 5;
    if (offset >= 0 && !strcasecmp(name + offset, ".tiff"))
        return offset;
    return 0;
}

/**
 * I was in a very mathematical mood when doing this. I even considered 
 * using maximum marriage in the bipartite graph of box and tiff files,
 * so here's kind of a light version :)
 */
char **omm_boxtiff_filter_names(char **names, int n, int *out_n_pairs)
{
    int *lengths = (int *) malloc(n * sizeof(int));
    for (int i = 0; i < n; i++)
        lengths[i] = strlen(names[i]);
    int *cpl = (int *) malloc((n - 1) * sizeof(int));
    for (int i = 1; i < n; i++)
        cpl[i - 1] = common_prefix_length(names[i - 1], names[i]);

    int n_pairs = 0;
    char **result = (char **) malloc(n * sizeof(char *));

    for (int i = 0; i < n; i++)
    {
        int box = box_file_basename_length(names[i], lengths[i]);
        if (!box)
            continue;

        // We have a file 'A.box'.
        // We're interested in all entries starting with 'A.'.
        // Since we have a sorted list, all those entries are consecutive.
        // CPL between those entries is at least the length of 'A.', 
        // which is box + 1.
        int span_min = i;
        while (span_min > 0 && cpl[span_min - 1] >= box + 1)
            span_min--;
        int span_max = i;
        while (span_max < n - 1 && cpl[span_max] >= box + 1)
            span_max++;
        
        // Now there should be exactly 1 TIFF among files that start with 'A.'
        int tif = -1;
        for (int j = span_min; j <= span_max; j++)
        {
            if (tiff_file_basename_length(names[j], lengths[j]))
            {
                if (tif >= 0)
                {
                    // error: 2 tif files for 1 box file
                    fprintf(stderr, "2 TIFF files for a single box file:\n"
                                    "    `%s' and `%s' correspond to `%s'\n",
                                    names[tif], names[j], names[i]);
                    free(result);
                    result = NULL;
                    break;
                }
                tif = j;
            }
        }
        if (tif < 0)
        {
            fprintf(stderr, "No TIFF file corresponds to `%s'\n", names[i]);
            free(result);
            result = NULL;
        }
        if (!result)
            break;
        result[2 * n_pairs] = names[i];
        result[2 * n_pairs + 1] = names[tif];
        n_pairs++;
    }

    *out_n_pairs = n_pairs;
    free(cpl);
    free(lengths);
    if (result == NULL)
        *out_n_pairs = -1; // to be consistent with omm_ls() interface
    return result;
}

char **omm_boxtiff_ls(const char *path, int *out_n_pairs)
{
    int n_files;
    char **files = omm_ls(path, &n_files);
    if (n_files < 0)
    {
        *out_n_pairs = -1;
        assert(!files);
        return NULL;
    }

    char **pairs = omm_boxtiff_filter_names(files, n_files, out_n_pairs);
    if (*out_n_pairs < 0)
    {
        omm_ls_free(files, n_files);
        assert(!pairs);
        return NULL;
    }

    for (int i = 0; i < 2 * (*out_n_pairs); i++)
        pairs[i] = omm_path_join(path, pairs[i]); 
    
    omm_ls_free(files, n_files); 
    return pairs;
}

// _________________________________________________________________________

struct OmmBoxtiffDataset
{
    int n_pairs;
    char **pairs; /// 2 * n_pairs entries in (box, tiff) pairs
};

OmmBoxtiffDataset *omm_boxtiff_dataset_new(char **input_files, int n)
{
    if (!n)
    {
        fprintf(stderr, "No input files\n");
        return NULL;
    }

    int n_pairs;
    char **pairs;

    if (n == 1)
    {
        // a directory
        pairs = omm_boxtiff_ls(input_files[0], &n_pairs);
        if (n_pairs < 0)
            return NULL;
        if (n_pairs == 0)
        {
            fprintf(stderr, "%s: no box/tiff pairs found in the directory\n", input_files[0]);
            return NULL;
        }
    }
    else
    {
        if (n % 2)
        {
            fprintf(stderr, "The input files should go in pairs (box, tiff)\n");
            return NULL;
        }
        n_pairs = n / 2;
        pairs = (char **) malloc(n * sizeof(char *));
        for (int i = 0; i < n; i++)
            pairs[i] = omm_strdup(input_files[i]);
    }

    OmmBoxtiffDataset *d = (OmmBoxtiffDataset *) 
                               malloc(sizeof(OmmBoxtiffDataset));
    d->n_pairs = n_pairs;
    d->pairs = pairs;
    return d;
}

int omm_boxtiff_dataset_size(OmmBoxtiffDataset *d)
{
    return d->n_pairs;
}

OmmBoxtiff *omm_boxtiff_dataset_get(OmmBoxtiffDataset *d, int i)
{
    return omm_boxtiff_open(d->pairs[2 * i], d->pairs[2 * i + 1]);
}

void omm_boxtiff_dataset_free(OmmBoxtiffDataset *d)
{
    omm_ls_free(d->pairs, 2 * d->n_pairs);
    free(d);
}
