/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include <stdio.h>
#include <stdlib.h>
#include "omm-io-arff.h"

static void write_arff_class(FILE *f, int n_classes)
{
    fprintf(f, "@ATTRIBUTE class {");
    for (int i = 0; i < n_classes; i++)
    {
        if (i)
            fprintf(f, ", ");
        fprintf(f, "%d", i);
    }
    fprintf(f, "}\n");
}

void omm_write_arff_header(FILE *f, 
                           int dimension, 
                           int n_classes)
{
    fprintf(f, "@RELATION omm\n\n");

    write_arff_class(f, n_classes);

    for (int i = 0; i < dimension; i++)
        fprintf(f, "@ATTRIBUTE feature%d NUMERIC\n", i);

    fprintf(f, "\n@DATA\n");
}

void omm_write_csv(FILE *f, int n_samples, int dimension, double *features, int *classes)
{
    for (int i = 0; i < n_samples; i++)
    {
        fprintf(f, "%d", classes[i]);
        for (int j = 0; j < dimension; j++)
            fprintf(f, ",%lf", features[i * dimension + j]);
        fprintf(f, "\n");
    }
}
