/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#ifndef OMM_IO_BOXTIFF
#define OMM_IO_BOXTIFF

#include "omm-image.h"

// http://code.google.com/p/tesseract-ocr/wiki/TrainingTesseract

typedef struct OmmBoxtiff OmmBoxtiff;

OmmBoxtiff *omm_boxtiff_open(const char *box_path, const char *tiff_path);
int omm_boxtiff_n_boxes(OmmBoxtiff *boxtiff);
int omm_boxtiff_get_width       (OmmBoxtiff *, int i);
int omm_boxtiff_get_height      (OmmBoxtiff *, int i);
void omm_boxtiff_get_size       (OmmBoxtiff *, int i, int *p_width, int *p_height);

OmmImage *omm_boxtiff_get_image(OmmBoxtiff *, int i);

/**
 * The return value becomes invalid after omm_boxtiff_close().
 */
const char *omm_boxtiff_get_text(OmmBoxtiff *, int i);
void omm_boxtiff_close          (OmmBoxtiff *);
void omm_boxtiff_dump(OmmBoxtiff *boxtiff);

/**
 * Given an alphabetically sorted list of files in a directory,
 * find .box-.tif pairs.
 * The rule is: A.box matches A.B.tif; if several tifs fit, then it's an error.
 * Box file names may not be like A.box and A.B.box.
 * This function permutes the list of names.
 *
 * \param[in] names           An alphabetically sorted list of file names.
 * \param[out]  out_n_pairs   The number of (box, tif) pairs found.

 *
 * \returns  2 * n_pairs names, in (box, tif) pairs.
 *           In case of error, prints a message to stderr and returns NULL.
 */
char **omm_boxtiff_filter_names(char **names, int n, int *out_n_pairs);

char **omm_boxtiff_ls(const char *path, int *out_n_pairs);

// _______________________________________________________________________

typedef struct OmmBoxtiffDataset OmmBoxtiffDataset;

OmmBoxtiffDataset *omm_boxtiff_dataset_new(char **input_files, int n);
int omm_boxtiff_dataset_size(OmmBoxtiffDataset *);
OmmBoxtiff *omm_boxtiff_dataset_get(OmmBoxtiffDataset *, int i);
void omm_boxtiff_dataset_free(OmmBoxtiffDataset *);

#endif
