/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#ifndef OMM_PATH_H
#define OMM_PATH_H

/**
 * A reimplementation of strdup().
 */
char *omm_strdup(const char *s);

/**
 * List the files in a directory in alphabetical order.
 */
char **omm_ls(const char *path, int *out_n);

/**
 * Free the result of omm_ls(). (It's very easy, it's just a vector of strings)
 */
void omm_ls_free(char **a, int n);

/**
 * Join paths (as os.path.join in Python), returns a malloc'd string.
 */
char *omm_path_join(const char *path1, const char *path2);

#endif
