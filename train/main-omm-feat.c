/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include <stdio.h>
#include <stdlib.h>
#include "omm-io-tiff.h"
#include "omm-median.h"
#include "omm-labeling.h"
#include "omm-features.h"

static void print_usage_and_exit(const char *prog)
{
    fprintf(stderr, "Usage: %s input.tif\n", prog);
    exit(EXIT_FAILURE);
}

int main(int argc, const char *const *argv)
{
    if (argc != 2)
        print_usage_and_exit(argv[0]);

    OmmImage *image = omm_tiff_read_first_page_rle(argv[1]);
    if (!image)
        return EXIT_FAILURE;
    
    // label
    uint32_t *labels;
    OmmBlobInfo *blobs;
    int n_blobs = omm_label_and_analyze_blobs(&labels, &blobs, image);
    int text_size = omm_guess_text_size(blobs, n_blobs);

    // filter
    int n_samples = 0;
    for (int i = 0; i < n_blobs; i++)
    {
        int w = blobs[i].max_x - blobs[i].min_x;
        int h = blobs[i].max_y - blobs[i].min_y;
        if (h > text_size / 2 
         && h < text_size * 2
         && w > h / 4
         && w < h * 2)
        {
            blobs[i].index = n_samples++;
        }
        else
            blobs[i].index = -1;
    }

    // fe
    int dim = omm_feature_length();
    int *bins = (int *) malloc(dim * n_samples * sizeof(int));
    omm_extract_features(blobs, n_blobs, n_samples, image, labels, bins);

    for (int i = 0; i < n_samples; i++)
    {
        for (int j = 0; j < dim; j++)
        {
            if (j)
                printf(" ");
            printf("%d", bins[i * dim + j]);
        }
        printf("\n");
    }

    free(bins);
    free(labels);
    free(blobs);
    omm_image_free(image);

    return EXIT_SUCCESS;
}
