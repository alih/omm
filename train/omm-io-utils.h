/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#ifndef OMM_IO_UTILS_H
#define OMM_IO_UTILS_H

#include <stdio.h>
#include <stdint.h>

// size_t omm_file_size(const char *restrict path, FILE *restrict file);

/**
 * Reads the file content into a malloc'd memory.
 * An additional zero byte is always added at the end.
 * In case of error, returns NULL and prints an error message to stderr.
 * In case of success, never returns NULL even for an empty file.
 *
 * This doesn't use mmap().
 */
char *omm_read_entire_file(
    const char *restrict path, 
    size_t *restrict out_size);

// XXX do we need that?
void omm_look_up_and_print(
    FILE *restrict file,
    const uint32_t *restrict offsets, 
    int n, 
    const char *restrict strpool);

#endif
