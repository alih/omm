/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include <sys/time.h>
#include <sys/resource.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include "omm-time.h"

static struct timeval last_utime = {.tv_sec = 0, .tv_usec = 0};
static struct timeval last_stime = {.tv_sec = 0, .tv_usec = 0};


// This implementation doesn't use 64-bit integers on a 32-bit machine.
// (There's little reason to bother, actually, but why not)
static void omm_timediff(
    struct timeval *restrict result, 
    const struct timeval *t1, 
    const struct timeval *t2)
{
    long usec_diff = (long) t1->tv_usec - (long) t2->tv_usec;
    if (usec_diff < 0)
    {
        result->tv_sec = t1->tv_sec - t2->tv_sec - 1;
        result->tv_usec = usec_diff + 1000000;
    }
    else
    {
        result->tv_sec = t1->tv_sec - t2->tv_sec;
        result->tv_usec = usec_diff;
    }
}

void omm_timestamp_count(FILE *f, int n)
{
    struct rusage r;
    if (getrusage(RUSAGE_SELF, &r))
    {
        perror("getrusage");
        exit(EXIT_FAILURE);
    }

    struct timeval udiff;
    struct timeval sdiff;
    omm_timediff(&udiff, &r.ru_utime, &last_utime);
    omm_timediff(&sdiff, &r.ru_stime, &last_stime);

    fprintf(f, "     usr %1ld.%06ld, sys %1ld.%06ld from previous measurement,\n"
               "     usr %1ld.%06ld, sys %1ld.%06ld from program start\n",
               udiff.tv_sec, udiff.tv_usec,
               sdiff.tv_sec, sdiff.tv_usec,
               r.ru_utime.tv_sec, r.ru_utime.tv_usec,
               r.ru_stime.tv_sec, r.ru_stime.tv_usec); 

    last_utime = r.ru_utime;
    last_stime = r.ru_stime;

    if (n >= 0)
    {
        int64_t t = udiff.tv_sec * 1000000 + udiff.tv_usec;
        fprintf(f, "%d iterations passed:\n", n);
        int64_t ips = 1000000000 * (int64_t) n / t;
        fprintf(f, "    iterations per sec: %d.%03d\n", (int) (ips / 1000), (int) (ips % 1000));
        int64_t spi = t / n;
        fprintf(f, "    iteration speed: %d.%06d sec/iter\n", (int) (spi / 1000000), (int) (spi % 1000000));
    }
}

void omm_timestamp(FILE *f)
{
    omm_timestamp_count(f, -1);
}

long omm_seconds_passed()
{
    struct rusage r;
    if (getrusage(RUSAGE_SELF, &r))
    {
        perror("getrusage");
        exit(EXIT_FAILURE);
    }

    struct timeval udiff;
    omm_timediff(&udiff, &r.ru_utime, &last_utime);
    return udiff.tv_sec;
}
