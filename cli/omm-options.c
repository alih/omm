/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "omm-options.h"

const char *omm_parse_string_option(
    int argc, 
    char **argv, 
    int *ref_cursor, 
    const char *opt)
{
    const char *a = argv[*ref_cursor];
    int len = strlen(opt);
    if (strncmp(a, opt, len))
        return NULL;

    if (a[len] == '=')
        return a + len + 1;

    if (a[len])
        return NULL;

    (*ref_cursor)++;
    if (*ref_cursor >= argc)
    {
        fprintf(stderr, "Option `%s' requires an argument, either like this:\n", opt);
        fprintf(stderr, "    %s argument\n", opt);
        fprintf(stderr, "or like this:\n");
        fprintf(stderr, "    %s=argument\n", opt);
        exit(EXIT_FAILURE);
    }
    return argv[*ref_cursor];
}

const char *omm_parse_string_option2(
    int argc, 
    char **argv, 
    int *ref_cursor, 
    const char *opt1,
    const char *opt2)
{
    const char *p = omm_parse_string_option(argc, argv, ref_cursor, opt1);
    if (p)
        return p;
    return omm_parse_string_option(argc, argv, ref_cursor, opt2);
}
