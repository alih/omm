/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <assert.h>
#include "omm-labeling.h"
#include "omm-io-tiff.h"
#include "omm-io-model.h"
#include "omm-features.h"
#include "omm-time.h"
#include "omm-median.h"
#include "omm-layout.h"
#include "omm-ocr.h"


typedef enum
{
    OUTPUT_NONE,
    OUTPUT_LINES,
    OUTPUT_TESS_BOX,
} OutputFormat;

static int compare_rec_chars(const void *p1, const void *p2)
{
    const OmmRecChar *r1 = (const OmmRecChar *) p1;
    const OmmRecChar *r2 = (const OmmRecChar *) p2;
    if (r1->line != r2->line)
        return r1->line - r2->line;
    return r1->blob->min_x + r1->blob->max_x - r2->blob->min_x - r2->blob->max_x;
}

/**
 * Get the gaps (boxes[i + 1].left - boxes[i].right) 
 * between the characters in a line.
 *
 * Starting from rec_char[offset], 
 * seek forward for rec_chars with the same line.
 *
 * gaps[] should contain at least n - offset - 1 item.
 *
 * Returns the number of gaps found.
 */
static int get_gaps(
    const OmmRecChar *restrict rec_chars,
    int offset,
    int n,
    int *restrict gaps)
{
    int line = rec_chars[offset].line;
    int i = offset + 1;
    int k = 0;
    while (i < n && rec_chars[i].line == line)
    {
        gaps[k++] = rec_chars[i].blob->min_x - rec_chars[i - 1].blob->max_x;
        i++;
    }
    return k;
}

enum { SPACING_COEF = 2 };

static void output_lines(FILE *restrict stream,
                         const OmmRecChar *restrict rec_chars,
                         int n_samples)
{
    int *gaps = (int *) malloc(n_samples * sizeof(int));
    int *gaps_copy = (int *) malloc(n_samples * sizeof(int)); // for median()
    
    for (int i = 0; i < n_samples;)
    {
        int n_gaps = get_gaps(rec_chars, i, n_samples, gaps);
        int m = 0;
        if (n_gaps > 0)
        {
            memcpy(gaps_copy, gaps, n_gaps * sizeof(int));
            m = SPACING_COEF * omm_median(gaps_copy, n_gaps);
        }
        for (int j = 0; j <= n_gaps; j++)
        {
            const char *s = rec_chars[i + j].text;
            fputs(s ? s : "[?]", stream);
            if (j < n_gaps && gaps[j] > m)
                fputc(' ', stream);
        }
        fputc('\n', stream);
        i += n_gaps + 1;
    }
    
    free(gaps);
    free(gaps_copy);
}

static void output(
    OutputFormat output_format,
    OmmRecChar *restrict rec_chars,
    int n_samples,
    int image_height,
    int page_no)
{
    // sort by line & X
    qsort(rec_chars, n_samples, sizeof(OmmRecChar), compare_rec_chars);
    switch (output_format)
    {
        case OUTPUT_NONE:
        break;
        case OUTPUT_LINES:
            output_lines(stdout, rec_chars, n_samples);
        break;
        /*case OUTPUT_QHULL_CENTERS:
            printf("2 %d\n", n_samples);
            for (int i = 0; i < n_samples; i++)
            {
                OmmBlobInfo *b = rec_chars[i].blob;
                int x = b->min_x + b->max_x;
                int y = b->min_y + b->max_y; 
                printf("%d.%d %d.%d\n", x / 2, x % 2 ? 5 : 0,
                                        y / 2, y % 2 ? 5 : 0);
            }
        break;*/ 
        case OUTPUT_TESS_BOX:
            for (int i = 0; i < n_samples; i++)
            { 
                OmmBlobInfo *b = rec_chars[i].blob;
                printf("%s %d %d %d %d %d\n", // %dx%d%+d%+d\n", 
                        rec_chars[i].text ? rec_chars[i].text : "[?]", //XXX dup
                        /*b->max_x - b->min_x, 
                        b->max_y - b->min_y, 
                        b->min_x, b->min_y, */
                        b->min_x, image_height - 1 - b->max_y,
                        b->max_x, image_height - 1 - b->min_y,
                        page_no);
            }
        break;
    }
}

static void process_page(
    OmmImage *rle, 
    OmmModel *model,
    int page_no,
    int min_size,
    int max_size,
    int report,
    OutputFormat output_format)
{
    /*if (report)
    {
        fprintf(stderr, "Starting with page %d\n", page_no);
        omm_timestamp(stderr);
    }*/
    OmmPage *page = omm_page_new(rle, min_size, max_size);
    /*if (report)
    {
        fprintf(stderr, "Layout analyzed\n");
        omm_timestamp(stderr);
    }*/
    (void) report;

    OmmRecChar *rec_chars = omm_ocr(model, rle, page);
    output(output_format, rec_chars, page->n_samples, rle->height, page_no);
    free(rec_chars);
    omm_page_free(page);
}

static void print_usage_and_exit(const char *this_prog)
{
    fprintf(stderr, "Usage: %s [options] input.tiff ... \n", this_prog);
    fprintf(stderr, "Options:\n");
    fprintf(stderr, "    -m <file> or --model <file>     Set the model file\n");
    fprintf(stderr, "    --min-size                      Minimum admissible character size, in pixels\n");
    fprintf(stderr, "    --max-size                      Maximum admissible character size, in pixels\n");
    fprintf(stderr, "    --boxes                         Output a bounding box for every letter\n");
    //fprintf(stderr, "    --qhull-centers                 Output bounding box centers in Qhull format\n");
    fprintf(stderr, "    -b or --benchmark               Run in a loop for 1 sec and print the speed\n");
    exit(EXIT_FAILURE);
}


static int atoi_or_die(const char *this_prog, const char *s)
{
    if (!s || !*s)
    {
        fprintf(stderr, "Integer option value expected\n");
        print_usage_and_exit(this_prog);
    }

    char *endptr;
    long result = strtol(s, &endptr, 0);
    if (*endptr || result < INT_MIN || result > INT_MAX)
    {
        fprintf(stderr, "Invalid integer value: %s\n", s);
        print_usage_and_exit(this_prog);
    }

    return result;
}

static const char *str_or_die(const char *this_prog, const char *s) // TODO: to some option parsing file
{
    if (!s || !*s)
    {
        fprintf(stderr, "String option value expected\n");
        print_usage_and_exit(this_prog);
    }

    return s;
}

int main(int argc, const char *argv[])
{
    int min_size = 0;
    int max_size = INT_MAX;
    int benchmark = 0;
    const char *model_path = NULL;
    const char *input_files[argc];
    int n_input_files = 0;
    int no_more_options = 0;
    OutputFormat output_format = OUTPUT_LINES; //OUTPUT_TESS_BOX;

    for (int i = 1; i < argc; i++)
    {
        // XXX use omm-options
        if (!no_more_options && argv[i][0] == '-' && argv[i][1])
        {
            if (!strcmp(argv[i], "--"))
                no_more_options = 1;
            else if (!strcmp(argv[i], "--min-size"))
                min_size = atoi_or_die(argv[0], argv[++i]);
            else if (!strcmp(argv[i], "--max-size"))
                max_size = atoi_or_die(argv[0], argv[++i]);
            else if (!strcmp(argv[i], "--boxes"))
                output_format = OUTPUT_TESS_BOX;
            else if (!strcmp(argv[i], "-m") || !strcmp(argv[i], "--model"))
                model_path = str_or_die(argv[0], argv[++i]);
            else if (!strcmp(argv[i], "-b") || !strcmp(argv[i], "--benchmark"))
                benchmark = 1; 
            else
            {
                fprintf(stderr, "Unrecognized option: %s\n\n", argv[i]);
                print_usage_and_exit(argv[0]);
            }
        }
        else
        {
            input_files[n_input_files++] = argv[i];
        }
    }

    if (!n_input_files)
    {
        fprintf(stderr, "No input files\n\n");
        print_usage_and_exit(argv[0]);
    }

    if (!model_path)
    {
        fprintf(stderr, "No model file given\n\n");
        print_usage_and_exit(argv[0]);
    }

    OmmModel *model = omm_model_new_from_file(model_path);
    if (!model)
        exit(EXIT_FAILURE); // error message printed by the constructor

    for (int i = 0; i < n_input_files; i++)
    {
        OmmTIFF *tiff = omm_tiff_open(input_files[i]);
        if (!tiff)
            exit(EXIT_FAILURE); // error message printed by omm_tiff_open()

        int n_pages = omm_tiff_get_n_pages(tiff);
        for (int page = 0; page < n_pages; page++)
        {
            OmmImage *rle = omm_tiff_read_rle(tiff, page);
            if (!rle)
                exit(EXIT_FAILURE); // error message printed by omm_tiff_read_rle()
            if (!omm_image_check(rle))
            {
                fprintf(stderr, "Invalid RLE image loaded from the TIFF file!\n");
                exit(EXIT_FAILURE);
            }
            if (!benchmark)
            {
                process_page(rle, model, page, 
                             min_size, max_size, 
                             /* report: */ 1, 
                             output_format);
            }
            else
            {
                fprintf(stderr, "The image is loaded and converted to RLE\n");
                omm_timestamp(stderr);
                fprintf(stderr, "Recognizing for 1 second...\n");
                int count = 0;
                while (omm_seconds_passed() < 1)
                {
                    process_page(rle, model, page, 
                                 min_size, max_size, 
                                 /* report: */ 0, 
                                 OUTPUT_NONE);
                    count++;
                }
                fprintf(stderr, "Finished\n");
                omm_timestamp_count(stderr, count);
            }
            omm_image_free(rle);
        }
        omm_tiff_close(tiff);
    }

    omm_model_free(model);
    return EXIT_SUCCESS;
}
