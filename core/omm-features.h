/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */


#ifndef OMM_FEATURES_H
#define OMM_FEATURES_H

/**
 * \file omm-features.h
 *
 * Feature extractor.
 */

#include "omm-rle.h"
#include "omm-labeling.h"

/**
 * Return the length of the feature vector, in integers.
 * That equals dimension plus 1 for the denominator.
 *
 * This is a constant, usually 73.
 */
int omm_feature_length(void);

/**
 * Extract features from a character image.
 */
int omm_extract_features_from_glyph(
    OmmImage *restrict image, 
    int *restrict bins);

/**
 * \brief Extract features.
 *
 * This is a fast, one-pass feature extraction routine 
 *     that processes the page as a whole.
 *
 * \param[in]  blob_infos  The array of blobs.
 *                         Only the bounding rectangle 
 *                           and the indices are used.
 *                         The index field of the blob
 *                           is the index of its feature vector in the output.
 *                         Blobs with index -1 are skipped.
 *                         Blobs with the same index must have equal 
 *                           bounding boxes as well.
 *
 * \param  n_blobs         The number of blobs in blob_infos[].
 *
 * \param  n_features      The number of expected output vectors.
 *                         Must be greater than all blobs' indices.
 * 
 * \param  image           The input image.
 *
 * \param  labels          A labeling as produced by omm_label().
 *                         The labels are indices into blob_infos[].
 *
 * \param[out]  bins        The extracted features.
 *                          If NULL, returns immediately.
 *
 * \returns     The memory (in integers) needed per glyph.
 *              Equals dimension + 1 (for the denumerator).
 *              Multiply by n_features to get total size of bins[].
 */
int omm_extract_features(
    const OmmBlobInfo *restrict blob_infos,
    int n_blobs,
    int n_features,
    const OmmImage *restrict image, 
    const uint32_t *restrict labels,
    int *restrict bins);

// ____________________________________________________________________________

// Segment the [x0: x0 + w (inclusive)] range into n bins (or "cells"),
// symmetrically and as uniformly as possible.
// Return the cell index corresponding to x.
//
//
int omm_dotgrid_cell_index(int x0, int w, int n, int x);

// ______________________   feature transformations   _________________________

void omm_features_flip_x(const int *restrict input, int *restrict output);
void omm_features_flip_y(const int *restrict input, int *restrict output);
void omm_features_flip_xy(const int *restrict input, int *restrict output);
void omm_features_rotate_180(const int *restrict input, int *restrict output);
void omm_features_rotate_cw(const int *restrict input, int *restrict output);
void omm_features_rotate_ccw(const int *restrict input, int *restrict output);
void omm_features_transpose(const int *restrict input, int *restrict output);

#endif
