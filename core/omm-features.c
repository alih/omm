/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */


#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <limits.h>
#include "omm-features.h"

enum { NX = 3, NY = 3 };
enum { NXY = NX * NY };
enum { N_DIRS = 8 };

/**
 * Get an unbroken run pointed to by *p and advance *p past its end.
 * If not for broken runs, this could be as simple as runs[(*p)++].
 */
static int omm_rle8_advance(const uint8_t *restrict runs, int n, int *restrict p)
{
    int result = runs[*p];
    (*p)++;
    n--;
    while ((*p) < n && !runs[*p])
    {
        result += runs[*p + 1];
        *p += 2;
    }
    return result;
}

// rose_bin() {{{

/*@ requires x >= 0 && y >= 0;
    requires !(x == y == 0);
    requires x < INT_MAX / 169;
    requires y < INT_MAX / 169;

    ensures 0 <= \result <= 2;
 */
static int rose_bin_q1(int x, int y)
{
    // we use this approximation: tan(pi/8) = 70./169 * 1.00003...
    return 169 * y <= 70 * x ? 0 :
           169 * x <= 70 * y ? 2 : 1;
}

/*@ requires y >= 0;
    requires !(x == y == 0);
    requires -INT_MAX / 169 < x < INT_MAX / 169;
    requires y < INT_MAX / 169;

    ensures 0 <= \result <= 4;
 */
static int rose_bin_q12(int x, int y)
{
    return x >= 0 ? rose_bin_q1(x, y) :
                    4 - rose_bin_q1(-x, y);
}

const int rose_fix_q12[] = {0 * NXY, 1 * NXY, 2 * NXY, 3 * NXY, 4 * NXY};
const int rose_fix_q34[] = {0 * NXY, 7 * NXY, 6 * NXY, 5 * NXY, 4 * NXY};
const int rose_fix_q14[] = {2 * NXY, 1 * NXY, 0 * NXY, 7 * NXY, 6 * NXY};
const int rose_fix_q23[] = {2 * NXY, 3 * NXY, 4 * NXY, 5 * NXY, 6 * NXY};

// rose_bin }}}

// the grid {{{

// This is some deeply magical shit for a really simple thing.
// TODO: doc
int omm_dotgrid_cell_index(int x0, int w, int n, int x)
{
    int t = 2 * (x - x0) - w;
    int s = t >> (sizeof(t) * 8 - 1);
    return (t * n + w - 1 - ((2 * (w - 1)) & s)) / (2 * w) + n / 2;
}

static int cell_index(int z0, int z1, int n_cells, int z_premultiplied)
{
    return (z_premultiplied - n_cells * z0) / (z1 - z0);
}

static int cell_min(int z0, int z1, int n_cells, int i)
{
    return n_cells * z0 + i * (z1 - z0);
}

typedef struct
{
    int x0;
    int x1;
    int y0;
    int y1;
} Grid;

static int grid_x_cell_index(Grid *grid, int x_premultiplied)
{
    return cell_index(grid->x0, grid->x1, NX, x_premultiplied);
}

static int grid_y_cell_index(Grid *grid, int y_premultiplied)
{
    return cell_index(grid->y0, grid->y1, NY, y_premultiplied);
}

static int grid_x_cell_min(Grid *grid, int i)
{
    return cell_min(grid->x0, grid->x1, NX, i);
}

static int grid_y_cell_min(Grid *grid, int i)
{
    return cell_min(grid->y0, grid->y1, NY, i);
}

static void grid_add_horizontal(Grid *grid, int coef, int x0, int x0i, int x1, int x1i, int *row)
{
    //printf("grid_add_horizontal(grid, %d, %d, %d, %d, %d, ...)\n", coef, x0, x0i, x1, x1i);
    if (x0i == x1i)
        row[x0i] += (x1 - x0) * coef;
    else
    {
        row[x0i] += (grid_x_cell_min(grid, x0i + 1) - x0) * coef;
        for (int i = x0i + 1; i < x1i; i++)
            row[i] += (grid->x1 - grid->x0) * coef;
        row[x1i] += (x1 - grid_x_cell_min(grid, x1i)) * coef;
    }
}

static void grid_add_vertical(Grid *grid, int coef, int y0, int y0i, int y1, int y1i, int *col)
{
    //printf("grid_add_vertical(grid, %d, %d, %d, %d, %d, ...)\n", coef, y0, y0i, y1, y1i);
    if (y0i == y1i)
        col[y0i * NX] += (y1 - y0) * coef;
    else
    {
        col[y0i * NX] += (grid_y_cell_min(grid, y0i + 1) - y0) * coef;
        for (int i = y0i + 1; i < y1i; i++)
            col[i * NX] += (grid->y1 - grid->y0) * coef;
        col[y1i * NX] += (y1 - grid_y_cell_min(grid, y1i)) * coef;
    }
}

static void grid_init_from_blob_info(
    Grid *restrict grid, 
    const OmmBlobInfo *restrict blob_info)
{
    grid->x0 = blob_info->min_x;
    grid->x1 = blob_info->max_x;
    grid->y0 = blob_info->min_y;
    grid->y1 = blob_info->max_y;
}

// the grid }}}

static void add_run_right(Grid *restrict grid, 
                          int *restrict bins,
                          /* int ax, = 0 */ int ay,
                          int bx, /* int by, = 0 */
                          /* int cx, = 0 */ int cy,
                          int x, int y)
{
    // printf("add_run_right(ay=%d, bx=%d, cy=%d, [x, y] = [%d, %d])\n", ay, bx, cy, x, y);
    int coef = NY * (grid->y1 - grid->y0);
    assert(bx > 0);
    int *row = bins + omm_dotgrid_cell_index(grid->y0, grid->y1 - grid->y0, NY, y) * NX;
    const int margin = 2;
    const int b_coef = 2;

    int bb = bx;
    const int *rose_fix = rose_fix_q12;
    int aby = ay;
    int bcy = cy;
    int ob = b_coef * bb;

    assert(aby && bcy); 

    int x0 = x * NX;
    int x0i = grid_x_cell_index(grid, x0);
    int x3 = (x + bb) * NX;
    int x3i = grid_x_cell_index(grid, x3);
    if (bb < 2 * margin)
    {
        // there's an area where margins overlap
        int oy = ay + cy;

        if (bb <= margin)
        {
            // the gradient is the same all along
            grid_add_horizontal(grid, coef, x0, x0i, x3, x3i,
                                row + rose_fix[rose_bin_q12(-oy, ob)]);
        }
        else
        {
            // margins overlap
            // int d = bb - margin;
            const int d = 1;
            int x1 = (x + d) * NX;
            int x2 = (x + margin) * NX;
            int x1i = grid_x_cell_index(grid, x1);
            int x2i = grid_x_cell_index(grid, x2);

            grid_add_horizontal(grid, coef, x0, x0i, x1, x1i, 
                                    row + rose_fix[rose_bin_q12(-aby, ob)]);
            grid_add_horizontal(grid, coef, x1, x1i, x2, x2i,
                                    row + rose_fix[rose_bin_q12(-oy, ob)]);
            grid_add_horizontal(grid, coef, x2, x2i, x3, x3i,
                                    row + rose_fix[rose_bin_q12(-bcy, ob)]);
        }
    }
    else
    {
        int x1 = (x + margin) * NX;
        int x2 = (x + bb - margin) * NX;
        int x1i = grid_x_cell_index(grid, x1);
        int x2i = grid_x_cell_index(grid, x2);
        grid_add_horizontal(grid, coef, x0, x0i, x1, x1i, row + rose_fix[rose_bin_q12(-aby, ob)]);
        if (bb > 2 * margin)
        {
            grid_add_horizontal(grid, coef, x1, x1i, x2, x2i, 
                                row + rose_fix[2]);
        }
        grid_add_horizontal(grid, coef, x2, x2i, x3, x3i,
                            row + rose_fix[rose_bin_q12(-bcy, ob)]);
    }
}

static void add_run_left(Grid *restrict grid, 
                         int *restrict bins,
                         /* int ax, = 0 */ int ay,
                         int bx, /* int by, = 0 */
                         /* int cx, = 0 */ int cy,
                         int x, int y)
{
    // printf("add_run_left(ay=%d, bx=%d, cy=%d, [x, y] = [%d, %d])\n", ay, bx, cy, x, y);
    int coef = NY * (grid->y1 - grid->y0);
    int *row = bins + omm_dotgrid_cell_index(grid->y0, grid->y1 - grid->y0, NY, y) * NX;
    const int margin = 2;
    const int b_coef = 2;

    assert(bx < 0);
    int bb = -bx;
    const int *rose_fix = rose_fix_q34;
    int aby = ay;
    int bcy = cy;
    int ob = b_coef * bb;

    assert(aby && bcy); 

    int x0 = (x - bb) * NX;
    int x0i = grid_x_cell_index(grid, x0);
    int x3 = x * NX;
    int x3i = grid_x_cell_index(grid, x3);
    if (bb < 2 * margin)
    {
        // there's an area where margins overlap
        int oy = ay + cy;

        if (bb <= margin)
        {
            // the gradient is the same all along
            grid_add_horizontal(grid, coef, x0, x0i, x3, x3i,
                                row + rose_fix[rose_bin_q12(-oy, ob)]);
        }
        else
        {
            // margins overlap
            // int d = bb - margin;
            const int d = 1;
            int x1 = (x - margin) * NX;
            int x2 = (x - d) * NX;
            int x1i = grid_x_cell_index(grid, x1);
            int x2i = grid_x_cell_index(grid, x2);

            grid_add_horizontal(grid, coef, x0, x0i, x1, x1i, 
                                row + rose_fix[rose_bin_q12(-bcy, ob)]);
            grid_add_horizontal(grid, coef, x1, x1i, x2, x2i,
                                row + rose_fix[rose_bin_q12(-oy, ob)]);
            grid_add_horizontal(grid, coef, x2, x2i, x3, x3i,
                                row + rose_fix[rose_bin_q12(-aby, ob)]);
        }
    }
    else
    {
        int x1 = (x + margin - bb) * NX;
        int x2 = (x - margin) * NX;
        int x1i = grid_x_cell_index(grid, x1);
        int x2i = grid_x_cell_index(grid, x2);
        grid_add_horizontal(grid, coef, x0, x0i, x1, x1i, 
                            row + rose_fix[rose_bin_q12(-bcy, ob)]);
        if (bb > 2 * margin)
        {
            grid_add_horizontal(grid, coef, x1, x1i, x2, x2i,
                                row + rose_fix[2]);
        }
        grid_add_horizontal(grid, coef, x2, x2i, x3, x3i,
                            row + rose_fix[rose_bin_q12(-aby, ob)]);
    }
}
/**
 * Register a run in the feature accumulator.
 * This needs 3 consecutive runs to work, with the middle one being registered.
 * 
 * \param x,y   The coordinates of the corner point between A and B runs.
 */
static void add_horizontal_run_to_rose(Grid *restrict grid, 
                                       int *restrict bins,
                                       /* int ax, = 0 */ int ay,
                                       int bx, /* int by, = 0 */
                                       /* int cx, = 0 */ int cy,
                                       int x, int y)
{
    if (bx > 0)
        add_run_right(grid, bins, ay, bx, cy, x, y);
    else
        add_run_left(grid, bins, ay, bx, cy, x, y);
}


static void add_run_down(Grid *restrict grid, 
                         int *restrict bins,
                         int ax, /* int ay, = 0 */
                         /*int bx,=0*/ int by,
                         int cx, /* int cy, = 0 */
                         int x, int y)
{
    // printf("add_run_down(ax=%d, by=%d, cx=%d, [x, y] = [%d, %d])\n", ax, by, cx, x, y);
    int coef = NX * (grid->x1 - grid->x0);

    assert(by > 0);
    bins += omm_dotgrid_cell_index(grid->x0, grid->x1 - grid->x0, NX, x);
    const int margin = 2;
    const int b_coef = 2;

    int bb = by;
    const int *rose_fix = rose_fix_q23;
    int abx = ax;
    int bcx = cx; 

    assert(abx && bcx); 

    int y0 = y * NY;
    int y0i = grid_y_cell_index(grid, y0);
    int y3 = (y + bb) * NY;
    int y3i = grid_y_cell_index(grid, y3);
    int ob = b_coef * bb;
    if (bb < 2 * margin)
    {
        // there's an area where margins overlap
        int ox = ax + cx;

        if (bb <= margin)
        {
            // the gradient is the same all along
            grid_add_vertical(grid, coef, y0, y0i, y3, y3i, bins + rose_fix[rose_bin_q12(ox, ob)]);
        }
        else
        {
            // margins overlap
            // int d = bb - margin;
            const int d = 1;
            int y1 = (y + d) * NY;
            int y2 = (y + margin) * NY;
            int y1i = grid_y_cell_index(grid, y1);
            int y2i = grid_y_cell_index(grid, y2);
            grid_add_vertical(grid, coef, y0, y0i, y1, y1i,
                              bins + rose_fix[rose_bin_q12(abx, ob)]);
            grid_add_vertical(grid, coef, y1, y1i, y2, y2i,
                              bins + rose_fix[rose_bin_q12(ox, ob)]);
            grid_add_vertical(grid, coef, y2, y2i, y3, y3i,
                              bins + rose_fix[rose_bin_q12(bcx, ob)]);
        }
    }
    else
    {
        int y1 = (y + margin) * NY;
        int y2 = (y + bb - margin) * NY;
        int y1i = grid_y_cell_index(grid, y1);
        int y2i = grid_y_cell_index(grid, y2);
        grid_add_vertical(grid, coef, y0, y0i, y1, y1i,
                          bins + rose_fix[rose_bin_q12(abx, ob)]);
        if (bb > 2 * margin)
        {
            grid_add_vertical(grid, coef, y1, y1i, y2, y2i,
                              bins + rose_fix[2]);
        }
        grid_add_vertical(grid, coef, y2, y2i, y3, y3i,
                          bins + rose_fix[rose_bin_q12(bcx, ob)]);
    }
}

static void add_run_up(Grid *restrict grid, 
                                     int *restrict bins,
                                     int ax, /* int ay, = 0 */
                                     /*int bx,=0*/ int by,
                                     int cx, /* int cy, = 0 */
                                     int x, int y)
{
    // printf("add_run_up(ax=%d, by=%d, cx=%d, [x, y] = [%d, %d])\n", ax, by, cx, x, y);
    int coef = NX * (grid->x1 - grid->x0);

    assert(by < 0);
    bins += omm_dotgrid_cell_index(grid->x0, grid->x1 - grid->x0, NX, x);
    const int margin = 2;
    const int b_coef = 2;

    int bb = -by;
    const int *rose_fix = rose_fix_q14;
    int abx = ax;
    int bcx = cx; 

    assert(abx && bcx); 

    int ob = b_coef * bb;
    int y0 = (y - bb) * NY;
    int y0i = grid_y_cell_index(grid, y0);
    int y3 = y * NY;
    int y3i = grid_y_cell_index(grid, y3);
    if (bb < 2 * margin)
    {
        // there's an area where margins overlap
        int ox = ax + cx;

        if (bb <= margin)
        {
            // the gradient is the same all along
            grid_add_vertical(grid, coef, y0, y0i, y3, y3i,
                              bins + rose_fix[rose_bin_q12(ox, ob)]);
        }
        else
        {
            // margins overlap
            // int d = bb - margin;
            const int d = 1;
            int y1 = (y - margin) * NY;
            int y2 = (y - d) * NY;
            int y1i = grid_y_cell_index(grid, y1);
            int y2i = grid_y_cell_index(grid, y2);
            grid_add_vertical(grid, coef, y0, y0i, y1, y1i,
                              bins + rose_fix[rose_bin_q12(bcx, ob)]);
            grid_add_vertical(grid, coef, y1, y1i, y2, y2i,
                              bins + rose_fix[rose_bin_q12(ox, ob)]);
            grid_add_vertical(grid, coef, y2, y2i, y3, y3i,
                              bins + rose_fix[rose_bin_q12(abx, ob)]);
        }
    }
    else
    {
        int y1 = (y + margin - bb) * NY;
        int y2 = (y - margin) * NY;
        int y1i = grid_y_cell_index(grid, y1);
        int y2i = grid_y_cell_index(grid, y2);
        grid_add_vertical(grid, coef, y0, y0i, y1, y1i,
                          bins + rose_fix[rose_bin_q12(bcx, ob)]);
        if (bb > 2 * margin)
        {
            grid_add_vertical(grid, coef, y1, y1i, y2, y2i,
                              bins + rose_fix[2]);
        }
        grid_add_vertical(grid, coef, y2, y2i, y3, y3i,
                          bins + rose_fix[rose_bin_q12(abx, ob)]);
    }
}

// max bin: nx * ny * (2wh+w+h)
// if w = h, nx = ny = n, it's 2 * n**2 * (w ** 2 + w)

/**
 * A Triplet is a 3-segment fragment of a contour
 * that starts on a horizontal line and doesn't get below it.
 * A triplet can be open or closed.
 * Here's how open triplets might look:
 *
 *
 *         |
 *       __|
 *      |          ___
 *      |         |   |
 * _____|_____________|______
 *
 *
 * Here's a closed triplet:
 *
 *        ___
 *       |   |
 * ______|___|_____________
 *
 * 
 * Note that a closed triplet is represented by 2 Triplet structs.
 * We'll call that those structs complementary.
 *
 * There's an ownership relationship between triplets and segments,
 * namely an open triplet owns first 2 segments (dy and dx),
 * and a closed triplet owns all 3.
 *
 * As we scan the image from top to bottom,
 * we register some segments with the grid.
 * We're going to maintain the following invariant:
 *
 *  All segments above the sweeping line are either
 *       owned by exactly one triplet 
 *    or registered with the grid exactly once.
 *
 * So ownership, informally, is a promise to register 
 * once the length of 2 neighboring segments is determined.
 * 
 */ 

// Triplet structure
//              . <- (xp,yp)
//              |
// (x,y) -> .___|
//          |   
//      ____|____

typedef struct
{
    int x;
    int y;
    int xp;
    int yp;
} Triplet;

/*
 * Ownership of the middle segment (O - the triplet origin):
 *   ___
 *  |   |
 *  |       yes: the leg that reaches further down owns the middle 
 * _O_____
 *
 *   ___
 *  |   |
 *  |   |   no: the triplet with the rightmost origin owns it
 * _O___|_
 *
 *   ___
 *  |   |  yes: this is the rightmost origin triplet
 * _|___O_
 *
 *   ___
 *  |   |
 * _|___O_  no: the other leg is already prolonged, so it reaches further down
 *  |
 */
static int triplet_owns_middle(const Triplet *triplet, int y, int has_prolonged_complement)
{
    if (triplet->yp != y)
        return 1;
    if (has_prolonged_complement)
        return 0;    
    return triplet->xp < triplet->x;
}

// triplet stack {{{

/*
 * If we come to a closed triplet that needs to be prolonged on both ends,
 *            ____
 *           |    |
 * __________|____|__________
 *           |    |
 *
 * then we need to modify 2 Triplet structures and prolong both dy and dyp.
 *
 * That's why we need the stack.
 * As we prolong the left end of a closed triplet,
 * we push it the stack.
 * Since the contours can't intersect,
 * the order in which we meet closed triplets is indeed FIFO.
 *
 * When we come to a closed triplet from the right,
 *   | it's not on the stack => it has been opened from the left
 *   | it's on the stack =>
 *       | we prolong it => prolong in both Triplet structs
 *       | we open it
 */

static void triplet_stack_init(int *top)
{
    *top = -2;
}

static void triplet_stack_push(int *restrict stack,
                               int *restrict top,
                               int y,
                               int i, 
                               int x,
                               const Triplet *restrict triplet)
{
    if (triplet->yp == y && triplet->xp > x)
    {
        *top += 2;
        stack[*top] = i;
        stack[*top + 1] = triplet->xp;
    }
}

static void triplet_stack_pop(int *p_top)
{
    *p_top -= 2;
}

static int triplet_stack_peek_i(const int *stack, int top)
{
    return stack[top];
}

static int triplet_stack_top_x_is(const int *stack, int top, int x)
{
    return top >= 0 && stack[top + 1] == x;
}


// triplet stack }}}


// register_{cup, cap, zigzag} {{{

/* There are 3 kinds of horizontal segments:
 *
 *                  _____
 *      |_____|    |     |    ______|  |______     
 *                           |                |
 *        cup        cap           zigzag
 */

/* A cap is the easiest to register.
 */
static void register_cap(
    Triplet *restrict left,
    Triplet *restrict right,
    int left_x, int right_x, int y)
{
    left->x = left_x;
    left->y = y;
    left->xp = right_x;
    left->yp = y + 1;
    
    right->x = right_x;
    right->y = y;
    right->xp = left_x;
    right->yp = y + 1;
}

/* Register the middle segment of the given triplet.
 */
static void register_triplet_middle(
    Grid *restrict grid,
    int *restrict bins,
    const Triplet *restrict triplet,
    int y,
    int has_prolonged_complement,
    int going_down)
{
    if (!triplet_owns_middle(triplet, y, has_prolonged_complement))
        return;

    // register the horizontal segment of the upper
    if (going_down)
    {
        add_horizontal_run_to_rose(grid, bins,                         
                                triplet->y - triplet->yp,
                                triplet->x - triplet->xp,
                                y - triplet->y,
                                triplet->xp, triplet->y);
    }
    else
    {
        add_horizontal_run_to_rose(grid, bins,                         
                                triplet->y - y,
                                triplet->xp - triplet->x,
                                triplet->yp - triplet->y,
                                triplet->x, triplet->y);
    }

}


static void register_zigzag(
    Grid *restrict grid,
    int *restrict bins,
    const Triplet *restrict upper,
    Triplet *restrict lower,
    int has_complement,
    int lower_x, 
    int y, 
    int going_down)
{
    lower->x = lower_x;
    lower->y = y;
    lower->xp = upper->x;
    lower->yp = upper->y;

    // register the vertical segment of the upper
    if (going_down)
    {
        add_run_down(grid, bins,                         
                                upper->x - upper->xp,
                                y - upper->y,
                                lower_x - upper->x,
                                upper->x, upper->y);
    }
    else
    {
        add_run_up(grid, bins,                         
                                upper->x - lower_x,
                                upper->y - y,
                                upper->xp - upper->x,
                                upper->x, y);
    }

    register_triplet_middle(grid, bins, upper,
                            y, has_complement, going_down);
}

// register_rectangle {{{
/*
 * This is a special case of a cup: the bottom of a rectangle.
 * Since the other part is a closed triplet, none of the segments are registered.
 * So this function registers all 4.
 *
 * If the rectangle is black, then (x,y) is the top left point;
 * if the rectangle is white, then width and height should be negative 
 *  and (x,y) is the bottom right point.
 */
static void register_positive_rectangle(
    Grid *restrict grid,
    int *restrict bins,
    int width,
    int height,
    int x,
    int y)
{
    assert(width > 0);
    assert(height > 0);
    add_run_down(grid, bins, -width, height, width, x, y);
    add_run_right(grid, bins, height, width, -height, x, y + height);
    add_run_up(grid, bins, width, -height, -width, x + width, y + height);
    add_run_left(grid, bins, -height, -width, height, x + width, y);
}
static void register_negative_rectangle(
    Grid *restrict grid,
    int *restrict bins,
    int width,
    int height,
    int x,
    int y)
{
    assert(width < 0);
    assert(height < 0);
    add_run_left(grid, bins, -height, width, height, x, y);
    add_run_up(grid, bins, width, height, -width, x + width, y);
    add_run_right(grid, bins, height, -width, -height, x + width, y + height);
    add_run_down(grid, bins, -width, -height, width, x, y + height);
}
// register_rectangle }}} 

static void register_cup(
    Grid *restrict grid,
    int *restrict bins,
    const Triplet *restrict left,
    int left_has_complement,
    const Triplet *restrict right,
    int right_has_complement,
    int y,
    int going_right)
{
    if (left->x == right->xp && left->xp == right->x)
    {
        assert(left->y == right->y);
        if (going_right)
        {
            // the rectangle is black
            register_positive_rectangle(grid, bins,
                               right->x - left->x,
                               y - left->y,
                               left->x, left->y);
        }
        else
        {
            register_negative_rectangle(grid, bins,
                               left->x - right->x,
                               left->y - y,
                               right->x, y);
        }
        return;
    }

    register_triplet_middle(grid, bins, left, y, left_has_complement, going_right);
    register_triplet_middle(grid, bins, right, y, right_has_complement, !going_right);

    // register the horizontal segment that is the cup bottom
    // and two adjacent vertical segments
    if (going_right)
    {
        add_run_down(grid, bins, 
                                left->x - left->xp,
                                y - left->y,
                                right->x - left->x,
                                left->x, left->y);
        add_run_right(grid, bins,                         
                                y - left->y,
                                right->x - left->x,
                                right->y - y,
                                left->x, y);
        add_run_up(grid, bins, 
                                right->x - left->x,
                                right->y - y,
                                right->xp - right->x,
                                right->x, y);
    }
    else
    {
        add_run_down(grid, bins, 
                                right->x - right->xp,
                                y - right->y,
                                left->x - right->x,
                                right->x, right->y);
        add_run_left(grid, bins,
                                y - right->y,
                                left->x - right->x,
                                left->y - y,
                                right->x, y);
        add_run_up(grid, bins, 
                                left->x - right->x,
                                left->y - y,
                                left->xp - left->x,
                                left->x, y);
    }
}

// register_{cup, cap, zigzag} }}}


static void fe_rose_single_line_multi(
    Grid *restrict grids,
    int *restrict bins,
    int dimension,
    const uint32_t *restrict labels,
    const OmmBlobInfo *restrict blobs,
    const uint8_t *restrict runs,
    int n_runs)
{
    int i = 0;
    int x = 0;
    while (i < n_runs)
    {
        x += omm_rle8_advance(runs, n_runs, &i);
        int black_run_start = x;
        uint32_t label = labels[(i - 1) / 2];
        int index = blobs[label].index;
        x += omm_rle8_advance(runs, n_runs, &i);
        if (x != black_run_start && index >= 0)
            register_positive_rectangle(&grids[label], bins + dimension * index, 
                           /* width: */ x - black_run_start, /* height: */ 1, 
                           /* left: */ black_run_start, /* top: */ 0);
    }
}

static void fe_rose_first_line(
    Triplet *restrict triplets,
    const uint8_t *restrict runs,
    int n_runs)
{
    int i = 0;
    int x = 0;
    while (i < n_runs)
    {
        x += omm_rle8_advance(runs, n_runs, &i);
        int left_triplet = i;
        int black_run_start = x;
        x += omm_rle8_advance(runs, n_runs, &i);
        if (x != black_run_start)
            register_cap(&triplets[left_triplet], &triplets[i], black_run_start, x, 0);
    }
}


static void fe_rose_last_line_multi(
    Grid *restrict grids,
    int *restrict bins,
    int dimension,
    const uint32_t *restrict labels,
    const OmmBlobInfo *restrict blobs,
    const Triplet *restrict triplets,
    const uint8_t *restrict runs,
    int n_runs,
    int image_height)
{
    int i = 0;
    int x = 0;
    while (i < n_runs)
    {
        x += omm_rle8_advance(runs, n_runs, &i);
        int left_x = x;
        int left_i = i;
        x += omm_rle8_advance(runs, n_runs, &i);
        if (x == left_x)            
            continue;
        uint32_t label = labels[(left_i - 1) / 2];
        int index = blobs[label].index;
        if (index < 0)
            continue;

        assert(left_x == triplets[left_i].x);
        assert(x == triplets[i].x);
        register_cup(&grids[label], bins + index * dimension,
                     &triplets[left_i], 0,
                     &triplets[i], 0,
                     image_height, /* going_right: */ 1);
    }
}

/**
 * A step between lines.
 *
 * \param grid              The grid helping us to store features into the bins.
 * \param bins              The accumulator for the features.
 * \param current_triplets  The triplet line we're building in this function.
 * \param upper_triplets    The previous triplet line.
 *
 * \param stack             A temporary workspace holding at least current_n_runs ints.
 */
static void fe_rose_step_multi(
    Grid *restrict grids,
    int *restrict bins,
    int dimension,
    const uint32_t *restrict current_labels,
    const uint32_t *restrict upper_labels,
    const OmmBlobInfo *restrict blobs,
    Triplet *restrict current_triplets,
    const Triplet *restrict upper_triplets,
    const uint8_t *restrict current_runs,
    int current_n_runs,
    const uint8_t *restrict upper_runs,
    int upper_n_runs,
    int *restrict stack,
    int y)
{
    int stack_top;
    triplet_stack_init(&stack_top);

    int i = 0; // cursor in current_runs
    int j = 0; // cursor in upper_runs

    /*
     * We have 6 cases of what could happen in the iteration:
     * 
     *  @@@@@@  .@@@@@  ......
     *  @.....  ......  ......
     *  dx > 0  dx > 0  dx = 0
     *  dy < 0  dy > 0
     *
     *  @.....  ......  @@@@@@
     *  @@@@@@  .@@@@@  @@@@@@
     *  dx < 0  dx < 0  dx = 0
     *  dy < 0  dy > 0
     *
     * Now the iteration meets one of 3 cases:
     *   - change in the upper line
     *   - change in the current line
     *   - both.
     * 
     */ 

    // skip initial white runs
    int xi = omm_rle8_advance(current_runs, current_n_runs, &i);
    int xj = omm_rle8_advance(upper_runs, upper_n_runs, &j);

    while (1) // the main loop
    {
        assert(((i ^ j) & 1) == 0); // the upper and the current runs have the same color

        if (xi == xj) // We prolong a vertical edge {{{
        {
            // this branch ends with 'continue'

            if (i < current_n_runs && current_runs[i] == 0)
            {
                assert(j < upper_n_runs && upper_runs[j] == 0);
                break;
            }

            int32_t label = blobs[current_labels[(i - 1) / 2]].index;
            assert(label == blobs[upper_labels[(j - 1) / 2]].index);
            
            //    ..@        @@.
            //    ..@   or   @@.
            
            if (label >= 0)
            {
                assert(upper_triplets[j].x == xi);
                current_triplets[i].x = xi;
                current_triplets[i].y = upper_triplets[j].y;
                current_triplets[i].xp = upper_triplets[j].xp;

                int has_complement = triplet_stack_top_x_is(stack, stack_top, xi);
                if (has_complement)
                {
                    int complement = triplet_stack_peek_i(stack, stack_top);
                    triplet_stack_pop(&stack_top);

                    current_triplets[complement].yp = y + 1;
                    current_triplets[i].yp = y + 1;
                }
                else
                {
                    current_triplets[i].yp = upper_triplets[j].yp;
                    triplet_stack_push(stack, &stack_top, y, i, xi, &current_triplets[i]);
                }
            }

            // advance both i and j

            assert((i < current_n_runs  && j < upper_n_runs)
                || (i == current_n_runs && j == upper_n_runs));
           
            if (i == current_n_runs)
                break;

            xi = xi + omm_rle8_advance(current_runs, current_n_runs, &i);
            xj = xj + omm_rle8_advance(upper_runs, upper_n_runs, &j);

            // now the upper and the current runs have the same color again
            continue;
        } // xi == xj }}}

        // remainder of the main loop: follow a horizontal edge

        int left_has_complement = 0;
        int right_has_complement = 0;
        int left_from_below = 0;
        int left_triplet_index;
        int left_x;
        uint32_t label;
        int index;

        if (xi < xj)
        {
            // ...      @@@
            // ..@  or  @@.

            label = current_labels[(i - 1) / 2];
            index = blobs[label].index;

            left_from_below = 1;
            left_x = xi;
            left_triplet_index = i;

            assert(i <= current_n_runs);

            xi += omm_rle8_advance(current_runs, current_n_runs, &i);
        }
        else
        {
            // ..@      @@.
            // ...  or  @@@
            
            label = upper_labels[(j - 1) / 2];
            index = blobs[label].index;

            left_triplet_index = j;

            if (index >= 0)
            {
                left_has_complement = triplet_stack_top_x_is(stack, stack_top, xj);
                if (left_has_complement)
                    triplet_stack_pop(&stack_top);
            }

            left_x = xj;

            assert(j <= upper_n_runs);
            xj += omm_rle8_advance(upper_runs, upper_n_runs, &j);
        }

        assert(((i ^ j) & 1) == 1); // the upper and the current runs have different color
        int going_right = i & 1;

        // the end of our horizontal edge
        if (xi < xj || ((xi == xj) && (j & 1)))
        {
            assert(i <= current_n_runs);

            if (index >= 0)
            {
                if (left_from_below)
                {
                    // cap
                    register_cap(&current_triplets[left_triplet_index],
                                 &current_triplets[i],
                                 left_x, xi, y);
                }
                else
                {
                    // zigzag
                    register_zigzag(&grids[label], bins + index * dimension, 
                                    &upper_triplets[left_triplet_index],
                                    &current_triplets[i],
                                    left_has_complement, /* lower_x: */ xi,
                                    y, /* going down: */ going_right);
                }
            }

            if (i == current_n_runs)
                break;
            xi += omm_rle8_advance(current_runs, current_n_runs, &i);
        }
        else
        {
            if (index >= 0)
            {
                right_has_complement = triplet_stack_top_x_is(stack, stack_top, xj);
                if (right_has_complement)
                    triplet_stack_pop(&stack_top);

                assert(j <= upper_n_runs);
                if (left_from_below)
                {
                    // zigzag
                    register_zigzag(&grids[label], bins + index * dimension, &upper_triplets[j],
                                                &current_triplets[left_triplet_index],
                                                right_has_complement, /* lower_x: */ left_x,
                                                y, /* going down: */ !going_right);
                }
                else
                {
                    // cup
                    register_cup(&grids[label], bins + index * dimension, &upper_triplets[left_triplet_index],
                                             left_has_complement,
                                             &upper_triplets[j],
                                             right_has_complement,
                                             y, going_right);
                }
            }

            if (j == upper_n_runs)
                break;
            
            xj += omm_rle8_advance(upper_runs, upper_n_runs, &j);
        }
    } // main loop
}

int omm_feature_length()
{
    return NXY * N_DIRS + 1;
}

int omm_extract_features(
    const OmmBlobInfo *restrict blob_infos,
    int n_blobs,
    int n_features,
    const OmmImage *restrict rle, 
    const uint32_t *restrict labels,
    int *restrict bins)
{
    assert(omm_image_check(rle));

    int dimension = omm_feature_length();

    memset(bins, 0, dimension * n_features * sizeof(int)); 
        
    Grid *grids = (Grid *) malloc(n_blobs * sizeof(Grid));
    for (int i = 0; i < n_blobs; i++)
    {
        grid_init_from_blob_info(&grids[i], &blob_infos[i]);
        int index = blob_infos[i].index;
        if (index >= 0)
        {
            bins[index * dimension] = 
                (blob_infos[i].max_x - blob_infos[i].min_x) 
              * (blob_infos[i].max_y - blob_infos[i].min_y);
        }
    }
    bins++;

    if (rle->height == 1)
    {
        fe_rose_single_line_multi(grids, bins, dimension, 
                                  labels, blob_infos, rle->runs, rle->n_runs);
        free(grids);
        return dimension;
    }
    
    /*
     * m is the max number of between-run borders per line.
     * (== 2 * number of black runs == number of all runs
     *  because every black run causes 2 between-run borders)
     */
    int m = omm_image_longest_line_in_runs(rle->line_offsets, rle->height);

    int *stack = (int *) malloc(2 * m * sizeof(int));

    Triplet *triplets1 = (Triplet *) malloc((m + 1) * sizeof(Triplet));
    Triplet *triplets2 = (Triplet *) malloc((m + 1) * sizeof(Triplet));
    fe_rose_first_line(triplets1, rle->runs, rle->line_offsets[1]);

    Triplet *current_triplets = triplets1;
    Triplet *upper_triplets = triplets2;
    int *offsets = rle->line_offsets;
    for (int y = 1; y < rle->height; y++)
    {
        Triplet *t = current_triplets;
        current_triplets = upper_triplets;
        upper_triplets = t;
        
        fe_rose_step_multi(grids, bins, dimension, 
                     labels + offsets[y] / 2,
                     labels + offsets[y - 1] / 2, 
                     blob_infos,
                     current_triplets, upper_triplets,
                     rle->runs + offsets[y], offsets[y + 1] - offsets[y],
                     rle->runs + offsets[y - 1], offsets[y] - offsets[y - 1],
                     stack, y);
    }
    fe_rose_last_line_multi(grids, bins, dimension,
                      labels + offsets[rle->height - 1] / 2,
                      blob_infos,
                      current_triplets, 
                      rle->runs + offsets[rle->height - 1], 
                      offsets[rle->height] - offsets[rle->height - 1],
                      rle->height); 

    free(triplets1);
    free(triplets2);
    free(stack);
    free(grids);

    return dimension;
}

int omm_extract_features_from_glyph(
    OmmImage *restrict rle, 
    int *restrict bins)
{
    uint32_t *labels = (uint32_t *) calloc(rle->n_runs / 2, sizeof(uint32_t));

    OmmBlobInfo blob_info;
    blob_info.min_x = 0;
    blob_info.min_y = 0;
    blob_info.max_x = rle->width;
    blob_info.max_y = rle->height;
    blob_info.index = 0;

    int result = omm_extract_features(&blob_info, 1, 1, rle, labels, bins);
    free(labels);

    return result;
}

// ______________________   feature transformations   ______________________________

void omm_features_flip_x(const int *restrict input, int *restrict output)
{
    assert(input != output);
    *output++ = *input++; // skip the denominator
    char dirmap[] = {4, 3, 2, 1, 0, 7, 6, 5};
    for (int dir = 0; dir < N_DIRS; dir++)
    {
        int *out = output + dir * NXY;
        const int *in = input + dirmap[dir] * NXY;
        for (int y = 0; y < NY; y++)
            for (int x = 0; x < NX; x++)
                out[y * NX + x] = in[y * NX + NX - 1 - x];
    }
}

void omm_features_flip_y(const int *restrict input, int *restrict output)
{
    assert(input != output);
    *output++ = *input++; // skip the denominator
    char dirmap[] = {0, 7, 6, 5, 4, 3, 2, 1};
    for (int dir = 0; dir < N_DIRS; dir++)
    {
        int *out = output + dir * NXY;
        const int *in = input + dirmap[dir] * NXY;
        for (int y = 0; y < NY; y++)
            for (int x = 0; x < NX; x++)
                out[y * NX + x] = in[(NY - 1 - y) * NX + x];
    }
}

void omm_features_rotate_180(const int *restrict input, int *restrict output)
{
    assert(input != output);
    *output++ = *input++; // skip the denominator
    char dirmap[] = {4, 5, 6, 7, 0, 1, 2, 3};
    for (int dir = 0; dir < N_DIRS; dir++)
    {
        int *out = output + dir * NXY;
        const int *in = input + dirmap[dir] * NXY;
        for (int y = 0; y < NY; y++)
            for (int x = 0; x < NX; x++)
                out[y * NX + x] = in[(NY - 1 - y) * NX + NX - 1 - x];
    }
}

void omm_features_flip_xy(const int *restrict input, int *restrict output)
{
    omm_features_rotate_180(input, output);
}

void omm_features_rotate_ccw(const int *restrict input, int *restrict output)
{
    assert(input != output);
    *output++ = *input++; // skip the denominator
    char dirmap[] = {2, 3, 4, 5, 6, 7, 0, 1};
    for (int dir = 0; dir < N_DIRS; dir++)
    {
        int *out = output + dir * NXY;
        const int *in = input + dirmap[dir] * NXY;
        for (int y = 0; y < NY; y++)
            for (int x = 0; x < NX; x++)
                out[y * NX + x] = in[x * NX + NY - 1 - y];
    }
}

void omm_features_rotate_cw(const int *restrict input, int *restrict output)
{
    assert(input != output);
    *output++ = *input++; // skip the denominator
    char dirmap[] = {6, 7, 0, 1, 2, 3, 4, 5};
    for (int dir = 0; dir < N_DIRS; dir++)
    {
        int *out = output + dir * NXY;
        const int *in = input + dirmap[dir] * NXY;
        for (int y = 0; y < NY; y++)
            for (int x = 0; x < NX; x++)
                out[y * NX + x] = in[(NY - 1 - x) * NX + y];
    }
}

void omm_features_transpose(const int *restrict input, int *restrict output)
{
    assert(input != output);
    *output++ = *input++; // skip the denominator
    char dirmap[] = {2, 1, 0, 7, 6, 5, 4, 3};
    for (int dir = 0; dir < N_DIRS; dir++)
    {
        int *out = output + dir * NXY;
        const int *in = input + dirmap[dir] * NXY;
        for (int y = 0; y < NY; y++)
            for (int x = 0; x < NX; x++)
                out[y * NX + x] = in[x * NX + y];
    }
}

// vim: set foldmethod=marker:
