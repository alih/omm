/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#ifndef OMM_ENDIAN_H
#define OMM_ENDIAN_H

/**
 * \file omm-endian.h
 * A little helper to deal with endianness issues.
 */

#include <stdint.h>

/**
 * The endianness markers are chosen so that can be written into files
 * in any endianness. They also look recognizable in the files ('>>>>', '<<<<').
 */
typedef enum
{
    OMM_BIG_ENDIAN    = '>' * 0x1010101,
    OMM_LITTLE_ENDIAN = '<' * 0x1010101
} OmmEndianness;

/**
 * Determine this machine's endianness.
 */
OmmEndianness omm_endianness(void);

/**
 * Convert an array of 32-bit integers 
 * from the given endianness to the native one.
 */
void omm_import_endian_32(uint32_t *, int n, OmmEndianness foreign);

/**
 * Convert an array of 32-bit integers
 * from the native endianness to the given one.
 * (Actually, does the same thing as import)
 */
void omm_export_endian_32(uint32_t *, int n, OmmEndianness foreign);

#endif
