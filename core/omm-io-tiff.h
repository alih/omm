/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#ifndef OMM_IO_TIFF_H
#define OMM_IO_TIFF_H

#include "omm-image.h"

/**
 * \file omm-io-tiff.h
 * Reading bi-level TIFF images.
 */

/**
 * Read a TIFF file and return a RLE image.
 * If the TIFF is compressed using either CCITT Group 3 or Group 4 encoding,
 *     then reading is fast because RLE is read from the file directly.
 * Otherwise, RLE has to be extracted from full pixel rows.
 *
 * Only bi-level images are supported.
 * Tiled TIFFs are not supported.
 *
 * In case of error, a message is printed to stderr and NULL is returned.
 */
OmmImage *omm_tiff_read_first_page_rle(const char *path);

typedef struct OmmTIFF OmmTIFF;

OmmTIFF *omm_tiff_open(const char *path);
int omm_tiff_get_n_pages(const OmmTIFF *);
OmmImage *omm_tiff_read_rle(OmmTIFF *, int page_no);
int omm_tiff_write(OmmImage *, const char *path);

unsigned char *omm_tiff_read_bitmap(
    OmmTIFF *,
    int page_no,
    int *p_width,
    int *p_height);

void omm_tiff_close(OmmTIFF *);

#endif
