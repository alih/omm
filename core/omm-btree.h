/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#ifndef OMM_BTREE_H
#define OMM_BTREE_H

/**
 * \file omm-btree.h
 *
 * \brief B-trees.
 *
 * An implementation of B-trees with insertion only (no removing).
 * A B-tree stores pointers and compares them through a user-provided callback.
 */

typedef struct OmmBTree OmmBTree;

/**
 * Create a B-tree with default parameters.
 * Call omm_btree_set_max_children() immediately afterwards 
 *     to override the default settings.
 * The given compare function should be strcmp-like.
 */
OmmBTree *omm_btree_new(int (*compare_func)(const void *, const void *));

/**
 * Call the given function on each pointer stored in the tree, in order.
 *
 * \param data  An opaque pointer passed to visitor().
 */
void omm_btree_foreach(OmmBTree *, void (*visitor)(void *, void *data), void *data);

/**
 * Set the parameter of the B-tree:
 *     the maximum number of children in a non-leaf node.
 * The maximum number of children of a leaf will be twice as much. 
 * This call is only possible on an empty tree.
 * The minimal sensible value is 3.
 */
void omm_btree_set_max_children(OmmBTree *, int);

/**
 * Insert a new pointer into the tree or find it already there.
 * Returns either the given pointer or a previously stored one.
 *
 * The pointer is not owned by the tree.
 */
void *omm_btree_insert(OmmBTree *, void *query);

/**
 * Find a pointer in the tree.
 * Returns a pointer from the tree or NULL.
 */
void *omm_btree_find(OmmBTree *, void *query);

/**
 * Same as omm_btree_insert(), except that if it inserts,
 * then it uses dup(query) instead of query.
 * dup() may cancel insertion by returning NULL.
 */
void *omm_btree_insert_dup(OmmBTree *, void *query, void *(*dup)(void *));

/**
 * Free the B-tree, not touching the user data pointers.
 * If they need to be freed also, use omm_btree_foreach().
 */
void omm_btree_free(OmmBTree *);

#endif
