/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */


#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <assert.h>
#include "omm-labeling.h"
#include "omm-io-tiff.h"
#include "omm-io-model.h"
#include "omm-features.h"
#include "omm-median.h"
#include "omm-layout.h"
#include "omm-ocr.h"


int omm_linear_ova(
    const int *restrict features,
    const int32_t *restrict weights,
    int n_classes,
    int dimension,
    int *restrict scores)
{
    int best = -1;
    double best_score = 0;
    for (int c = 0; c < n_classes; c++)
    {
        const int32_t *row = weights + c * dimension;
        int score = 0;
        for (int i = 0; i < dimension; i++)
            score += row[i] * features[i];

        if (scores)
            scores[c] = score;

        if (best < 0 || score > best_score)
        {
            best = c;
            best_score = score;
        }
    }
    return best;
}
static int filter_blobs(
    OmmBlobInfo *blobs, int n_blobs, 
    int min_size, int max_size)
{
    int n_samples = 0;
    for (int i = 0; i < n_blobs; i++)
    {
        int w = blobs[i].max_x - blobs[i].min_x;
        int h = blobs[i].max_y - blobs[i].min_y;
        if (w >= min_size && h >= min_size 
         && w <= max_size && h <= max_size)
            blobs[i].index = n_samples++;
        else
            blobs[i].index = -1;
    }
    return n_samples;
}


OmmPage *omm_page_new(OmmImage *rle, int min_size, int max_size)
{
    OmmPage *page = (OmmPage *) malloc(sizeof(OmmPage));
    page->labels = (uint32_t *) malloc(rle->n_runs / 2 * sizeof(uint32_t));
    unsigned n_blobs = page->n_blobs = omm_label(page->labels, rle); 
#ifndef NDEBUG
    for (int i = 0; i < rle->n_runs / 2; i++)
        assert(rle->runs[2 * i + 1] == 0 || page->labels[i] < n_blobs);
#endif
    page->blobs = (OmmBlobInfo *) malloc(n_blobs * sizeof(OmmBlobInfo));
    omm_analyze_blobs(page->blobs, n_blobs, page->labels, rle);

    // filter the blobs
    page->n_samples = filter_blobs(page->blobs, n_blobs, min_size, max_size);
    page->layout = omm_la_by_projection_profiles(page->blobs, n_blobs, 
                                                 rle->width, rle->height);
    return page; 
}

int *omm_page_extract_features(OmmPage *page, OmmImage *rle)
{
    int dim = omm_feature_length();
    int *features = (int *) malloc(dim * page->n_samples * sizeof(int));
    omm_extract_features(page->blobs, page->n_blobs, 
                         page->n_samples, rle, page->labels, features);
    return features;
}

void omm_page_free(OmmPage *page)
{
    free(page->labels);
    free(page->blobs);
    omm_layout_free(page->layout);
    free(page);
}


// _______________________________   model   _________________________________

/**
 * All the information contained in the saved model,
 * with pointers to meaningful parts of it.
 *
 * A wrapper over OmmModelReader.
 */
struct OmmModel
{
    OmmModelReader *reader; // owned
    const uint32_t *data;     // unowned
    const char *strpool;      // unowned
    const int32_t *weights; // unowned
    const char **classmap; // owned (this pointer only, without content)

    size_t length;

    uint32_t model_version;
    uint32_t n_classes;
    uint32_t dimension;
};

/**
 * See also: output_default() in main-omm-train.c
 */
OmmModel *omm_model_new_from_file(const char *path)
{
    return omm_model_new(omm_model_reader_new_from_file(path));
}

OmmModel *omm_model_new_from_data(const void *data, size_t length)
{
    return omm_model_new(omm_model_reader_new_from_data(data, length));
}

static void model_init(OmmModel *m)
{
    const uint32_t *p = m->data;
    m->model_version = *p++;
    m->n_classes = *p++;
    m->dimension = *p++;
    m->weights = (const int32_t *) p;
    p += m->dimension * m->n_classes; 
    m->classmap = (const char **) malloc(m->n_classes * sizeof(const char *));
    for (unsigned i = 0; i < m->n_classes; i++)
        m->classmap[i] = m->strpool + p[i];
}

OmmModel *omm_model_new(OmmModelReader *reader)
{
    if (!reader)
        return NULL;

    OmmModel *m = (OmmModel *) malloc(sizeof(OmmModel));
    m->reader = reader;
    m->data = omm_model_reader_get_storage(reader);
    m->length = omm_model_reader_get_storage_length(reader);
    m->strpool = omm_model_reader_get_string_pool(reader);

    model_init(m);

    return m;
}

OmmModel *omm_model_new_from_hardcoded(
    const uint32_t *storage, 
    size_t length,
    const char *strpool)
{
    OmmModel *m = (OmmModel *) malloc(sizeof(OmmModel));
    m->reader = NULL;
    m->data = storage;
    m->length = length;
    m->strpool = strpool;

    model_init(m);

    return m;
}

void omm_model_free(OmmModel *m)
{
    // the reader should be freed in the end 
    // because the pointers may be mmapped
    if (m->reader)
        omm_model_reader_free(m->reader);
    free(m->classmap);
    free(m);
}

static OmmRecChar *new_rec_chars(OmmPage *page)
{
    OmmBlobInfo *blobs = page->blobs;
    int n_blobs = page->n_blobs; 
    int n_samples = page->n_samples;
    int *blob_lines = page->layout->blob_lines;
    OmmRecChar *rec_chars = (OmmRecChar *) malloc(n_samples * sizeof(OmmRecChar));
    int *index_to_label = (int *) malloc(n_samples * sizeof(int));
    for (int i = 0; i < n_blobs; i++)
    {
        if (blobs[i].index >= 0)
            index_to_label[blobs[i].index] = i;
    }
    for (int i = 0; i < n_samples; i++)
    {
        int label = index_to_label[i];
        rec_chars[i].blob = &blobs[label];
        rec_chars[i].line = blob_lines[label];
    }
    free(index_to_label);
    return rec_chars;
}

OmmRecChar *omm_ocr(OmmModel *model, OmmImage *rle, OmmPage *page)
{
    OmmRecChar *rec_chars = new_rec_chars(page);
    int dim = omm_feature_length();
    int *features = omm_page_extract_features(page, rle);

    for (int i = 0; i < page->n_samples; i++)
    {
        int best = omm_linear_ova(features + i * dim + 1, 
                                  model->weights, 
                                  model->n_classes, 
                                  model->dimension, NULL); 
        rec_chars[i].text = model->classmap[best];
    }

    free(features);
    return rec_chars;
}
