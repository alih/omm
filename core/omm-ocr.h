/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */


#ifndef OMM_OCR_H
#define OMM_OCR_H

#include "omm-layout.h"

typedef struct
{
    uint32_t *labels;
    int n_blobs;
    int n_samples;
    OmmBlobInfo *blobs;
    OmmLayout *layout;
} OmmPage;

OmmPage *omm_page_new(OmmImage *rle, int min_size, int max_size);
int *omm_page_extract_features(OmmPage *page, OmmImage *rle);
void omm_page_free(OmmPage *page);

typedef struct OmmModel OmmModel;

OmmModel *omm_model_new(OmmModelReader *reader);
OmmModel *omm_model_new_from_file(const char *path);
OmmModel *omm_model_new_from_data(const void *data, size_t length);
OmmModel *omm_model_new_from_hardcoded(
    const uint32_t *storage, 
    size_t length, 
    const char *strpool);
void omm_model_free(OmmModel *m);

/**
 * Classify using a one-vs-all linear model without bias.
 * \param[in]  features  The query (an array of length dimension)
 * \param[in]  weights   The n_classes x dimension matrix of model weights.
 * \param[out] scores    The scores of the query point in each of the classes 
 *                        (may be NULL).
 * \returns              The class with the maximum score.
 *
 */
int omm_linear_ova(
    const int *restrict features,
    const int32_t *restrict weights,
    int n_classes,
    int dimension,
    int *restrict scores);

typedef struct
{
    const char *text; // owned by an OmmModel
    int line;
    OmmBlobInfo *blob; // owned by an OmmPage
} OmmRecChar;

OmmRecChar *omm_ocr(OmmModel *model, OmmImage *image, OmmPage *page);

#endif
