/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <tiffio.h>
#include "omm-io-tiff.h"
#include "omm-rle.h"


struct OmmTIFF
{
    TIFF *tiff;
    int n_pages;
};


/**
 * This is a callback that libtiff feeds RLE data to. 
 * KLUGE ALERT: buf actually points to our RLE image.
 */
static void fill(uint8_t *buf, uint32 *runs32, uint32 *erun32, uint32 width)
{    
    OmmImage *rle = (OmmImage *) buf; // here clang complains but it will work
                                      // (the buf is alloc'd by _TIFFmalloc,
                                      // so the alignment won't be a problem)
                                      // (did I mention that libtiff is a mess?)

    int row = rle->line_offsets[0]; // another kluge!
    uint8_t *runs = rle->runs + rle->n_runs;
    int count = omm_rle_32_to_8(runs, width, runs32, erun32 - runs32);
    if (row)
        rle->line_offsets[row] = rle->n_runs;
    rle->line_offsets[0]++; // keeping the row index in line_offsets[0]
    rle->n_runs += count;
}

static void fallback(OmmImage *rle, uint8_t *packed, int width, int row, int white)
{
    uint8_t *runs = rle->runs + rle->n_runs;
    rle->line_offsets[row] = rle->n_runs;
    rle->n_runs += omm_rle_extract_packed(runs, packed, width, white);
}


static const char *check_tif(TIFF *tif)
{
    uint32 samples_per_pixel = 1;
    uint32 bits_per_sample = 1; 

    TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &bits_per_sample);
    TIFFGetField(tif, TIFFTAG_SAMPLESPERPIXEL, &samples_per_pixel);
    
    if (samples_per_pixel != 1 || bits_per_sample != 1)
        return "Only bi-level TIFF images are supported";

    return NULL;
}

OmmImage *omm_tiff_read_first_page_rle(const char *path)
{
    OmmTIFF *tiff = omm_tiff_open(path);
    if (!tiff)
        return NULL;

    OmmImage *result = omm_tiff_read_rle(tiff, 0);
    omm_tiff_close(tiff);
    return result;
}

OmmImage *omm_tiff_read_rle(OmmTIFF *omm_tiff, int page_no)
{
    TIFF *tif = omm_tiff->tiff;

    if (page_no < 0 || page_no >= omm_tiff->n_pages)
    {
        fprintf(stderr, "Invalid page number: %d (expected between 0 and %d)\n",
                         page_no, omm_tiff->n_pages-1);
        return NULL;
    }
    TIFFSetDirectory(tif, page_no);
    const char *error = check_tif(tif);

    if (error)
    {
        fprintf(stderr, "%s\n", error);
        return NULL;
    }

    void *fill_func;
    if (TIFFGetField(tif, TIFFTAG_FAXFILLFUNC, &fill_func))
    {
        TIFFSetField(tif, TIFFTAG_FAXFILLFUNC, fill);
    }

    uint32 width;
    uint32 height;
    TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &width);
    TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &height);

    OmmImage *rle = omm_image_new(width, height);
    
    int photometric;
    if (!TIFFGetField(tif, TIFFTAG_PHOTOMETRIC, &photometric))
    {
        photometric = PHOTOMETRIC_MINISWHITE;
    }

    size_t buf_size = TIFFScanlineSize(tif);
    if (buf_size < sizeof(OmmImage))
        buf_size = sizeof(OmmImage);
    tdata_t buf = _TIFFmalloc(buf_size);
    
    /* kluge: we make a shallow copy within the buffer */
    rle->line_offsets[0] = 0;
    memcpy(buf, rle, sizeof(OmmImage));
    TIFFReadScanline(tif, buf, 0, 0);
    
    if (rle->line_offsets[0])
    {
        /* Hooray, our fill() was called */
        for (uint32 y = 1; y < height; y++)
            TIFFReadScanline(tif, buf, y, 0);
        rle->line_offsets[0] = 0;
        memcpy(rle, buf, sizeof(OmmImage));
        // FIXME: photometric is ignored here!
    }
    else
    {
        /* Falling back to RLE extraction */
        int white = photometric == PHOTOMETRIC_MINISWHITE ? 0 : 1;
        fallback(rle, buf, width, 0, white);
        for (uint32 y = 1; y < height; y++)
        {
            TIFFReadScanline(tif, buf, y, 0);
            fallback(rle, buf, width, y, white);
        }
    }
    rle->line_offsets[height] = rle->n_runs;

    _TIFFfree(buf);

    return rle;
}

// XXX
// this could be a straightforward implementation
unsigned char *omm_tiff_read_bitmap(
    OmmTIFF *tiff,
    int page_no,
    int *p_width,
    int *p_height)
{
    OmmImage *rle = omm_tiff_read_rle(tiff, page_no);
    if (!rle)
        return NULL;

    *p_width = rle->width;
    *p_height = rle->height;
    uint8 *result = (uint8 *) malloc(rle->width * rle->height);
    omm_rle_render(result, rle->width * rle->height, rle->runs, rle->n_runs);
    omm_image_free(rle);
    return result;
}

OmmTIFF *omm_tiff_open(const char *path)
{
    OmmTIFF *result = (OmmTIFF *) malloc(sizeof(OmmTIFF));
    if (!result)
        return NULL;
    result->tiff = TIFFOpen(path, "r");
    if (!result->tiff)
    {
        fprintf(stderr, "Unable to open TIFF file: %s\n", path);
        free(result);
        return NULL;
    }
    result->n_pages = TIFFNumberOfDirectories(result->tiff);
    return result;
}

int omm_tiff_get_n_pages(const OmmTIFF *tiff)
{
    return tiff->n_pages;
}

void omm_tiff_close(OmmTIFF *tiff)
{
    if (!tiff)
        return;
    if (tiff->tiff)
        TIFFClose(tiff->tiff);
    free(tiff);
}

int omm_tiff_write(OmmImage *image, const char *path)
{
    TIFF *tiff = TIFFOpen(path, "w");
    if (!tiff)
        return 0;

    TIFFSetField(tiff, TIFFTAG_IMAGEWIDTH, image->width);
    TIFFSetField(tiff, TIFFTAG_IMAGELENGTH, image->height);
    TIFFSetField(tiff, TIFFTAG_BITSPERSAMPLE, 1);
    TIFFSetField(tiff, TIFFTAG_SAMPLESPERPIXEL, 1);
    TIFFSetField(tiff, TIFFTAG_COMPRESSION, COMPRESSION_CCITTFAX4);
    TIFFSetField(tiff, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISWHITE);
    TIFFSetField(tiff, TIFFTAG_ROWSPERSTRIP, 256);
    
    size_t buf_size = TIFFScanlineSize(tiff);
    tdata_t buf = _TIFFmalloc(buf_size);
    memset(buf, 0, buf_size);
    
    for (int i = 0; i < image->height; i++)
    {
        // Unfortunately we don't have a way to hack through this
        // like FILLFUNC for reading.
        omm_rle_render_to_packed((uint8_t *) buf, 
                       image->width, 
                       image->runs + image->line_offsets[i],
                       image->line_offsets[i + 1] - image->line_offsets[i]);
        if (TIFFWriteScanline(tiff, buf, i, 0) < 0)
        {
            TIFFError(NULL, "while writing to `%s'", path);
            break;
        }
    }
    _TIFFfree(buf);
    TIFFClose(tiff);
    return 1;
}
