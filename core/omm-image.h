/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#ifndef OMM_IMAGE_H
#define OMM_IMAGE_H

#include <stdint.h>

/**
 * \file omm-image.h
 * Run-length encoded images.
 */

/**
 * \brief A RLE image.
 *
 * An OmmImage has a single stream of runs,
 * and the individual lines are addressed with line_offsets.
 * The line number i corresponds to runs[line_offsets[i] : line_offsets[i + 1]].
 * line_offsets is an array of height + 1 elements,
 * and it's always true that line_offsets[0] = 0 
 *    and line_offsets[height] = n_runs. 
 * Each line has an even number of runs. Therefore, all line_offsets are even.
 */
typedef struct
{
    uint8_t *runs;
    int *line_offsets;
    int n_runs;
    int width;
    int height;
} OmmImage;

/**
 * Create a new RLE image with the given size.
 */ 
OmmImage *omm_image_new(int width, int height);

/**
 * Extract the RLE image from a raster binary image.
 */
OmmImage *omm_image_new_from_raster(
    const unsigned char *pixels, 
    int w, 
    int h);

/**
 * Threshold an image and simultaneously extract the RLE representation.
 * \param threshold The threshold, positive or negative, 
 *                  as in omm_rle_threshold_and_extract() 
 * \see omm_rle_threshold_and_extract
 */
OmmImage *omm_image_new_from_grayscale(
    const unsigned char *pixels, 
    int w, 
    int h, 
    int stride,
    int threshold);

/**
 * Free the RLE image.
 */
void omm_image_free(OmmImage *);

/**
 * Check that the RLE is consistent,
 * mainly that the rows sum up to the declared width.
 */
int omm_image_check(const OmmImage *rle);

/**
 * Return the maximal length of a line, measured in runs.
 * This is the same as the biggest difference in the offsets array.
 * FIXME: make a better interface for this
 */
int omm_image_longest_line_in_runs(const int *offsets, int height);

/**
 * Crop an RLE image. Free the result with omm_image_free() as usual.
 * x0 and y0 are inclusive, x1 and y1 are exclusive.
 */
OmmImage *omm_image_crop(OmmImage *image, int x0, int y0, int x1, int y1);

/**
 * Decode the RLE representation.
 */
void omm_image_render(
    const OmmImage *restrict image, 
    uint8_t *restrict pixels, 
    int w, int h, int stride);

#endif
