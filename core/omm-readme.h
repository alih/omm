// This header contains only the Doxygen comments.

/**
 * \mainpage
 *
 * \section Overview
 *
 * OMM, which stands for Ocromancer Mobile (Ocromancer is my other OCR project),
 * is a minimalistic OCR engine that sacrifices everything for speed.
 * OMM is about two orders of magnitude faster than Tesseract. 
 *
 * The accuracy is slightly better in version 0.2, and I have even seen an instance 
 * where OMM trained on roughly 500 character samples competed with stock Tesseract in 
 * accuracy. That can only happen on single font problems. 
 *
 * OMM is too minimalistic to recognize non-connected characters (i, j). If you need 
 * something way more accurate at a moderate (2-3 times) speed cost, check out GNU ocrad. 
 * Note, however, that OMM is trainable, while ocrad isn't.
 *
 * OMM is meant to be used in situations where the output needs to make just some statistic sense.
 * For example, you might want to apply it twice to check if a page is upside down. 
 * Or check a bunch of pages to see if a book contains more Latin or Cyrillic characters.
 * Or try 10 thresholds to binarize the image and see which one works best.
 * Or run it on every video frame to search for a particular word. Or use it to recognize near-perfect images.
 * It's your call.
 *
 *
 * OMM has a very simple structure: find connected components, find lines (skew detection supported),
 * extract features, classify. It operates completely in RLE domain, and can process G4-compressed TIFF
 * files without ever fully uncompressing them.
 *
 * \section Installation
 *
 * OMM uses "waf" build system. Just do
 *
 *     ./waf configure
 *     ./waf
 * 
 * You can follow this with 
 *     ./waf install 
 * or use the programs from build/ subdirectory.
 *
 * Use
 *
 *     ./waf debug
 *
 * to run tests and compile a version for debugging.
 *
 * \section Usage
 *
 * OMM accepts TIFF inputs and works faster if they are G4-compressed. A model file (*.omm) is required.
 * Use like this:
 *
 *     omm -m digits.omm foo.tif
 *
 * \section Training
 *
 * OMM can be trained with data in Tesseract's boxtiff format.
 * OMM uses a simple linear classifier and relies on separate training tools
 * such as LIBLINEAR. 
 *
 * Note that:
 *    - OMM doesn't scale up well with too many classes
 *    - It's better to split a class with several shape variations into several classes
 *    - OMM doesn't handle disconnected characters
 *
 *
 * 1. Download and compile LIBLINEAR from http://www.csie.ntu.edu.tw/~cjlin/liblinear/
 * 2. Obtain some Tesseract-compatible training data. You could go to 
 *        https://code.google.com/p/tesseract-ocr/downloads/list
 *    and download, for example, boxtiff-2.01.eng.tar.gz or directly with this link:
 *        https://tesseract-ocr.googlecode.com/files/boxtiff-2.01.eng.tar.gz
 * 3. Prepare a classmap file: a text file with a class label on each line. 
 *    To recognize digits, try this: 
 *
 *        seq 0 9 >digits.classmap
 *
 *    You can get the list of all the characters in the training data like this:
 *
 *        cat /path/to/data/*.box | sed 's/\s.*$//' | sort -u >all.classmap
 *
 *    but it's probably going to give more characters than OMM can nicely handle.
 * 4. Compile OMM. Make sure you can run omm-train1 and omm-train2 by either installing
 *    them system-wide or setting LD_LIBRARY_PATH=build.
 * 5. Feature Extraction: run omm-train1 to extract training data features. Here's an example:
 *
 *        omm-train1 -c digits.classmap -o digits.features /path/to/data
 *
 * 6. Training an SVM: Use `train' from LIBLINEAR to train a multiclass (-s 4) linear SVM.
 *    You might want to try several values of C (-c) to come up with the best accuracy.
 *    You can check accuracy with cross-validation (-v number_of_folds).
 *    Here's an example:
 *
 *        /wherever/liblinear-1.91/train -s 4 -c 0.5 -v 5 digits.feats 
 *        /wherever/liblinear-1.91/train -s 4 -c 1 -v 5 digits.feats
 *        /wherever/liblinear-1.91/train -s 4 -c 2 -v 5 digits.feats
 *        /wherever/liblinear-1.91/train -s 4 -c 1 digits.feats digits.svm
 *
 * 7. Making a Model: use omm-train2 to pack the SVM into a model, for example:
 *
 *        omm-train2 -c digits.classmap digits.svm -o digits.omm
 *
 *    If it says something like "'MCSVM_CS' expected", 
 *    you've probably forgot to make a multiclass SVM (-s 4).
 */
