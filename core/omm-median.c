/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */


#include <stdlib.h>
#include "omm-median.h"

/*
 * This is QuickSelect that I took from 
 *    http://ndevilla.free.fr/median/median/index.html
 * and adapted a little bit.
 * This original comment:
 *
 *  This Quickselect routine is based on the algorithm described in
 *  "Numerical recipes in C", Second Edition,
 *  Cambridge University Press, 1992, Section 8.5, ISBN 0-521-43108-5
 *  This code by Nicolas Devillard - 1998. Public domain.
 */


#define ELEM_SWAP(a,b) { int t=(a);(a)=(b);(b)=t; }

int omm_median(int *arr, int n) 
{
    int middle, ll, hh;

    int low = 0;
    int high = n - 1; 
    int median = (low + high) / 2;

    while (1)
    {
        if (high <= low) /* One element only */
            return arr[median];

        if (high == low + 1)   /* Two elements only */
        {
            if (arr[low] > arr[high])
                ELEM_SWAP(arr[low], arr[high]);
            return arr[median];
        }

        /* Find median of low, middle and high items; swap into position low */
        middle = (low + high) / 2;
        if (arr[middle] > arr[high])    ELEM_SWAP(arr[middle], arr[high]);
        if (arr[low] > arr[high])       ELEM_SWAP(arr[low], arr[high]);
        if (arr[middle] > arr[low])     ELEM_SWAP(arr[middle], arr[low]);

        /* Swap low item (now in position middle) into position (low+1) */
        ELEM_SWAP(arr[middle], arr[low+1]);

        /* Nibble from each end towards middle, swapping items when stuck */
        ll = low + 1;
        hh = high;
        while (1)
        {
            do ll++; while (arr[low] > arr[ll]);
            do hh--; while (arr[hh]  > arr[low]);

            if (hh < ll)
                break;

            ELEM_SWAP(arr[ll], arr[hh]);
        }

        /* Swap middle item (in position low) back into correct position */
        ELEM_SWAP(arr[low], arr[hh]);

        /* Re-set active partition */
        if (hh <= median)
            low = ll;
        if (hh >= median)
            high = hh - 1;
    } // while(1)
}

int omm_guess_text_size(OmmBlobInfo *blobs, int n)
{
    int *a = (int *) malloc(n * sizeof(int));
    int i;
    int c = 0;
    int result;
    for (i = 0; i < n; i++)
    {
        OmmBlobInfo *b = &blobs[i];
        int w = b->max_x - b->min_x;
        int h = b->max_y - b->min_y;
        if (b->n_black_runs > h
         && w < 8 * h 
         && h < 8 * w)
        {
            a[c] = h;
            c++;
        }
    }
    result = (c > 0 ? omm_median(a, c) : 0);
    free(a);
    return result;
}
