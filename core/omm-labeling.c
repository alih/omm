/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "omm-labeling.h"

/**
 * \file omm-label.c
 *
 * This is RLE-based connected component labeling for binary images.
 *
 * Parts of this code are adapted from Camellia's RLE labeling,
 * which attributes the original idea to CMU.
 * (Camellia is accessible at http://camellia.sourceforge.net).
 *
 * The 2-pass algorithm is classic, I believe.
 * Let us assume we have a binary image with N runs. Since white and black runs
 * are strictly alternating in our encoding, we have exactly N/2 black runs.
 *
 * We will maintain a forest on nonzero black runs.
 * The forest is stored in an array of length N/2 called `forest'.
 * Items of `forest' correspond to black runs in this way:
 *
 *      forest[i] corresponds to runs[2 * i + 1]
 *
 * The values corresponding to zero runs
 *     are meaningless and should not be accessed.
 * The value of forest[i] is an index of a nonzero run.
 *
 * 
 * The `forest' array must satisfy this "monotonicity" condition:
 *
 *    forest[i] <= i    for all i such that runs[2 * i + 1] != 0
 *
 * This condition forces the forest structure on nonzero runs.
 * The vertices for which forest[i] == i are the roots of this forest.
 * 
 */


/**
 * Given a forest where forest[i] == i for the roots,
 * find a root of the given vertex.
 * Implements path compression heuristic.
 */
static uint32_t find_root(uint32_t *forest, uint32_t p)
{
    uint32_t orig_p = p;
    while (p != forest[p])
        p = forest[p]; 
    uint32_t result = p;
    p = orig_p;
    while (p != forest[p])
    {
        uint32_t t = forest[p];
        forest[p] = result;
        p = t;
    }
    return result;
}

/**
 * Given a fully built forest (see the comments to this file),
 * make all the nodes point to the roots.
 * 
 * This is the second scan of labeling.
 */
static int second_scan(
    uint32_t *restrict forest,
    const uint8_t *restrict runs,
    int n_runs)
{
    int count = 0;
    uint32_t i;
    uint32_t n = n_runs / 2;

    // invariant: count <= i
    //    indeed, an iteration always increments i
    //            and sometimes increments count
    //
    // invariant: all the forest[] items before i
    //            store the indices of the connected components
    for (i = 0; i < n; i++)
    {
        if (!runs[2 * i + 1])
            continue;

        if (forest[i] == i)
        {
            // We have found a new component. Relabel it.
            forest[i] = count++;
        }
        else
        {
            assert(forest[i] < i); // because the forest satisfies monotonicity
            // thus, forest[forest[i]] is the CC label because of invariant
            assert(runs[2 * forest[i] + 1]); // forest can't point to zero runs
            forest[i] = forest[forest[i]];
        }
    }
    return count;
}

/**
 * Given a monotonic forest,
 * merge two trees containing a and b by making one root point to another.
 */
static void merge(uint32_t *forest, uint32_t a, uint32_t b)
{
    a = find_root(forest, a);
    b = find_root(forest, b);

    if(a > b)
        forest[a] = b;
    else
        forest[b] = a;
}

/**
 * Label CCs in a line.
 * 
 * \returns  The number of CCs found.
 */
static int label_1d(
    uint32_t *restrict forest,
    const uint8_t *restrict runs,
    int n_runs,
    int initial_count)
{
    // OK, I admit this is ridiculously complicated because of broken runs.
    int count = initial_count;  // the number of CCs met so far
    int cursor = 0;
    int cursor_cap = n_runs / 2;
    int last_white_run = 1; // any nonzero will do

    // find the first nonzero black run
    while (cursor < cursor_cap && !runs[cursor * 2 + 1])
        cursor++;

    if (cursor >= cursor_cap)
        return count;

    // now runs[cursor * 2 + 1] is a nonzero black run

    while (1)
    {
        if (last_white_run)
        {
            forest[cursor] = cursor;
            count++;
        }
        else
            forest[cursor] = cursor - 1;


        do // find the next nonzero black run
        {
            cursor++;
            if (cursor >= cursor_cap)
                return count;

            last_white_run = runs[cursor * 2];
        } while (!runs[cursor * 2 + 1]);
    }
    return count;    
}


/**
 * Create a forest of the runs such that each tree in it corresponds to a CC.
 * This is the traditional first scan.
 *
 * This code is roughly based on camRLELabelling() from Camellia.
 */
static void first_scan(
    uint32_t *restrict forest,
    const uint8_t *restrict runs,
    int n_runs,
    int width,
    int height,
    const int *restrict line_offsets)
{
    assert(width > 0);
    assert(height > 0);

    label_1d(forest, runs, line_offsets[1], 0);
    // At this point, we already have a monotonic forest.

    if (height == 1)
        return; // abort before dangerous computation of e1


    // We run 2 cursors through the image,
    // the "1" cursor is one line lower than the "2".
    int l1 = line_offsets[1] / 2, l2 = 0; // offsets of the cursors in pairs
    int x1 = runs[line_offsets[1]], x2 = runs[0]; // run starts
    int e1 = x1 + runs[line_offsets[1] + 1];      // run ends
    int e2 = x2 + runs[1];
    int cursor_cap = n_runs / 2;

    int s = -1; // the last matched run from the first cursor

    // This is the check-move lockstep loop.
    // Invariant:
    //   - all touching runs less then l1 belong to the same tree in the forest
    //   - l1 connects to nothing yet if s != l1 (therefore no merge required)
    while (l1 < cursor_cap)
    {
        // ------ check ------
        if (x1 != e1 && x2 != e2 // both runs are nonzero
        && ((e1 <= e2 && x2 < e1) || (e2 <= e1 && x1 < e2)))
        {
            // modify the forest to reflect the touch
            if (s == l1)
                merge(forest, l1, l2);
            else
            {
                // l1 connects to nothing yet
                assert(l2 < l1);
                forest[l1] = forest[l2];
                s = l1;
            }
        }

        // ------ move ------
	if (e1 < e2)
        {
            if (s != l1)
                forest[l1] = l1; // it was not in the forest

            // OK, this looks much less ugly in Camellia,
            // because they don't have broken runs.
            if (l1 + 1 >= cursor_cap)
                break;
            x1 = e1 + runs[++l1 * 2];
            if (!runs[2 * l1] && e1 % width != 0)
            {
                // broken white run, but not a line break
                forest[l1] = forest[l1 - 1];
                s = l1;
            }
            e1 = x1 + runs[l1 * 2 + 1];
	}
        else
        {
            x2 = e2 + runs[++l2 * 2];
            e2 = x2 + runs[l2 * 2 + 1];
	}
    }
}


int omm_label(uint32_t *forest, const OmmImage *rle)
{
    first_scan(
        forest,
        rle->runs,
        rle->n_runs,
        rle->width,
        rle->height,
        rle->line_offsets);

    return second_scan(forest, rle->runs, rle->n_runs);
}

static void blob_info_init(OmmBlobInfo *b, int w, int h)
{
    b->n_black_runs = 0;
    b->mass = 0;
    b->min_x = w;
    b->max_x = 0;
    b->min_y = h;
    b->max_y = 0;
}

static void blob_info_update(OmmBlobInfo *b, int y, int x0, int x1)
{
    b->n_black_runs++;
    b->mass += x1 - x0;
    if (x0 < b->min_x)
        b->min_x = x0;
    if (x1 > b->max_x)
        b->max_x = x1;
    if (y < b->min_y)
        b->min_y = y;
    if (y >= b->max_y)
        b->max_y = y + 1;
}

void omm_analyze_blobs(
    OmmBlobInfo *restrict blobs,
    int n_blobs,
    const uint32_t *restrict labels, 
    const OmmImage *restrict rle)
{
    const int w = rle->width;
    const int h = rle->height;

    for (int i = 0; i < n_blobs; i++)
        blob_info_init(&blobs[i], w, h);

    for (int y = 0; y < h; y++)
    {
        int x = 0;
        const uint32_t *row_labels = labels + rle->line_offsets[y] / 2;
        const int n = (rle->line_offsets[y + 1] - rle->line_offsets[y]) / 2;
        const uint8_t *runs = rle->runs + rle->line_offsets[y];
        for (int i = 0; i < n; i++)
        {
            x += runs[2 * i]; // white run
            if (runs[2 * i + 1]) // black run
            {
                int new_x = x + runs[2 * i + 1];
                assert(row_labels[i] < (unsigned) n_blobs);
                blob_info_update(blobs + row_labels[i], y, x, new_x);
                x = new_x;
            }
        }
    } 
}

void omm_render_multi(
    uint8_t *restrict *restrict images,
    OmmBlobInfo *restrict blobs,
    const uint32_t *restrict labels,
    const OmmImage *restrict rle)
{
    const int w = rle->width;
    const int n = rle->n_runs / 2;
    int x = 0, y = 0;

    for (int i = 0; i < n; i++)
    {
        x += rle->runs[2 * i];
        int black_run = rle->runs[2 * i + 1];
        if (black_run)
        {
            OmmBlobInfo *blob = &blobs[labels[i]];
            int image_w = blob->max_x - blob->min_x;
            uint8_t *p = images[labels[i]] + (y - blob->min_y) * image_w + x - blob->min_x;
            for (int j = 0; j < black_run; j++)
                p[j] = 1;
            x += black_run;
        }

        if (x == w)
        {
            x = 0;
            y++;
        }
    }
}

void omm_render_components_onto(
    uint8_t *restrict dst,
    int dst_width,
    int dst_height,
    OmmBlobInfo *restrict blobs,
    const int *restrict sx,
    const int *restrict sy,
    const OmmImage *restrict rle,
    const uint32_t *restrict labels)
{
    (void) dst_height;
    const int w = rle->width;
    const int n = rle->n_runs / 2;
    int x = 0, y = 0;

    for (int i = 0; i < n; i++)
    {
        x += rle->runs[2 * i];
        int black_run = rle->runs[2 * i + 1];
        if (black_run)
        {
            OmmBlobInfo *blob = &blobs[labels[i]];
            if (blob->index >= 0)
            {
                int dst_x = sx[blob->index] + x - blob->min_x;
                int dst_y = sy[blob->index] + y - blob->min_y;
                if (dst_y < dst_height)
                {
                    uint8_t *p = dst + dst_y * dst_width + dst_x;
                    int run = dst_width - dst_x;
                    if (black_run < run)
                        run = black_run;
                    for (int j = 0; j < run; j++)
                        p[j] = 1;
                }
            }
            x += black_run;
        }

        if (x == w)
        {
            x = 0;
            y++;
        }
    }
}

int omm_label_and_analyze_blobs(
    uint32_t **restrict p_labels,
    OmmBlobInfo **restrict p_blobs,
    const OmmImage *restrict rle)
{
    uint32_t *labels = (uint32_t *) malloc(rle->n_runs / 2 * sizeof(uint32_t));
    int n = omm_label(labels, rle);
    OmmBlobInfo *blobs = (OmmBlobInfo *) malloc(n * sizeof(OmmBlobInfo));
    omm_analyze_blobs(blobs, n, labels, rle);
    *p_blobs = blobs;
    *p_labels = labels;
    return n;
}
