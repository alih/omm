/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#ifndef OMM_LAYOUT_H
#define OMM_LAYOUT_H

#include "omm-labeling.h"

typedef struct
{
    int n_lines;
    int n_blobs;
    int skew_angle_num;
    int skew_angle_den; // XXX change to log_2
    int *line_intercepts; // (start, end) pairs
    int *blob_lines;
    int *line_blobs;
    int *line_offsets;
} OmmLayout;

OmmLayout *omm_la_by_projection_profiles(OmmBlobInfo *blobs, int n_blobs, int width, int height);
void omm_layout_free(OmmLayout *layout);

#endif 
