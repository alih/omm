/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include "omm-btree.h"

typedef int (*Comparator)(const void *, const void *);

/** A binary search with a slightly convoluted interface.
 *
 * \returns     A negative value i - n if an exact match was found at index i;
 *              otherwise a nonnegative value from 0 to n (incl.)
 *              showing the position of the query 
 *              should it be inserted into the array.
 */
static int omm_binary_search(
    const void *query, 
    void **data, 
    int n, 
    Comparator compare_func)
{
    int l = 0;
    int r = n;
    while (r > l)
    {
        int m = (l + r) / 2;
        int c = compare_func(query, data[m]);
        if (c == 0)
            return m - n;
        if (c < 0)
            r = m;
        else
            l = m + 1;
    }
    return l;
}


/**
 * A node is an array of 2k pointers, treated thus:
 *   1 length, k-1 bounds, k children for a non-leaf;
 *   2k values for a leaf; if length <= 2k - 2, then
 *      the last but one is NULL, the last is length
 */
typedef void **Node;

#define NONLEAF_KEYS(NODE) ((NODE) + 1)
#define NONLEAF_CHILDREN(NODE, K) ((NODE) + (k))

struct OmmBTree
{
    Node root;
    int k;
    int depth;
    Comparator compare_func;
};

OmmBTree *omm_btree_new(int (*compare_func)(const void *, const void *))
{
    OmmBTree *result = (OmmBTree *) malloc(sizeof(OmmBTree));
    result->root = NULL;
    result->k = 3;
    result->depth = -1;
    result->compare_func = compare_func;
    return result;
}

void omm_btree_set_max_children(OmmBTree *tree, int k)
{
    if (tree->root)
    {
        fprintf(stderr, "Cannot set parameters on a non-empty B-tree\n");
        exit(EXIT_FAILURE);
    }
    if (k < 3)
    {
        fprintf(stderr, "B-tree cannot have max_children less than 3\n");
        exit(EXIT_FAILURE);
    }
    tree->k = k;
}

static int leaf_get_length(Node leaf, int k)
{
    int n = 2 * k;
    if (leaf[n - 2])
        return leaf[n - 1]  ?  n  :  n - 1;
    else
        return (int) (* (intptr_t *) (leaf + n - 1));
}

static void leaf_set_length(Node leaf, int k, int len)
{
    int n = 2 * k;
    if (len <= n - 2)
    {
        leaf[n - 2] = NULL;
        * (intptr_t *) &leaf[n - 1] = (intptr_t) len;
    }
    else if (len == n - 1)
    {
        leaf[n - 1] = NULL;
    }
}

static int nonleaf_get_length(Node node)
{
    return (int) (* (intptr_t *) node);
}

static void nonleaf_set_length(Node node, int len)
{
    * (intptr_t *) node = (intptr_t) len;
}

static void nonleaf_inc_length(Node node)
{
    (* (intptr_t *) node)++;
}

static Node node_alloc(int k)
{
    return (Node) malloc(2 * k * sizeof(void *));
}

/**
 * Given an array `left' with `n' pointers 
 * add a 1-element hole into it at position `hole_pos' (0 <= hole_pos <= n)
 * and then split the array into two: `left' of `n_left' elements
 * and `right' of n + 1 - n_left elements.
 *
 * \returns  The pointer to the hole.
 */
static void **insert_hole_and_split
    (void **left, 
     void **right, 
     size_t n, 
     int hole_pos,
     int n_left)
{
    int n_right = n + 1 - n_left;
    if (hole_pos < n_left)
    {
        memcpy(right, left + n_left - 1, n_right * sizeof(void *));
        memmove(left + hole_pos + 1, left + hole_pos, (n_left - hole_pos - 1) * sizeof(void *));
        return left + hole_pos;
    }
    else
    {
        memcpy(right, left + n_left, (hole_pos - n_left) * sizeof(void *));
        memcpy(right + hole_pos - n_left + 1, left + hole_pos, (n - hole_pos) * sizeof(void *));
        
        return right + hole_pos - n_left;
    }
}

static Node leaf_split_insert_at(Node left, int k, int newpos, void *newval, void **p_bound)
{
    Node right = node_alloc(k);

    void **hole = insert_hole_and_split(left, right, 2 * k, newpos, k + 1);
    *hole = newval;
    *p_bound = left[k];

    leaf_set_length(left, k, k);
    leaf_set_length(right, k, k);
    return right;
}

static Node nonleaf_split_insert_at(Node left, int k, int newpos, void *bound, Node child, void **p_bound)
{
    Node right = node_alloc(k);
    // there are k children and k - 1 bounds, so (k - 1) / 2 bounds go left
    int n_left_bounds = (k - 1) / 2;

    // split bounds
    // from the viewpoint of insert_hole_and_split(),
    //    n_left_bounds + 1 bounds go to the left.
    // Actual length will indicate 1 less bounds:
    // the last bounds will be pushed up.
    void **hole = insert_hole_and_split(NONLEAF_KEYS(left), NONLEAF_KEYS(right),  
                                        k - 1, newpos, n_left_bounds + 1);
    *hole = bound; // must be done before the next line!
    *p_bound = NONLEAF_KEYS(left)[n_left_bounds];

    // split children
    hole = insert_hole_and_split(left + k, right + k,  
                                 k, newpos + 1, n_left_bounds + 1);
    *hole = child;

    nonleaf_set_length(left, n_left_bounds + 1);
    nonleaf_set_length(right, k - n_left_bounds);
    return right;
}

static void leaf_nosplit_insert_at(Node leaf, int k, int len, int newpos, void *newval)
{
    memmove(leaf + newpos + 1, leaf + newpos, (len - newpos) * sizeof(void *));
    leaf[newpos] = newval;
    leaf_set_length(leaf, k, len + 1);
}

static void nonleaf_nosplit_insert_at(Node node, int k, int len, int newpos, void *bound, Node child)
{
    void **keys = NONLEAF_KEYS(node);
    void **children = NONLEAF_CHILDREN(node, k);
    memmove(keys + newpos + 1, keys + newpos, (len - 1 - newpos) * sizeof(void *));
    keys[newpos] = bound;
    newpos++; // we insert a child to (orig. newpos) + 1
    memmove(children + newpos + 1, children + newpos, (len - newpos) * sizeof(void *));
    children[newpos] = child;
    nonleaf_inc_length(node);
}

static Node leaf_insert_at(Node leaf, int k, int newpos, void *newval, void **p_bound)
{
    int len = leaf_get_length(leaf, k);
    if (len == 2 * k)
        return leaf_split_insert_at(leaf, k, newpos, newval, p_bound);
    leaf_nosplit_insert_at(leaf, k, len, newpos, newval);
    return NULL;
}

static Node nonleaf_insert_at(Node node, int k, int newpos, void *bound, Node child, void **p_bound)
{
    int len = nonleaf_get_length(node);
    if (len == k)
        return nonleaf_split_insert_at(node, k, newpos, bound, child, p_bound);
    nonleaf_nosplit_insert_at(node, k, len, newpos, bound, child);
    return NULL;
}

static void *find_or_insert_rec(OmmBTree *tree, void *query, Node node, int height, Node *p_new_node, void **p_new_bound, void *(*dup)(void *))
{
    int k = tree->k;
    Comparator compare_func = tree->compare_func;
    if (!height)
    {
        // leaf
        int len = leaf_get_length(node, k);
        int b = omm_binary_search(query, node, len, compare_func);
        if (b < 0)
        {
            // we've found the query!
            *p_new_node = NULL;
            *p_new_bound = NULL;
            return node[b + len];
        }

        query = dup(query);
        if (query)
            *p_new_node = leaf_insert_at(node, k, b, query, p_new_bound);
        else
            *p_new_node = NULL;
        return query;
    }
    else
    {
        // nonleaf
        int len = nonleaf_get_length(node);
        int b = omm_binary_search(query, node + 1, len - 1, compare_func);

        if (b < 0)
        {
            // we've found the query!
            *p_new_node = NULL;
            *p_new_bound = NULL;
            return node[b + len];
        }
        
        Node child = node[b + k]; 
        Node new_child;
        void *new_child_bound;
        void *result = find_or_insert_rec(tree, query, child, height - 1, &new_child, &new_child_bound, dup);
        if (new_child)
            *p_new_node = nonleaf_insert_at(node, k, b, new_child_bound, new_child, p_new_bound);
        else
            *p_new_node = NULL;
        return result;
    }
}

static void create_root(OmmBTree *tree, void *item)
{
    tree->root = node_alloc(tree->k);
    leaf_set_length(tree->root, tree->k, 1);
    tree->depth = 0;
    tree->root[0] = item;
}

static void split_root(OmmBTree *tree, Node new_right, void *bound)
{
    Node new_left = tree->root;
    tree->root = node_alloc(tree->k);
    nonleaf_set_length(tree->root, 2); 
    tree->depth++;
    tree->root[tree->k] = new_left;
    tree->root[tree->k + 1] = new_right;
    tree->root[1] = bound;
}

void *omm_btree_insert_dup(OmmBTree *tree, void *query, void *(*dup)(void *))
{
    if (!tree->root)
    {
        query = dup(query);
        if (query)
            create_root(tree, query);
        return query;
    }

    Node new_node;
    void *new_bound;
    void *result = find_or_insert_rec(tree, query, tree->root, tree->depth, &new_node, &new_bound, dup);

    if (new_node)
        split_root(tree, new_node, new_bound);

    return result;
}

static void *no_dup(void *ptr)
{
    return ptr;
}

static void *null_dup(void *ptr)
{
    (void) ptr; // avoid the unused warning
    return NULL;
}

void *omm_btree_insert(OmmBTree *tree, void *query)
{
    return omm_btree_insert_dup(tree, query, no_dup);
}

void *omm_btree_find(OmmBTree *tree, void *query)
{
    return omm_btree_insert_dup(tree, query, null_dup);
}

static void node_free_rec(int depth, int k, Node node)
{
    if (depth)
    {
        int len = nonleaf_get_length(node);
        for (int i = 0; i < len; i++)
            node_free_rec(depth - 1, k, node[k + i]);
    }
    free(node);
}

void omm_btree_free(OmmBTree *tree)
{
    if (tree->root)
        node_free_rec(tree->depth, tree->k, tree->root);
    free(tree);
}

static void foreach_rec(
    Node node, 
    int k, 
    int height, 
    void (*visitor)(void *, void *user_data), 
    void *user_data)
{
    if (height)
    {
        int len = nonleaf_get_length(node);
        
        foreach_rec(NONLEAF_CHILDREN(node, k)[0], k, height - 1, visitor, user_data);
        for (int i = 1; i < len; i++)
        {
            visitor(NONLEAF_KEYS(node)[i - 1], user_data);
            foreach_rec(NONLEAF_CHILDREN(node, k)[i], k, height - 1, visitor, user_data);
        }
    }
    else
    {
        int len = leaf_get_length(node, k);
        for (int i = 0; i < len; i++)
            visitor(node[i], user_data);
    }
}

void omm_btree_foreach(OmmBTree *tree, void (*visitor)(void *, void *data), void *data)
{
    if (tree->root)
        foreach_rec(tree->root, tree->k, tree->depth, visitor, data);
}
