/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#ifndef OMM_LABELING_H
#define OMM_LABELING_H

#include "omm-image.h"

/**
 * \file omm-labeling.h
 * Labeling connected components.
 */

/**
 * Label 4-connected components in the image.
 *
 * \param[out] labels   An array of length (rle->n_runs / 2) with the CC indices
 * \param[in]  rle      The input image in RLE
 *
 * \returns  The number of connected components.
 */
int omm_label(uint32_t *labels, const OmmImage *rle);

/**
 * Information about a connected component.
 */
typedef struct
{
    int n_black_runs;
    int mass;
    short min_x;
    short max_x;
    short min_y;
    short max_y;
    int index;
} OmmBlobInfo;

/**
 * Analyze the result of omm_rle_label().
 */
void omm_analyze_blobs(
    OmmBlobInfo *restrict blobs,
    int n_blobs,
    const uint32_t *restrict labels, 
    const OmmImage *restrict rle);

void omm_render_multi(
    uint8_t *restrict *restrict images,
    OmmBlobInfo *restrict blobs,
    const uint32_t *restrict labels,
    const OmmImage *restrict rle);

int omm_label_and_analyze_blobs(
    uint32_t **restrict p_labels,
    OmmBlobInfo **restrict p_blobs,
    const OmmImage *restrict rle);

void omm_render_components_onto(
    uint8_t *restrict dst,
    int dst_width,
    int dst_height,
    OmmBlobInfo *restrict blobs,
    const int *restrict sx,
    const int *restrict sy,
    const OmmImage *restrict src,
    const uint32_t *restrict labels);


#endif
