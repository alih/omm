/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include <assert.h>
#include <stdlib.h>
#include "omm-image.h"
#include "omm-rle.h"

static OmmImage *omm_image_new_no_alloc(int width, int height)
{
    OmmImage *rle = (OmmImage *) malloc(sizeof(OmmImage));
    rle->width = width;
    rle->height = height;
    rle->line_offsets = (int *) malloc((height + 1) * sizeof(int));
    rle->n_runs = 0;
    return rle;
}

OmmImage *omm_image_new(int width, int height)
{
    OmmImage *rle = omm_image_new_no_alloc(width, height);
    rle->runs = (uint8_t *) malloc(height * omm_rle_max_size(width));
    return rle;
}

OmmImage *omm_image_new_from_raster(const unsigned char *pixels, int w, int h)
{
    OmmImage *rle = omm_image_new(w, h);
    rle->line_offsets[0] = 0;
    for (int i = 0; i < h; i++)
    {
        int n = omm_rle_extract(rle->runs + rle->line_offsets[i],
                                pixels + i * w, w);
        rle->line_offsets[i + 1] = rle->line_offsets[i] + n;
    }
    rle->n_runs = rle->line_offsets[h];
    return rle;
}

OmmImage *omm_image_new_from_grayscale(const unsigned char *pixels, int w, int h, int stride, int threshold)
{
    OmmImage *rle = omm_image_new(w, h);
    rle->line_offsets[0] = 0;
    for (int i = 0; i < h; i++)
    {
        int n = omm_rle_threshold_and_extract(rle->runs + rle->line_offsets[i],
                                pixels + i * stride, w, threshold);
        rle->line_offsets[i + 1] = rle->line_offsets[i] + n;
    }
    rle->n_runs = rle->line_offsets[h];
    return rle;
}


void omm_image_free(OmmImage *rle)
{
    free(rle->runs);
    free(rle->line_offsets);
    free(rle);
}

int omm_image_longest_line_in_runs(const int *offsets, int height)
{
    int result = offsets[1] - offsets[0];
    int i;
    for (i = 1; i < height; i++)
    {
        int c = offsets[i + 1] - offsets[i];
        if (c > result)
            result = c;
    }
    return result;
}

OmmImage *omm_image_crop(OmmImage *image, int x0, int y0, int x1, int y1)
{
    if (x0 < 0)
        x0 = 0;
    if (x1 > image->width)
        x1 = image->width;
    if (y0 < 0)
        y0 = 0;
    if (y1 > image->height)
        y1 = image->height;
    if (x0 >= x1 || y0 >= y1)
        return NULL;

    OmmImage *result = omm_image_new_no_alloc(x1 - x0, y1 - y0);
    result->runs = (uint8_t *) malloc(image->line_offsets[y1] - image->line_offsets[y0]);
    result->line_offsets[0] = 0;
    for (int y = y0; y < y1; y++)
    {
        int n = omm_rle_crop(result->runs + result->line_offsets[y - y0],
                        image->runs + image->line_offsets[y],
                        image->line_offsets[y + 1] - image->line_offsets[y],
                        x0, x1);
        result->n_runs += n;
        result->line_offsets[y - y0 + 1] = result->n_runs;
    }
    assert(omm_image_check(result));
    return result;
}

int omm_image_check(const OmmImage *rle)
{
    const int w = rle->width;
    const int h = rle->height;
    const int n = rle->n_runs;

    if (w < 0 || h < 0 || n < 0 || n % 2)
        return 0;

    if (rle->line_offsets[0] != 0 || rle->line_offsets[h] != n)
        return 0;
    
    for (int i = 0; i < h; i++)
    {
        // XXX replace this with omm_rle_check()
        const int start = rle->line_offsets[i];
        const int end = rle->line_offsets[i + 1];
        if (end % 2)
            return 0;

        for (int j = start + 1; j < end; j++)
            if (rle->runs[j] == 0 && rle->runs[j - 1] == 0)
                return 0;

        int s = 0;
        for (int j = start; j < end; j++)
            s += rle->runs[j];

        if (s != w)
            return 0;
    }

    return 1;
}

void omm_image_render(
    const OmmImage *restrict image, 
    uint8_t *restrict pixels, 
    int w, int h, int stride)
{
    for (int i = 0; i < h; i++)
    {
        omm_rle_render(pixels + i * stride, w, 
                       image->runs + image->line_offsets[i],
                       image->line_offsets[i + 1] - image->line_offsets[i]);
    }
}
