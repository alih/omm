/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include "omm-rle.h"


int omm_rle_max_size(int width)
{
    return width + 2;
}

int omm_rle_extract_with_max_run(
    uint8_t *restrict runs,
    const uint8_t *restrict pixels,
    int n,
    int max_run)
{
    int count;
    int p = 0;
    int current = 0;

    //@ ghost int last_run = max_run;
    /*@ loop invariant 0 <= count <= p + 1;
        loop invariant 0 <= p <= n;
        loop invariant current == pixels[p] || p == n || last_run == max_run;
        loop invariant last_run == 0  ==>  p == n || current == pixels[p];
        loop invariant last_run == max_run  ==>  count <= p;
        loop invariant current == 0 || current == 1;
        loop variant n + 1 - count;
     */
    for (count = 0; p < n; count++)
    {
        int old_p = p;

        int max_p = p + max_run;
        if (max_p > n)
            max_p = n;

        //@ assert p < max_p;

        /*@ loop invariant p >= old_p;
            loop invariant p <= max_p <= n;
            loop variant max_p - p;
         */
        while (p < max_p && pixels[p] == current)
            p++;
        //@ assert pixels[old_p] == current ==> p > old_p;

        //@ assert p == max_p ==> (p == n || max_p == old_p + max_run);

        //@ assert 0 <= p - old_p <= n;
        runs[count] = p - old_p;
        //@ ghost last_run = runs[count];
        //@ assert p > old_p  ==>  last_run > 0;
        //@ assert pixels[old_p] == current ==> last_run > 0;
        //@ assert p == max_p ==> (p == n || last_run == max_run);
        //@ assert p == max_p || pixels[p] != current;
        /*@ assert last_run == max_run 
                || p == n
                || pixels[p] != current;
         */
        current = 1 - current;
    }
    
    //@ assert count <= n + 1;
    if (current)
        runs[count++] = 0;

    return count;
}

int omm_rle_crop(
    uint8_t *restrict result,
    const uint8_t *restrict runs,
    int n_runs,
    int x0,
    int x1)
{
    if (x0 < 0)
        x0 = 0;
    if (x0 >= x1 || x1 <= 0)
        return 0;

    int x = 0;
    int i = 0;

    // the skip loop
    for (; i < n_runs; i++)
    {
        x += runs[i];
        if (x >= x0)
            break;
    }
    if (i == n_runs)
        return 0; // x never was >= x0
    
    if (x >= x1) // we've covered the interval with a single run
    {
        result[1 - (i & 1)] = 0;
        result[i & 1] = x1 - x0;
        return 2;
    }

    // preparation for the copy loop
    int k = 0;
    if (x > x0)
    {
        // overshoot: cut the last run
        if (i & 1)
            result[k++] = 0; // sync the color between result and i
        result[k++] = x - x0; // <= 255 because the for loop didn't stop before
        i++;
    }
    else
    {
        assert(x == x0); // otherwise i == n_runs and x0 > width
        i++;
        if (i < n_runs && !runs[i]) // check for a 0 run lest we have 2 0s
            i++;
        if (i & 1)
           result[k++] = 0;
    }


    assert((i & 1) == (k & 1));
    // the copy loop
    // invariant: result[0] + ... + result[k-1] == x - x0
    //  (although it's broken by the break statement)
    for (; i < n_runs; i++)
    {
        x += runs[i];
        if (x >= x1)
            break;
        result[k++] = runs[i];
    }

    if (i == n_runs)
        return k; // x never was >= x1

    // we broke out of the copy loop because x >= x1; last run not copied
    result[k++] = x1 - x + runs[i];
    if (k & 1)
        result[k++] = 0;
    return k;
}


int omm_rle_threshold_and_extract_with_max_run(
    uint8_t *restrict runs,
    const uint8_t *restrict pixels,
    int n,
    int threshold,
    int max_run)
{
    int count;
    int p = 0;
    int current = 0;

    if (threshold < 0)
    {
        threshold = -threshold;
        for (count = 0; p < n; count++)
        {
            int old_p = p;

            int max_p = p + max_run;
            if (max_p > n)
                max_p = n;

            if (current)
            {
                while (p < max_p && pixels[p] >= threshold)
                    p++;
            }
            else
            {
                while (p < max_p && pixels[p] < threshold)
                    p++;
            }
            runs[count] = p - old_p;
            current = 1 - current;
        }
    }
    else
    {
        // this is a copy of the loop in the previous branch
        for (count = 0; p < n; count++)
        {
            int old_p = p;

            int max_p = p + max_run;
            if (max_p > n)
                max_p = n;

            if (current)
            {
                while (p < max_p && pixels[p] < threshold)
                    p++;
            }
            else
            {
                while (p < max_p && pixels[p] >= threshold)
                    p++;
            }
            runs[count] = p - old_p;
            current = 1 - current;
        }
    }
    
    if (current)
        runs[count++] = 0;

    return count;
}

int omm_rle_extract(
    uint8_t *restrict runs,
    const uint8_t *restrict pixels,
    int n)
{
    return omm_rle_extract_with_max_run(runs, pixels, n, 255);
}

int omm_rle_threshold_and_extract(
    uint8_t *restrict runs,
    const uint8_t *restrict pixels,
    int n,
    int threshold)
{
    return omm_rle_threshold_and_extract_with_max_run(
        runs, pixels, n, threshold, 255);
}

int omm_rle_extract_packed(
    uint8_t *restrict runs,
    const uint8_t *restrict packed,
    int n,
    int white)
{
    const int max_run = 255;
    int coef = 0x80;
    int a = *packed;
    int run = 0;
    int color = white;
    uint8_t *p = runs;
     
    while (n--)
    {
        if ((a & coef ? 1 : 0) == color)
        {
            run++;
            if (run == max_run)
            {
                *p++ = max_run;
                run = 0;
                if (!n)
                    break; // or two 0s might happen
                color = !color;
            }
        }
        else
        {
            *p++ = run;
            color = !color;
            run = 1;
        }

        coef >>= 1;
        if (!coef)
        {
            coef = 0x80;
            if (!n) break; // avoid buffer over-read in the next statement
            a = *++packed;
        }
    }
   
    if (run)
        *p++ = run;

    if ((p - runs) & 1)
        *p++ = 0;

    return p - runs;
}


int omm_rle_32_to_8(
    uint8_t *restrict runs,
    int width,
    const uint32_t *restrict runs32, int n)
{
    int i;
    int count = 0;
    int total = 0;
    for (i = 0; i < n; i++)
    {
        uint32_t run = runs32[i];
        total += run;
        if (total > width)
        {
            fprintf(stderr, "Error: invalid 32-bit RLE code (line too short)\n");
            exit(EXIT_FAILURE);
        }

        while (run > 255)
        {
            runs[count++] = 255;
            runs[count++] = 0;
            run -= 255;
        }

        runs[count++] = run;
    }
    
    if (count & 1)
        runs[count++] = 0;

    return count;
}

void omm_rle_render(
    uint8_t *restrict pixels,
    int n,
    const uint8_t *restrict runs,
    int n_runs)
{
    (void) n;
    uint8_t *p = pixels;
    for (int i = 0; i < n_runs; i++)
    {
        uint8_t color = i & 1;
        assert(p - pixels + runs[i] <= n);
        for (int j = 0; j < runs[i]; j++)
            *p++ = color;
    }
}

void omm_rle_render_to_packed(
    uint8_t *restrict pixels,
    int n,
    const uint8_t *restrict runs,
    int n_runs)
{
    assert(n_runs % 2 == 0);
    memset(pixels, 0, (n + 7) >> 3);
    int x = 0;
    for (int i = 0; i < n_runs; i += 2)
    {
        x += runs[i];
        int si = x >> 3;
        int sk = x & 7;
        x += runs[i + 1];
        assert(x <= n);
        int ei = (x - 1) >> 3;
        int ek = (x - 1) & 7;
        if (si == ei)
            pixels[si] |= ((1 << runs[i + 1]) - 1) << (7 - ek);
        else
        {
            pixels[si] |= 0xFF >> sk;
            for (int j = si + 1; j < ei; j++)
                pixels[j] = 255;
            pixels[ei] = 0xFF80 >> ek;
        }
    }
}

void omm_rle_render_labels(
    uint32_t *restrict result,
    int n,
    const uint32_t *restrict labels,
    const uint8_t *restrict runs,
    int n_runs)
{
    (void) n;
    uint32_t *p = result;
    for (int i = 0; i < n_runs; i++)
    {
        assert(p - result + runs[i] <= n);
        uint32_t label = (i & 1  ?  labels[i / 2]  :  0xFFFFFFFFu);
        for (int j = 0; j < runs[i]; j++)
            *p++ = label;
    }
}

int omm_rle_check(const uint8_t *runs, int n_runs, int width)
{
    assert(n_runs >= 0);
    if (n_runs & 1)
        return 0;
    int sum = 0;
    for (int i = 0; i < n_runs; i++)
        sum += runs[i];
    if (sum != width)
        return 0;

    // check that there are no two consecutive zeros
    for (int i = 1; i < n_runs; i++)
        if (!runs[i] && !runs[i-1])
            return 0;
    return 1;
}

