/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include <stdio.h>
#include <stdlib.h>
#include "omm-endian.h"

OmmEndianness omm_endianness()
{
    uint32_t test = 0x04030201;
    char *p = (char *) &test;
    int t = p[0] * 1000 + p[1] * 100 + p[2] * 10 + p[3];
    if (t == 4321)
        return OMM_BIG_ENDIAN;
    if (t == 1234)
        return OMM_LITTLE_ENDIAN;

    fprintf(stderr, 
            "%s:%d: Have you actually run this on a PDP-endian machine?\n",
            __FILE__, __LINE__);
    exit(EXIT_FAILURE);
}

static uint32_t flip32(uint32_t a)
{
    return (a << 24)
         | ((a & 0xFF00) << 8)
         | ((a & 0xFF0000) >> 8)
         | (a >> 24);
}

void omm_import_endian_32(uint32_t *data, int n, OmmEndianness foreign)
{
    if (foreign == omm_endianness())
        return;

    for (int i = 0; i < n; i++)
        data[i] = flip32(data[i]);
}

void omm_export_endian_32(uint32_t *data, int n, OmmEndianness foreign)
{
    omm_import_endian_32(data, n, foreign);
}
