/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "omm-btree.h"
#include "omm-io-model.h"

enum
{
    STORAGE_SIZE_OFFSET = 4,
    STORAGE_OFFSET = 12
};

// writer {{{


struct OmmModelWriter
{
    FILE *file;
    OmmBTree *btree;
    size_t storage_size; // in 32-bit members
    size_t strpool_size;   // in bytes
    OmmEndianness endianness;
};

typedef struct
{
    char *string;
    uint32_t offset;
} Entry;

// A crude hack to avoid const cast warnings
typedef struct
{
    const char *string;
    uint32_t offset;
} ConstStringEntry;

// a function that can be passed to omm_btree_new()
static int entry_cmp(const void *p1, const void *p2)
{
    const Entry *e1 = (const Entry *) p1;
    const Entry *e2 = (const Entry *) p2;
    return strcmp(e1->string, e2->string);
}

// a function that can be passed to omm_btree_foreach()
static void entry_into_pool(void *p, void *data)
{
    Entry *e = (Entry *) p;
    char *strpool = (char *) data;
    strcpy(strpool + e->offset, e->string);
    free(e->string);
    free(e);
}

// a function that can be passed to omm_btree_insert_dup()
static void *entry_dup(void *p)
{
    Entry *result = (Entry *) malloc(sizeof(Entry));
    memcpy(result, p, sizeof(Entry));
   
    // reimplementing strdup() because it's not turned on by _POSIX_SOURCE (why?)
    char *t = (char *) malloc(strlen(result->string) + 1);
    strcpy(t, result->string);
    result->string = t;

    return result;
}

OmmModelWriter *omm_model_writer_new(const char *path, OmmEndianness endianness)
{
    FILE *f = fopen(path, "wb");
    if (!f)
    {
        perror(path);
        return NULL;
    }
    OmmModelWriter *writer = (OmmModelWriter *) malloc(sizeof(OmmModelWriter));
    
    writer->file = f;
    writer->btree = omm_btree_new(entry_cmp);
    writer->storage_size = 0;
    writer->strpool_size = 0;
    writer->endianness = endianness;

    int32_t e = (int32_t) endianness;
    fwrite(&e, sizeof(e), 1, f);

    // check that STORAGE_SIZE_OFFSET is defined correctly
    assert(ftell(f) == STORAGE_SIZE_OFFSET);

    e = 0; // placeholder for storage & strpool size
    fwrite(&e, sizeof(e), 1, f);
    fwrite(&e, sizeof(e), 1, f);

    return writer;
}

void omm_model_writer_write(OmmModelWriter *writer, const uint32_t *a, int n)
{
    if (omm_endianness() == writer->endianness)
    {
        fwrite(a, sizeof(uint32_t), n, writer->file);
    }
    else
    {
        uint32_t *t = (uint32_t *) malloc(n * sizeof(uint32_t));
        memcpy(t, a, n * sizeof(uint32_t));
        omm_export_endian_32(t, n, writer->endianness);
        fwrite(t, sizeof(uint32_t), n, writer->file);
        free(t);
    }
    writer->storage_size += n;
}

uint32_t omm_model_writer_intern(OmmModelWriter *writer, const char *str)
{
    size_t s = strlen(str) + 1;
    ConstStringEntry e;
    // this const cast works here because if we insert, then we dup
    e.string = str;
    e.offset = UINT32_MAX; // a signal value to see if we insert or not
    Entry *p = (Entry *) omm_btree_insert_dup(writer->btree, (Entry *) &e, entry_dup);
    if (p->offset == UINT32_MAX)
    {
        p->offset = writer->strpool_size;
        writer->strpool_size += s;
    }
    return p->offset;
}

void omm_model_writer_write_string(OmmModelWriter *writer, const char *str)
{
    uint32_t i = omm_model_writer_intern(writer, str);
    omm_model_writer_write(writer, &i, 1);
}

void omm_model_writer_free(OmmModelWriter *writer)
{
    // write the string pool
    char *strpool = (char *) malloc(writer->strpool_size);
    omm_btree_foreach(writer->btree, entry_into_pool, strpool);
    fwrite(strpool, writer->strpool_size, 1, writer->file);

    // fix the storage size
    fseek(writer->file, STORAGE_SIZE_OFFSET, SEEK_SET);
    uint32_t s = (uint32_t) writer->storage_size;
    omm_export_endian_32(&s, 1, writer->endianness);
    fwrite(&s, sizeof(s), 1, writer->file);

    // fix the strpool size
    s = (uint32_t) writer->strpool_size;
    omm_export_endian_32(&s, 1, writer->endianness);
    fwrite(&s, sizeof(s), 1, writer->file);

    // free memory
    fclose(writer->file);
    free(strpool);
    omm_btree_free(writer->btree);
    free(writer);
}

// }}}

struct OmmModelReader
{
    const uint32_t *storage;
    const char *strpool;
    size_t storage_size;
    size_t strpool_size;

    int to_close;
    void *to_free;
    void *to_munmap;
    size_t to_munmap_length;

};

static void check_storage_size(size_t storage_size, size_t length)
{
    if (STORAGE_OFFSET + storage_size * sizeof(uint32_t) > length)
    {
        fprintf(stderr, "Error while loading model:\n");
        fprintf(stderr, "   actual data size: %lu\n", (long unsigned) length);
        fprintf(stderr, "   specified in the file: %lu\n", 
                            (long unsigned) (storage_size * sizeof(uint32_t)));
        exit(1);
    }
}

static OmmModelReader *reader_new(const void *void_ptr, size_t length)
{
    const uint32_t *ptr = (const uint32_t *) void_ptr;    
    assert(*ptr == omm_endianness());
    OmmModelReader *reader = (OmmModelReader *) malloc(sizeof(OmmModelReader));
    reader->storage = ptr + STORAGE_OFFSET / sizeof(uint32_t);
    reader->storage_size = ptr[1];
    reader->strpool_size = ptr[2];

    check_storage_size(reader->storage_size, length);
    reader->strpool = (const char *) (reader->storage + reader->storage_size);

    reader->to_close  = -1;
    reader->to_free   = NULL;
    reader->to_munmap = NULL;
    return reader;
}

OmmModelReader *omm_model_reader_new_from_data_and_fix(void *void_ptr, size_t length)
{
    uint32_t *ptr = (uint32_t *) void_ptr;
    OmmEndianness e = (OmmEndianness) *ptr;
    if (e != omm_endianness())
    {
        omm_import_endian_32(ptr + 1, 1, e);
        uint32_t storage_size = ptr[1];
        omm_import_endian_32(ptr + 2, 1, e); // strpool_size
        check_storage_size(storage_size, length);
        omm_import_endian_32(ptr + STORAGE_OFFSET / sizeof(uint32_t), storage_size, e);
        *ptr = omm_endianness();
    }
    return reader_new(ptr, length);
}

OmmModelReader *omm_model_reader_new_from_data(
    const void *void_ptr, 
    size_t length)
{
    const uint32_t *ptr = (const uint32_t *) void_ptr;
    OmmEndianness e = (OmmEndianness) *ptr;
    if (e == omm_endianness())
        return reader_new(ptr, length);
    
    void *c = malloc(length);
    memcpy(c, void_ptr, length);
    OmmModelReader *reader = omm_model_reader_new_from_data_and_fix(c, length);
    reader->to_free = c;
    return reader;
}

static int checked_open(const char *path, int flags)
{
    int fd = open(path, flags);
    if (fd == -1)
    {
        perror(path);
        exit(EXIT_FAILURE);
    }
    return fd;
}

static OmmModelReader *reader_load(const char *path, int fix)
{
    int fd = checked_open(path, O_RDONLY);
    uint32_t e;
    read(fd, &e, sizeof(e));
    
    struct stat s;
    if (fstat(fd, &s))
    {
        perror(path);
        exit(EXIT_FAILURE);
    }

    size_t len = (size_t) s.st_size;

    void *model;
    if (e == omm_endianness())
        model = mmap(NULL, len, PROT_READ, MAP_PRIVATE, fd, 0);
    else if (fix)
        model = mmap(NULL, len, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    else
        model = mmap(NULL, len, PROT_READ | PROT_WRITE, MAP_PRIVATE, fd, 0);

    if (model == MAP_FAILED)
    {
        perror(path);
        close(fd);
        exit(EXIT_FAILURE);
    }

    OmmModelReader *reader = omm_model_reader_new_from_data_and_fix(model, len);
    reader->to_munmap = model;
    reader->to_munmap_length = len;
    reader->to_close = fd;

    return reader;
}

OmmModelReader *omm_model_reader_new_from_file(const char *path)
{
    return reader_load(path, 0);
}

OmmModelReader *omm_model_reader_new_from_file_and_fix(const char *path)
{
    return reader_load(path, 1);
}

const uint32_t *omm_model_reader_get_storage(OmmModelReader *reader)
{
    return reader->storage;
}

size_t omm_model_reader_get_storage_length(OmmModelReader *reader)
{
    return reader->storage_size;
}

const char *omm_model_reader_get_string_pool(OmmModelReader *reader)
{
    return reader->strpool;
}

size_t omm_model_reader_get_string_pool_length(OmmModelReader *reader)
{
    return reader->strpool_size;
}

void omm_model_reader_free(OmmModelReader *reader)
{
    if (reader->to_munmap)
        munmap(reader->to_munmap, reader->to_munmap_length);
    if (reader->to_free)
        free(reader->to_free);
    if (reader->to_close != -1)
        close(reader->to_close);
    free(reader);
}

// vim: set foldmethod=marker:
