/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#ifndef OMM_IO_MODEL_H
#define OMM_IO_MODEL_H

/**
 * \file omm-io-model.h
 *
 * \brief Binary I/O for model files.
 *
 *
 * The OMM model file format is designed with loading speed in mind.
 * There should be as little malloc() calls as possible when loading a model.
 * Therefore, all strings are stored in a single memory chunk ("string pool").
 * The model is supposed to stay in memory at all times during the OCR run,
 * therefore the strings from the model may be referenced by their offsets.
 *
 * Besides the strings, the model file stores a bunch of 32-bit integers.
 * This solution might be temporary; it's based on decision not to use
 * floating-point computations.
 *
 * The integers might be stored with either endianness; it's best to store
 * them with the native endianness of the platform on which the OCR will run,
 * so that the entire model file may be mmapped or fread. The model reader
 * will work even if the endianness is different: it will flip the integers
 * in the memory and, if desired, will flip the model file also.
 *
 * Here is the file format:
 *
 *  (magic bytes currently not implemented)
 *  4 bytes - endianness marker ('<<<<' or '>>>>')
 *  4 bytes - the number of 32-bit integers that follow
 *  4 bytes x that number - the 'storage'
 *  string pool
 */

#include <stdint.h>
#include <stdlib.h>
#include "omm-endian.h"


typedef struct OmmModelReader OmmModelReader;
typedef struct OmmModelWriter OmmModelWriter;

OmmModelWriter *omm_model_writer_new(const char *path, OmmEndianness endianness);
void omm_model_writer_write(OmmModelWriter *, const uint32_t *, int n);
uint32_t omm_model_writer_intern(OmmModelWriter *, const char *str);
/**
 * Insert the string into the pool and append its offset to the model file.
 */
void omm_model_writer_write_string(OmmModelWriter *, const char *str);
void omm_model_writer_free(OmmModelWriter *);

/**
 * Read a model from file with mmap(), copying if the endianness is wrong.
 */
OmmModelReader *omm_model_reader_new_from_file(const char *path);
/**
 * Read a model from file, rewriting the file if the endianness is wrong.
 */
OmmModelReader *omm_model_reader_new_from_file_and_fix(const char *path);
OmmModelReader *omm_model_reader_new_from_data(const void *, size_t length);
OmmModelReader *omm_model_reader_new_from_data_and_fix(void *, size_t length);

const uint32_t *omm_model_reader_get_storage(OmmModelReader *);
size_t omm_model_reader_get_storage_length(OmmModelReader *);
const char *omm_model_reader_get_string_pool(OmmModelReader *);
size_t omm_model_reader_get_string_pool_length(OmmModelReader *);

void omm_model_reader_free(OmmModelReader *);

#endif
