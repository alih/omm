/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */


#ifndef OMM_RLE_H
#define OMM_RLE_H

#include <stdint.h>

/**
 * \file omm-rle.h
 * Low-level routines for working with run-length encoded lines.
 *
 * RLE images are fundamental in OMM.
 *
 * There are at least 2 interesting libraries that also deal with RLE images:
 *   - iulib (http://code.google.com/p/iulib/)
 *   - Camellia (http://camellia.sourceforge.net/)
 */

/**
 * Get the maximum RLE line size by width.
 *
 * Calculates the maximum storage in bytes 
 * necessary to hold RLE code of a binary line with the given width.
 */
int omm_rle_max_size(int width);

/**
 * Convert the array of 0/1s into a sequence of (white run, black run) pairs.
 *
 * \param runs    Output array. Must be at least omm_rle_max_size(n) bytes.
 * \param pixels  Input array, containing only 0s and 1s.
 * \param n       Input array length.
 * \returns Number of runs.
 */
int omm_rle_extract(
    uint8_t *restrict runs, 
    const uint8_t *restrict pixels, 
    int n);

/**
 * Same as omm_rle_extract(), but with a limit on the run size.
 * This was a little exercise with Frama-C, so there's some ACSL in the comments.
 * \see omm_rle_extract
 */
/*@ requires \valid_range(runs, 0, n+1);
    requires \valid_range(pixels, 0, n-1);
    requires \forall integer i;  0 <= i < n  ==>  pixels[i] == 0 || pixels[i] == 1;
    requires 2 <= max_run <= 255;
    requires 0 <= n <= INT_MAX - max_run;

    assigns runs[0..n+1];

    ensures 0 <= \result <= n + 2;
 */
int omm_rle_extract_with_max_run(
    uint8_t *restrict runs,
    const uint8_t *restrict pixels,
    int n,
    int max_run);


/**
 *  Same as omm_rle_extract(), but for bit-packed line.
 * 
 * \param[out] runs    Output array. Must be at least omm_rle_max_size(n) bytes.
 * \param[in]  packed  Input array, containing PBM-like packed line.
 * \param n            The number of bits packed into the line.
 * \param white        White color (0 or 1).
 * \returns Number of runs.
 */
int omm_rle_extract_packed(
    uint8_t *restrict runs,
    const uint8_t *restrict packed, 
    int n,
    int white);


/**
 * Convert 32-bit RLE line to 8-bit one.
 *
 * \param[out] runs     The output runs. Should contain enough space,
 *                      namely, omm_rle_max_size(width) bytes.
 * \param      width    The line width, in pixels.
 * \param[in]  runs32   The 32-bit runs, in (white run, black run) pairs.
 * \param      n        The amount of 32-bit runs.
 */
int omm_rle_32_to_8(
    uint8_t *restrict runs,
    int width,
    const uint32_t *restrict runs32,
    int n);

/**
 * Uncompress the RLE line into the output array, 1 byte per pixel.
 * This is the inverse operation to omm_rle_extract().
 */
void omm_rle_render(
    uint8_t *restrict pixels,
    int width,
    const uint8_t *restrict runs,
    int n_runs);

void omm_rle_render_to_packed(
    uint8_t *restrict pixels,
    int n,
    const uint8_t *restrict runs,
    int n_runs);

/**
 * Render a segmentation (an array of integer labels).
 * The labels correspond to black runs.
 * Background is (uint) (-1).
 *
 * \param result   The resulting segmentation.
 * \param width    The length of the result[]. Isn't actually checked.
 * \param labels   n_runs / 2 labels corresponding to the black runs.
 * \param runs
 * \param n_runs
 */
void omm_rle_render_labels(
    uint32_t *restrict result,
    int width,
    const uint32_t *restrict labels,
    const uint8_t *restrict runs,
    int n_runs);

/**
 * Crop the line to the [x0, x1) interval.
 *
 * \returns              The number of runs in the cropped line
 * \param[out]  result   The runs of the cropped line,
 *                       should be at least n_runs long
 */
int omm_rle_crop(
    uint8_t *restrict result,
    const uint8_t *restrict runs,
    int n_runs,
    int x0,
    int x1);


/**
 * Check that the RLE line is consistent,
 * mainly that the rows sum up to the declared width.
 */
int omm_rle_check(const uint8_t *runs, int n_runs, int width);

/**
 * Same as omm_rle_threshold_and_extract, but limiting the maximum run length.
 */
int omm_rle_threshold_and_extract_with_max_run(
    uint8_t *restrict runs,
    const uint8_t *restrict pixels,
    int n,
    int threshold,
    int max_run);

/**
 * Simultaneously threshold a line and extract its RLE.
 * If the threshold is positive, then the foreground is lighter.
 * If the threshold is negative, then the negated (positive) threshold is used,
 *     but the foreground is darker.
 * The threshold of 0 makes no sense.
 */
int omm_rle_threshold_and_extract(
    uint8_t *restrict runs,
    const uint8_t *restrict pixels,
    int n,
    int threshold);

#endif
