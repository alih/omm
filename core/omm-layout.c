/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

/*
 * The idea for profile score (simply sum of squares) is taken from:
 *   H. S. Baird, "The Skew Angle of Printed Documents", 1987
 */

#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "omm-layout.h"

// _________________________   skew estimation   ______________________________

// XXX p/2^q would be WAAY better than p/q...

/* The tan table was generated with the help of this Python2 script:
 * 
 * import numpy
 * 
 * for i in numpy.arange(.5, 10.5, 0.5):
 *     t = numpy.tan(i / 180 * numpy.pi)
 *     b = 1
 *     a = 0
 *     for j in range(1, 1001):
 *         p = t * j
 *         c = abs(p - round(p))
 *         if (c < b):
 *            a = j
 *            b = c
 *     print "%3d, %3d," % (int(round(t * a)), a)
 */


static int tan_table[] = {  
      5, 573,
      7, 401,
     16, 611,
     11, 315,
     31, 710,
     37, 706,
     20, 327,
     10, 143,
     17, 216,
      7,  80,
     96, 997,
     35, 333,
      9,  79,
     97, 790,
     47, 357,
     26, 185,
     68, 455,
     51, 322,
    165, 986,
     73, 414};

static int omm_tan_max_arg()
{
    return sizeof(tan_table) / sizeof(tan_table[0]) / 2;
}

/**
 * Return x * tan(angle * angle_step), rounded towards 0.
 */
static int omm_tan_scaled(int angle, int x)
{
    if (angle == 0)
        return 0;
    int abs_angle = angle > 0 ? angle : -angle;
    int p = tan_table[2 * abs_angle - 2];
    int q = tan_table[2 * abs_angle - 1];
    int a = x * p / q;
    if (angle > 0)
        return a;
    else
        return -a;
}

static void omm_tan(int angle, int *p_num, int *p_det)
{
    if (angle == 0)
    {
        *p_num = 0;
        *p_det = 1;
        return;
    }
    int abs_angle = angle > 0 ? angle : -angle;
    *p_num = tan_table[2 * abs_angle - 2];
    *p_det = tan_table[2 * abs_angle - 1];
    if (angle < 0)
        *p_num = - *p_num;
}

typedef struct
{
    int *bins;
    int min;
    int max;
    int angle; // an argument to omm_tan_scaled()
} Accumulator;

/*static int clip(int x, int min, int max)
{
    if (x < min)
        return min;
    if (x > max)
        return max;
    return x;
}*/

static void project(Accumulator *a, int x, int y0, int y1)
{
    int t = omm_tan_scaled(a->angle, x) - a->min;
    a->bins[y0 + t]++;
    a->bins[y1 + t]--;
}

static void project_all(Accumulator *a, OmmBlobInfo *blobs, int n_blobs, int angle)
{
    a->angle = angle;
    memset(a->bins, 0, (a->max - a->min + 1) * sizeof(int));
    for (int i = 0; i < n_blobs; i++)
    {
        if (blobs[i].index >= 0)
            project(a, blobs[i].min_x, blobs[i].min_y, blobs[i].max_y);
    }
}

static int accumulator_score(Accumulator *a)
{
    int n = a->max - a->min + 1;
    int c = 0;
    int s = 0;
    for (int i = 0; i < n; i++)
    {
        c += a->bins[i];
        s += c * c;
    }
    return s;
}

// ____________________   cutting a column into lines   _______________________

static int escape_corridor(
    const int *restrict bins,
    int cursor,
    int *restrict value,
    int corridor_min,
    int corridor_max)
{
    assert(corridor_min <= corridor_max);
    assert(corridor_min > 0); // otherwise we could go past the end
    while (*value >= corridor_min && *value <= corridor_max)
        *value += bins[cursor++];
    return cursor;
}

static int skip_to_local_max(
    const int *restrict bins,
    int cursor,
    int *restrict value)
{
    assert(*value > 0 || bins[cursor] > 0);
    while (bins[cursor] >= 0)
    {
        *value += bins[cursor++];
    }
    return cursor;
}

static int skip_to_local_min(
    const int *restrict bins,
    int cursor,
    int n,
    int *restrict value,
    int *restrict out_plateau_start)
{
    while (1)
    {
        *out_plateau_start = cursor;
        while (cursor < n && !bins[cursor])
            cursor++;
        if (cursor >= n || bins[cursor] > 0)
            return cursor;
        *value += bins[cursor++];
    }
}   

/**
 * Find lines based on the projection profile 
 *  (given by its discrete derivative).
 *
 * In the output, every line is encoded by 2 numbers:
 *     the start and the end intercept, both at profile minimums.
 * The start is the rightmost point of its plateau;
 * the end is the leftmost point of its plateau.
 */
static int cut(
    int *restrict positions, 
    const int *restrict bins, 
    int min, 
    int max)
{
    const int line_coef = 2;
    int n = max - min + 1;
    assert(bins[0] == 0);     // left buffering 0
    assert(bins[n - 1] == 0); // right --//--
    int n_lines = 0;

    // find the end of the first plateau of 0s
    int plateau_end = 0;
    while (plateau_end < n  &&  !bins[plateau_end])
        plateau_end++;
    if (plateau_end == n)
        return 0;
    plateau_end--;

    int v = 0;
    int cursor = plateau_end + 1;
    int last_local_min = 0;

    // invariants:
    //     v == bins[0] + bins[1] + ... + bins[cursor - 1]
    //  
    //     the cursor has just escaped from a corridor upwards,
    //         which means either:
    //              - that v > corridor_max and cursor is 1 past that
    //              - OR that the cursor is 1 past a plateau of 0s. 
    //         (the difference will be eaten by skip_to_local_max()) 
    //
    //     last_local_min is what defined the corridor.
    //     plateau_end is the end of its plateau.
    //     It will certainly be the start of a line.
    while (cursor < n)
    {
        int last_local_max;
        while (1)
        {
            cursor = skip_to_local_max(bins, cursor, &v);
            last_local_max = v;
            cursor = escape_corridor(bins, cursor, &v, 
                                        last_local_max / line_coef + 1,
                                        last_local_max);
            if (v <= last_local_max)
            {
                assert(v <= last_local_max / line_coef);
                break;
            }

            assert(cursor < n);
        }
        int plateau_start;
        cursor = skip_to_local_min(bins, cursor, n, &v, &plateau_start);
        last_local_min = v;
        int last_min_plateau_end = cursor - 1;
        while (last_local_min > 0)
        {
            cursor = escape_corridor(bins, cursor, &v,
                                      last_local_min,
                                      line_coef * last_local_min);
            if (v >= last_local_min)
            {
                assert(v > line_coef * last_local_min);
                break;
            }

            cursor = skip_to_local_min(bins, cursor, n, &v, &plateau_start);
            last_local_min = v;
            last_min_plateau_end = cursor - 1;
        }

        // we got a line
        *positions++ = plateau_end + min;
        *positions++ = plateau_start + min;
        n_lines++;
        assert(2 * n_lines <= n);
        
        plateau_end = last_min_plateau_end;
    }

    return n_lines;
}

static OmmLayout *layout_new()
{
    OmmLayout *layout = (OmmLayout *) malloc(sizeof(OmmLayout));
    layout->line_intercepts = NULL;
    layout->blob_lines = NULL;
    return layout;
}

static void mark_blobs(
    OmmLayout *restrict layout, 
    OmmBlobInfo *restrict blobs, 
    int n_blobs,
    int *restrict counters)
{
    int n_lines = layout->n_lines;
    memset(counters, 0, n_lines * sizeof(int));
    memset(layout->blob_lines, 255, n_blobs * sizeof(int));

    int line = 0;
    for (int i = 0; i < n_blobs; i++)
    {
        if (blobs[i].index < 0)
            continue;
        int y = (blobs[i].min_y + blobs[i].max_y) / 2 
               - blobs[i].min_x * layout->skew_angle_num 
                                 / layout->skew_angle_den;
        if (y < layout->line_intercepts[2 * line])
        {
            // go up
            do 
            {
                line--;
            } while (line >= 0 && y < layout->line_intercepts[2 * line]);
            if (line < 0)
            {
                line = 0;
                continue;
            }
            if (y > layout->line_intercepts[2 * line + 1])
                continue;
        }
        else
        {
            // go down
            while (line < n_lines && y > layout->line_intercepts[2 * line + 1])
                line++;
            if (line >= n_lines)
            {
                line = n_lines - 1;
                continue;
            }
            if (y < layout->line_intercepts[2 * line])
                continue;
        }

        layout->blob_lines[i] = line;
        counters[line]++;
    } // loop over blobs
}

static void assign_blobs_to_lines(
    OmmLayout *restrict layout, 
    OmmBlobInfo *restrict blobs,
    int n_blobs)
{
    int n_lines = layout->n_lines;
    layout->n_blobs = n_blobs;
    layout->blob_lines = (int *) malloc(n_blobs * sizeof(int));
    layout->line_blobs = (int *) malloc(n_blobs * sizeof(int));
    layout->line_offsets = (int *) malloc((n_lines + 1) * sizeof(int));
    int *offsets = layout->line_offsets;

    mark_blobs(layout, blobs, n_blobs, offsets);

    // integrate offsets
    for (int i = 0, s = 0; i < n_lines; i++)
        offsets[i] = s += offsets[i];
    offsets[n_lines] = offsets[n_lines - 1];

    // count sort the blobs into lines
    // (I'm pretty sure I did this count sort already somewhere else)
    for (int i = 0; i < n_blobs; i++)
    {
        int line = layout->blob_lines[i];
        if (line < 0)
            continue;

        layout->line_blobs[--offsets[line]] = i;
    }
}

// __________________________    public functions   ___________________________

OmmLayout *omm_la_by_projection_profiles(
    OmmBlobInfo *blobs, 
    int n_blobs,
    int width,
    int height)
{
    OmmLayout *layout = layout_new();    
    int m = omm_tan_max_arg();
    int margin = omm_tan_scaled(m, width) + 1; // + 1 for a buffering 0
    Accumulator a1;
    Accumulator a2;
    a1.min = a2.min = -margin;
    a1.max = a2.max = height + margin;
    int size = height + 2 * margin + 1;
    a1.bins = (int *) malloc(size * sizeof(int));
    a2.bins = (int *) malloc(size * sizeof(int));

    Accumulator *best = &a1;
    Accumulator *candidate = &a2;

    project_all(best, blobs, n_blobs, 0);
    int best_v = accumulator_score(best);    

    for (int i = -m; i <= m; i++)
    {
        if (i == 0) // did that right before this loop
            continue;         
        project_all(candidate, blobs, n_blobs, i);
        int v = accumulator_score(candidate);
        if (v > best_v)
        {
            best_v = v;
            Accumulator *tmp = best;
            best = candidate;
            candidate = tmp;
        }
    }

    omm_tan(best->angle, &layout->skew_angle_num, &layout->skew_angle_den);
    layout->n_lines = cut(candidate->bins, best->bins, best->min, best->max);
    layout->line_intercepts = candidate->bins;
    free(best->bins);

    assign_blobs_to_lines(layout, blobs, n_blobs);

    return layout;
}

void omm_layout_free(OmmLayout *layout)
{
    if (!layout)
        return;
    if (layout->line_intercepts)
        free(layout->line_intercepts);
    if (layout->blob_lines)
        free(layout->blob_lines);
    if (layout->line_blobs)
        free(layout->line_blobs);
    if (layout->line_offsets)
        free(layout->line_offsets);
    free(layout);
}

