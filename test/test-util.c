/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2014  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */


#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "omm-test.h"
#include "omm-test-util.h"

static void circular_k_sums_dumb(int *v, int n, int k, int *result, int stride)
{
    for (int i = 0; i < n; i++)
    {
        int sum = 0;
        for (int j = i; j < i + k; j++)
            sum += v[j % n];
        result[i * stride] = sum;
    }
}

static void test_circular_k_sums()
{
    TEST_CASE("utils/circular_k_sums")
    for (int n = 1; n < 10; n++)
    {
        for (int k = 1; k <= n; k++)
        {
            int in[n];
            int out[n];
            int out_dumb[n];
            for (int i = 0; i < n; i++)
                in[i] = rand();
            omm_circular_k_sums(in, n, k, out, 1);
            circular_k_sums_dumb(in, n, k, out_dumb, 1);
            assert(!memcmp(out, out_dumb, n * sizeof(int)));
        }
    }
    MUST_WORK;
}


static void test_binary_unpack()
{
    TEST_CASE("util/binary_unpack")
        const int n = 3;
        const uint8_t gt[] = {
            0, 0, 0,
            0, 0, 1,
            0, 1, 0,
            0, 1, 1,
            1, 0, 0,
            1, 0, 1,
            1, 1, 0,
            1, 1, 1
        };
        uint8_t buf[n];
        for (int i = 0; i < (1 << n); i++)
        {
            omm_binary_unpack(buf, n, i);
            assert(!memcmp(buf, gt + i * n, n));
        }
    MUST_WORK
}

int main()
{
    srand(getpid());
    /*int w = 20;
    int h = 10;
    char img[w * h];
    omm_random_raster(w, h, 128, 5, img);
    dump_raster(img, w, h);*/
    test_binary_unpack();
    test_circular_k_sums();
    return 0;
}
