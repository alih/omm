/*
 * Ocromancer - an OCR system
 * Copyright (C) 2009-2010  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OMM_CONTOURS_H
#define OMM_CONTOURS_H

#include <stdint.h>

typedef struct OmmPoint
{
    int16_t x;
    int16_t y;
} OmmPoint;

typedef struct OmmContour
{
    OmmPoint *points;
    int n;
} OmmContour;

OmmContour *omm_contours_trace(
        const uint8_t *restrict image, int w, int h,
        /* out: */ int *restrict n_contours);

void omm_contours_render(uint8_t *restrict image, int w, int h,
                         OmmContour *restrict contours, int n);

void omm_contours_free(OmmContour *contours, int n);
void omm_contours_test(void);

/**
 * OmmContourRuns is an alternative way to represent a contour.
 *
 * If it occurs in the future that we can't live without contour runs,
 * then maybe it would be worth to trace into them directly.
 * 
 * The runs are signed, and are alternating between horizontal and vertical.
 * The even runs (including the first, because it's number 0) are x shifts,
 * and the odd ones are y. Add the run to x or y to get to the new point.
 *
 * (x0, y0) is the first point and it also must be the last one.
 */
typedef struct OmmContourRuns
{
    int x0;
    int y0;
    int n;
    int16_t *runs;
} OmmContourRuns;

OmmContourRuns *omm_contour_runs_new(OmmContour *contour);
void omm_contour_runs_free(OmmContourRuns *runs);
int omm_contour_runs_length(const OmmContourRuns *contour_runs);
void omm_contour_runs_unfold(const OmmContourRuns *contour_runs,
                             OmmContour *result);
    

#endif
