/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "omm-labeling.h"
#include "omm-rle.h"
#include "omm-test-util.h"
#include "omm-cc-flood.h"
#include "omm-test.h"


static void test_labeling(int w, int h)
{
    TEST_CASE("labeling(%d, %d)", w, h)
        int n = w * h;
        uint8_t *pixels = (uint8_t *) malloc(n);
        uint32_t *result_flood = (uint32_t *) malloc(n * sizeof(uint32_t));
        uint32_t *result_rle = (uint32_t *) malloc(n * sizeof(uint32_t));
        int k = 1 << n;
        for (int i = 0; i < k; i++)
        {
            omm_binary_unpack(pixels, n, i);
            int n_cc_flood = omm_cc_flood(result_flood, pixels, w, h);
            OmmImage *image = omm_image_new_from_raster(pixels, w, h);
            uint32_t *labels = (uint32_t *) malloc(image->n_runs / 2 * sizeof(uint32_t));
            int n_cc_rle = omm_label(labels, image);
            omm_rle_render_labels(result_rle, n, labels, image->runs, image->n_runs);
            free(labels);
            omm_image_free(image);
            assert(n_cc_flood == n_cc_rle);
            assert(!memcmp(result_flood, result_rle, n * sizeof(uint32_t)));
        }
        free(pixels);
        free(result_flood);
        free(result_rle);
    MUST_WORK;
}

int main()
{
    for (int w = 1; w < 5; w++)
    {
        for (int h = 1; h < 5; h++)
        {
            if (w == 4 && h == 4)
                break;
            test_labeling(w, h);
        }
    }
    return EXIT_SUCCESS;
}
