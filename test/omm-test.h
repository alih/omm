/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2015  Ilya Mezhirov
 *
 * Unlike the rest of the OMM system, this file is 
 * licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * \file omm-test.h
 *
 * This is OMM's minimalistic test framework 
 * that runs every test in a separate process.
 *
 * Its advantage is that it has only one construct, TEST_CASE ... MUST_XXX,
 * and there's nothing else. Here's how a test could look like:
 * \code
 *    int a = 1;
 *    TEST_CASE("foo")
 *        assert(a == 1);
 *    MUST_WORK;    // semicolon is optional
 * \endcode
 *
 * Changes made in a test don't affect anything that happens after MUST_XXX.
 * Consider this example:
 *
 * \code
 *    int a = 1;
 *    TEST_CASE("changing a local variable")
 *        a = 2;
 *    MUST_WORK;
 *    TEST_CASE("change isn't visible here")
 *        assert(a == 1);
 *    MUST_WORK;
 * \endcode
 * 
 * This works because every test is run in a forked process.
 *
 * The MUST_FAIL test is successful when the exit code of the child is nonzero 
 * and the error message is printed to stderr exactly as specified.
 *
 * \code
 *   TEST_CASE("test for failure")
 *       fprintf(stderr, "Oh no!\n");
 *       exit(EXIT_FAILURE);
 *   MUST_FAIL("Oh no!\n");
 * \endcode
 *
 * You can also check for termination with a signal:
 * \code
 *   TEST_CASE("division by 0")
         printf("%d", 1 / abs(0)); // abs() to prevent compiler warnings
 *   MUST_TERMINATE(SIGFPE);
 * \endcode
 *
 * If the environment variable OMM_TEST_ONLY is set, 
 * then no forking will be done, but only the test 
 * with the given name will be run. Consider this example:
 *
 * \code
 * for (int i = 0; i < 10; i++)
 * {
 *     TEST_CASE("test that %d != 5", i)
 *         assert(i != 5);
 *     MUST_WORK;
 * }
 * \endcode
 *
 * One of those tests will fail, but you might get into debugging trouble
 * because of forking. In this case, you can debug it by setting 
 * OMM_TEST_ONLY="test that 5 != 5".
 *
 * To use the test framework, include "omm-test.h" and compile your program
 * in C99-compatible mode with POSIX support. For gcc, use
 * \code
 * -std=c99 -D_POSIX_SOURCE
 * \endcode
 */


#ifndef OMM_TEST_H
#define OMM_TEST_H


#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

#ifndef OMM_ON_TEST_FAIL
#define OMM_ON_TEST_FAIL exit(EXIT_FAILURE)
#endif

#define OMM_STRINGIFY_AUX(X) #X     /* otherwise doesn't work */
#define OMM_STRINGIFY(X) OMM_STRINGIFY_AUX(X)

#define OMM_LOCATION (__FILE__ ":"  OMM_STRINGIFY(__LINE__))

#define OMM_TEST_NAME_SIZE 70

#define OMM_COLORED_MESSAGE(COLOR, MSG)              \
    do                                               \
    {                                                \
        if (isatty(1))                               \
            printf("[\e[" COLOR "m" MSG "\e[0m]\n"); \
        else                                         \
            printf("[" MSG "]\n");                   \
    } while(0)

#define OMM_PERROR_AND_EXIT     \
    do                          \
    {                           \
        perror(OMM_LOCATION);   \
        exit(EXIT_FAILURE);     \
    } while(0)

/**
 * The opening part of the TEST_CASE - MUST construct.
 * The test name is given in printf-like arguments.
 * The test name should be less than 70 characters long.
 *
 * If the environment variable OMM_TEST_ONLY is set,
 * then only the test with that name will be executed and without fork.
 */
#define TEST_CASE(...)                                           \
    do                                                           \
    {                                                            \
        char omm_test_name[OMM_TEST_NAME_SIZE];                  \
        snprintf(omm_test_name, sizeof(omm_test_name), __VA_ARGS__);    \
        char *omm_test_env = getenv("OMM_TEST_ONLY");            \
        /* empty OMM_TEST_ONLY is counted as NULL */             \
        if (omm_test_env && !*omm_test_env)                      \
            omm_test_env = NULL;                                 \
        if (omm_test_env && strcmp(omm_test_name, omm_test_env)) \
            break;                                               \
        int omm_test_filedes[2];                                 \
        if (!omm_test_env && pipe(omm_test_filedes))             \
            OMM_PERROR_AND_EXIT;                                 \
        if (omm_test_env)                                        \
            printf("Running %s\n", omm_test_name);               \
        else                                                     \
            printf("%-" OMM_STRINGIFY(OMM_TEST_NAME_SIZE) "s  ", omm_test_name);  \
        fflush(stdout);                                          \
        int omm_test_child_pid = 0;                              \
        if (!omm_test_env)                                       \
            omm_test_child_pid = fork();                         \
        if (!omm_test_child_pid || omm_test_env)                 \
        {                                                        \
            if (!omm_test_env)                                   \
            {                                                    \
                close(omm_test_filedes[0]);                      \
                dup2(omm_test_filedes[1], 2);                    \
            }
            /* here comes the code between TEST_CASE and MUST_* */



/**
 * Check that the child process exited with a code that satisfies the predicate,
 * that the terminating signal is exactly SIG (or 0 not to expect any signal),
 * and that the stderr output is exactly ERRMSG.
 */
#define MUST_EXIT_WITH_EXIT_CODE_SATISFYING(EXIT_CODE_PREDICATE, SIG, ERRMSG) \
            exit(EXIT_SUCCESS);                                      \
        }                                                            \
        if (omm_test_child_pid)                                      \
        {                                                            \
            /* we're the parent */                                   \
            int omm_test_signal = (SIG);                             \
            close(omm_test_filedes[1]);                              \
            FILE *omm_test_f = fdopen(omm_test_filedes[0], "r");     \
            const char *omm_test_errmsg = (ERRMSG);                  \
            const char *omm_test_p = omm_test_errmsg;                \
            int omm_test_failed = 0;                                 \
            for (omm_test_p = omm_test_errmsg; ; omm_test_p++)       \
            {                                                        \
                int omm_test_c = fgetc(omm_test_f);                  \
                if ((*omm_test_p != omm_test_c && *omm_test_p)       \
                  ||(omm_test_c != EOF && !*omm_test_p))             \
                {                                                    \
                    omm_test_failed = 1; /* stderr output mismatch */\
                    OMM_COLORED_MESSAGE("1;31", " FAIL ");           \
                    /* copy the matching part to stderr */           \
                    fwrite(omm_test_errmsg, omm_test_p - omm_test_errmsg, 1, stderr); \
                    /* copy the rest to stderr */                    \
                    fputc(omm_test_c, stderr);                           \
                    while ((omm_test_c = fgetc(omm_test_f)) != EOF)      \
                        fputc(omm_test_c, stderr);                       \
                    break;                                               \
                }                                                        \
                if (!*omm_test_p)                                        \
                    break;                                               \
            }                                                            \
            fclose(omm_test_f);                                          \
            int omm_test_status;                                     \
            if (waitpid(omm_test_child_pid, &omm_test_status, 0) !=  \
                                        omm_test_child_pid)          \
                OMM_PERROR_AND_EXIT;                                 \
            if (!omm_test_failed && WIFSIGNALED(omm_test_status))    \
            {                                                        \
                if (!omm_test_signal ||                              \
                        (omm_test_signal > 0 && WTERMSIG(omm_test_status) != omm_test_signal))        \
                {                                                    \
                    OMM_COLORED_MESSAGE("1;31", "SIGNAL");           \
                    omm_test_failed = 1;                             \
                }                                                    \
            }                                                        \
            else if (!omm_test_failed && !(WEXITSTATUS(omm_test_status) EXIT_CODE_PREDICATE))    \
            {                                                        \
                OMM_COLORED_MESSAGE("1;31", " FAIL ");           \
                omm_test_failed = 1;                                 \
            }                                                        \
            if (omm_test_failed)                                     \
            {                                                        \
                OMM_ON_TEST_FAIL;                                    \
            }                                                        \
            else                                                     \
            {                                                        \
                OMM_COLORED_MESSAGE("1;32", "  OK  ");               \
            }                                                        \
        }                                                            \
    } while(0);

#define MUST_WORK           MUST_EXIT_WITH_EXIT_CODE_SATISFYING(== 0, 0, "")
#define MUST_PRINT(MSG)     MUST_EXIT_WITH_EXIT_CODE_SATISFYING(== 0, 0, MSG)
#define MUST_FAIL(ERRMSG)   MUST_EXIT_WITH_EXIT_CODE_SATISFYING(!= 0, -1, ERRMSG)
#define MUST_FAIL_WITH_SIGNAL(SIG, ERRMSG)   MUST_EXIT_WITH_EXIT_CODE_SATISFYING(!= 0, SIG, ERRMSG)
#define MUST_TERMINATE(SIG) MUST_EXIT_WITH_EXIT_CODE_SATISFYING(|| 1, SIG, "")

#endif
