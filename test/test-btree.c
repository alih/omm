/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */


#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "omm-btree.h"
#include "omm-test.h"

typedef int (*Comparator)(const void *, const void *);

// check_order() {{{

typedef struct
{
    Comparator compare;
    int count;
    const void *last;    
} VisitorCheckOrderData;

static void visitor_check_order(void *ptr, void *d)
{
    VisitorCheckOrderData *data = (VisitorCheckOrderData *) d;
    if (data->count)
        assert(data->compare(data->last, ptr) < 0);
    data->count++;
    data->last = ptr;
}

/**
 * Assert that the pointers stored in the tree are visited in order.
 * 
 * \return  The number of nodes in the tree.
 */
static int check_order(OmmBTree *btree, Comparator compare)
{
    VisitorCheckOrderData data;
    data.compare = compare;
    data.count = 0;
    data.last = NULL;
    omm_btree_foreach(btree, visitor_check_order, &data);
    return data.count;
}

// }}}

static int intcmp(const void *p1, const void *p2)
{
    int i = * (const int *) p1;
    int j = * (const int *) p2;
    return i < j ? -1 :
           i > j ?  1 : 0;
}

static void test_btree_random(int n, int max_children)
{
    TEST_CASE("btree/random(n = %d, max_children = %d)", n, max_children)
        int *numbers = (int *) malloc(n * sizeof(int));
        OmmBTree *btree = omm_btree_new(intcmp);
        if (max_children)
            omm_btree_set_max_children(btree, max_children);
        assert(!omm_btree_find(btree, 0));
        for (int i = 0; i < n; i++)
        {
            check_order(btree, intcmp);

            // check that the tree finds everything it has so far
            for (int j = 0; j < i; j++)
            {
                int *p = omm_btree_insert(btree, &numbers[j]);
                assert(p == &numbers[j]);
            }

            check_order(btree, intcmp);

            // add a new node
            while (1)
            {
                int a = numbers[i] = rand();
                int *p = omm_btree_insert(btree, &numbers[i]);
                int k = p - numbers;
                assert(k >= 0 && k <= i);
                if (k == i)
                    break;
                assert(numbers[k] == a); // really a collision
            }
        }
        omm_btree_free(btree);
        free(numbers);
    MUST_WORK;
}

static OmmBTree *global_tree = NULL;

static void free_global_tree()
{
    if (global_tree)
        omm_btree_free(global_tree);
}

static void test_btree_set_max_children()
{
    global_tree = omm_btree_new(intcmp);
    atexit(free_global_tree);
    
    TEST_CASE("btree/max_children/too_little")
        omm_btree_set_max_children(global_tree, 2);
    MUST_FAIL("B-tree cannot have max_children less than 3\n");

    TEST_CASE("btree/max_children/nonempty")
        int a = 0;
        omm_btree_insert(global_tree, &a);
        omm_btree_set_max_children(global_tree, 5);
    MUST_FAIL("Cannot set parameters on a non-empty B-tree\n");
}

int main()
{
    test_btree_random(500, 0);
    test_btree_random(500, 4);
    test_btree_random(500, 5);
    test_btree_random(500, 6);

    test_btree_set_max_children();
    return EXIT_SUCCESS;
}

// vim: set foldmethod=marker:
