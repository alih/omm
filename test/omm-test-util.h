/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */



#ifndef OMM_TEST_UTIL_H
#define OMM_TEST_UTIL_H

#include <stdint.h>

/// Convert an n-bit integer i into a n-byte 0-or-1 line.
void omm_binary_unpack(uint8_t *buf, int n, int i);

/**
 * Calculates circular sums of k items:
 *
 *  result[i * stride] = sum_i^{i + k - 1} v[i],
 *  where sum wraps over the boundary of v, that is, n.
 */
void omm_circular_k_sums(const int *v, int n, int k, int *result, int stride);

void omm_random_raster(int width, int height, int mean, int sigma, char *result);

void run_on_all_images   (void (*f)(const uint8_t *image, int w, int h), int min_w, int max_w, int min_h, int max_h);
void run_on_random_images(void (*f)(const uint8_t *image, int w, int h), int min_w, int max_w, int min_h, int max_h, int n_iter);
void omm_dump_raster(const uint8_t *raster, int width, int height);

#endif
