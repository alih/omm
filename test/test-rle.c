/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "omm-rle.h"
#include "omm-image.h"
#include "omm-test-util.h"
#include "omm-test.h"

static void check_extract_and_render(const uint8_t *pixels, int width)
{
    uint8_t *rendered = (uint8_t *) malloc(width);
    uint8_t *runs = (uint8_t *) malloc(omm_rle_max_size(width));
    int n_runs = omm_rle_extract(runs, pixels, width);
    omm_rle_render(rendered, width, runs, n_runs);
    assert(!memcmp(pixels, rendered, width));
    free(runs);
    free(rendered);
}

static void check_extract_and_render_2d(
    const uint8_t *pixels, 
    int width, 
    int height)
{
    uint8_t *rendered = (uint8_t *) malloc(width * height);
    OmmImage *rle = omm_image_new_from_raster(pixels, width, height);
    assert(omm_image_check(rle));
    omm_rle_render(rendered, width * height, rle->runs, rle->n_runs);
    assert(!memcmp(pixels, rendered, width * height));
    omm_image_free(rle);
    free(rendered); 
}

static void check_extract_packed_and_render(
    const uint8_t *packed,
    const uint8_t *pixels, int width)
{
    uint8_t *rendered = (uint8_t *) malloc(width);
    uint8_t *runs = (uint8_t *) malloc(omm_rle_max_size(width));
    int n_runs = omm_rle_extract_packed(runs, packed, width, 0);
    omm_rle_render(rendered, width, runs, n_runs);
    assert(!memcmp(pixels, rendered, width));
    free(runs);
    free(rendered);
}

static void test_extract_and_render_on_short_lines(int n)
{
    TEST_CASE("rle/extract_and_render_on_short_lines(n = %d)", n)
        // k is the number of bits to shift an n-bit word to the left
        // so that its length (in bits) will be a multiple of 8.
        // p is its length in bytes.
        int p = (n + 7) / 8;
        int k = p * 8 - n;

        uint8_t buf[n];
        int m = 1 << n;
        for (int i = 0; i < m; i++)
        {
            omm_binary_unpack(buf, n, i);
            check_extract_and_render(buf, n);
            uint8_t packed[p];
            for (int j = 0; j < p; j++)
                packed[j] = ((i << k) >> 8 * (p - j - 1)) & 0xFF; 
            check_extract_packed_and_render(packed, buf, n);
        }
    MUST_WORK
}

static void test_extract_and_render_on_small_images(int w, int h)
{
    TEST_CASE("rle/extract_and_render_on_small_images(w = %d, h = %d)", w, h)
        run_on_all_images(check_extract_and_render_2d, w, w, h, h);
    MUST_WORK
}

static void test_extract_and_render_on_long_lines(int n)
{
    TEST_CASE("rle/extract_and_render_on_long_lines(n = %d)", n)
        int p = (n + 7) / 8;

        uint8_t buf[n];
        for (int j = 0; j < n; j++)
            buf[j] = 0;
        check_extract_and_render(buf, n);
        uint8_t packed[p];
        for (int j = 0; j < p; j++)
            packed[j] = 0; 
        check_extract_packed_and_render(packed, buf, n);
        for (int j = 0; j < n; j++)
            buf[j] = 1;
        check_extract_and_render(buf, n);
        for (int j = 0; j < p; j++)
            packed[j] = 0xFF; 
        check_extract_packed_and_render(packed, buf, n);
    MUST_WORK
}

static void check_32_to_8(
    const uint32_t *runs32, 
    const uint8_t *runs8,
    int n32,
    int n8)
{
    int w = 0;
    for (int i = 0; i < n32; i++)
        w += runs32[i];

    uint8_t *runs = (uint8_t *) malloc(omm_rle_max_size(w));
    int n = omm_rle_32_to_8(runs, w, runs32, n32);

    assert(n == n8);
    assert(!memcmp(runs, runs8, n));
    free(runs);
}

static void smoke_test_32_to_8()
{
    TEST_CASE("rle/32_to_8/smoke")
        uint32_t in1[] = {0, 512};
        uint8_t out1[] = {0, 255, 0, 255, 0, 2};
        uint32_t in2[] = {512};
        uint8_t out2[] = {255, 0, 255, 0, 2, 0};
        uint32_t in3[] = {10, 20, 30, 40};
        uint8_t out3[] = {10, 20, 30, 40};
        check_32_to_8(in1, out1, sizeof(in1)/sizeof(uint32_t), sizeof(out1));
        check_32_to_8(in2, out2, sizeof(in2)/sizeof(uint32_t), sizeof(out2));
        check_32_to_8(in3, out3, sizeof(in3)/sizeof(uint32_t), sizeof(out3));
    MUST_WORK

    TEST_CASE("rle/32_to_8/bound")
        uint32_t runs32[] = {0, 512};
        uint8_t runs[5];
        omm_rle_32_to_8(runs, /* max_width: */ 3, runs32, 2);
    MUST_FAIL("Error: invalid 32-bit RLE code (line too short)\n");
}

static void test_crop_on_line(const uint8_t *pixels, int n)
{
    int size = omm_rle_max_size(n);
    uint8_t *runs =         (uint8_t *) malloc(size);
    uint8_t *cropped_runs = (uint8_t *) malloc(size);
    uint8_t *cropped_line = (uint8_t *) malloc(n);
    int n_runs = omm_rle_extract_with_max_run(runs, pixels, n, 3);
    for (int x0 = -1; x0 <= n + 1; x0++)
    {
        for (int x1 = x0; x1 <= n + 1; x1++)
        {
            int n_cropped_runs = 
                omm_rle_crop(cropped_runs, runs, n_runs, x0, x1);
            if (x1 <= x0)
            {
                assert(n_cropped_runs == 0);
                continue;
            }

            int min_x = x0 >= 0 ? x0 : 0;
            int max_x = x1 <= n ? x1 : n;
            int w = max_x - min_x;
            assert(omm_rle_check(cropped_runs, n_cropped_runs, w));
            omm_rle_render(cropped_line, w, cropped_runs, n_cropped_runs);
            assert(!memcmp(cropped_line, pixels + min_x, w));
        }
    }
    free(runs);
    free(cropped_runs);
    free(cropped_line);
}

static void test_crop_on_all_lines(int n)
{
    int k = 1 << n;
    uint8_t *pixels = (uint8_t *) malloc(n);
    for (int i = 0; i < k; i++)
    {
        omm_binary_unpack(pixels, n, i);
        test_crop_on_line(pixels, n);
    }
    free(pixels);
}

int main()
{
    for (int i = 5; i < 12; i++)
        test_extract_and_render_on_short_lines(i);

    test_extract_and_render_on_small_images(2, 2);
    test_extract_and_render_on_small_images(3, 3);
    test_extract_and_render_on_small_images(2, 4);
    test_extract_and_render_on_small_images(4, 2);
    
    test_extract_and_render_on_long_lines(255);
    test_extract_and_render_on_long_lines(256);
    test_extract_and_render_on_long_lines(1000);
    test_extract_and_render_on_long_lines(10000);

    smoke_test_32_to_8();

    TEST_CASE("rle/crop")
        for (int i = 0; i < 11; i++)
            test_crop_on_all_lines(i);
    MUST_WORK; 

    return EXIT_SUCCESS;
}
