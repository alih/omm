/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */


#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include "omm-test-util.h"

void omm_binary_unpack(uint8_t *buf, int n, int i)
{
    for (int j = 0; j < n; j++)
        buf[j] = ((1 << (n - 1 - j)) & i ? 1 : 0);
}

void omm_circular_k_sums(const int *v, int n, int k, int *result, int stride)
{
    assert(k <= n);
    
    int s = 0;
    for (int i = 0; i < k; i++)
        s += v[i];
    *result = s;

    for (int i = 0, i_cap = n - k; i < i_cap; i++)
    {
        s += v[i + k] - v[i];
        result += stride;
        *result = s;
    }

    for (int i = 0, i_cap = k - 1; i < i_cap; i++)
    {
        s += v[i] - v[n - k + i];
        result += stride;
        *result = s;
    } 
}

void omm_random_raster(int width, int height, int mean, int sigma, char *result)
{
    if (sigma < 0)
        sigma = (width < height ? width : height) / 8;
    assert(sigma < width);
    assert(sigma < height);

    int n = width * height;
    int *buf1 = (int *) malloc(n * sizeof(int));
    int *buf2 = (int *) malloc(n * sizeof(int));

    for (int i = 0; i < n; i++)
        buf1[i] = rand() & 0xFF;

    if (sigma > 1)
    {
        for (int y = 0; y < height; y++)
            omm_circular_k_sums(buf1 + y * width, width, sigma, buf2 + y, height);
        for (int x = 0; x < width; x++)
            omm_circular_k_sums(buf2 + x * height, height, sigma, buf1 + x, width);
    }

    int t = sigma > 1 ? mean * sigma * sigma : mean;
    for (int i = 0; i < n; i++)
        result[i] = !(buf1[i] >= t);

    free(buf1);
    free(buf2);
}

void run_on_all_images(void (*f)(const uint8_t *image, int w, int h), int min_w, int max_w, int min_h, int max_h)
{
    for (int w = min_w; w <= max_w; w++) for (int h = min_h; h <= max_h; h++)
    {
        int n = w * h;
        uint8_t buf[n];
        int m = 1 << n;
        for (int i = 0; i < m; i++)
        {
            omm_binary_unpack(buf, n, i);
            f(buf, w, h);
        }
    }
}

void run_on_random_images(void (*f)(const uint8_t *image, int w, int h), int min_w, int max_w, int min_h, int max_h, int n_iter)
{
    for (int iter = 0; iter < n_iter; iter++)
        for (int w = min_w; w <= max_w; w++)
            for (int h = min_h; h <= max_h; h++)
    {
        int n = w * h;
        uint8_t buf[n];
        omm_random_raster(w, h, 128, -1, (char *) buf);
        f(buf, w, h);
    }
}

void omm_dump_raster(const uint8_t *raster, int width, int height)
{
    for (int y = 0; y < height; y++)
    {
        const uint8_t *row = raster + y * width;
        for (int x = 0; x < width; x++)
            putchar(row[x] ? '@' : '.');
        putchar('\n');
    }
}
