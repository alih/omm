/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */


#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include "omm-cc-flood.h"

/*
 * We organize a stack using the memory of the labeling.
 * The stack items are pixel offsets.
 * The `top' variable has the offset of the stack top,
 * the memory at that offset has the offset of the next item,
 * etc. until UINT32_MAX.
 */

/**
 * Push the given offset to the stack,
 * but only if the corresponding pixel is black.
 * Also clear that pixel.
 */
static void push(
    uint32_t *restrict top, 
    uint32_t *restrict mem,
    uint32_t offset,
    uint8_t *restrict pixels)
{
    if (pixels[offset])
    {
        pixels[offset] = 0;
        mem[offset] = *top;
        *top = offset;
    }
}

/**
 * Pop from the stack. 
 * Fill the vacated memory cell with the given color.
 */
static uint32_t pop(
    uint32_t *restrict top,
    uint32_t *restrict mem,
    uint32_t color)
{
    uint32_t result = *top;
    uint32_t *p = &mem[*top];
    *top = *p; 
    *p = color; 
    return result;
}

/**
 * Remove a connected component from `pixels'
 * and paint the corresponding elements of `labels'
 * with the given color.
 */
static void flood(
    uint32_t *restrict labels,
    uint8_t *restrict pixels,
    int width,
    int height,
    uint32_t color,
    uint32_t start)
{
    uint32_t top = UINT32_MAX;
    push(&top, labels, start, pixels);
    uint32_t above_last_line = (uint32_t) width * (height - 1);
    while(top != UINT32_MAX)
    {
        uint32_t c = pop(&top, labels, color);
        if (c >= (uint32_t) width)
            push(&top, labels, c - width, pixels);
        if (c < above_last_line)
            push(&top, labels, c + width, pixels);
        int x = c % width;
        if (x > 0)
            push(&top, labels, c - 1, pixels);
        if (x < width - 1)
            push(&top, labels, c + 1, pixels);
    } 
}

uint32_t omm_cc_flood_raw(
    uint32_t *restrict labels, 
    uint8_t *restrict pixels,
    int width, 
    int height)
{
    uint32_t n_blobs = 0;
    for (int y = 0; y < height; y++) for (int x = 0; x < width; x++)
    {
        if (pixels[y * width + x])
        {
            flood(labels, pixels, width, height, n_blobs, y * width + x);
            n_blobs++;
        }
    }
    return n_blobs;
}

uint32_t omm_cc_flood(
    uint32_t *restrict labels, 
    const uint8_t *restrict pixels,
    int width, 
    int height)
{
    size_t area = width * height;
    uint8_t *buf = (uint8_t *) malloc(area);
    memcpy(buf, pixels, area);
    memset(labels, 255, area * sizeof(uint32_t));
    uint32_t n_blobs = omm_cc_flood_raw(labels, buf, width, height);
    free(buf);
    return n_blobs;
}
