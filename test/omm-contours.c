/*
 * Ocromancer - an OCR system
 * Copyright (C) 2009-2010  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "omm-contours.h"


static OmmPoint *trace(const uint8_t *restrict image, 
                       int w, int h,
                       int x0, int y0, int dx0, int dy0,
                       int *p_n_points)
{
    int allocated = 32;
    int n = 0;
    OmmPoint *points = (OmmPoint *) malloc(allocated * sizeof(OmmPoint));
    if (!points)
        return NULL;

    int dx = dx0; // the movement direction
    int dy = dy0;

    assert((dx ==  0 && dy == 1)
        || (dx ==  0 && dy == -1)
        || (dx ==  1 && dy == 0)
        || (dx == -1 && dy == 0));


    int x = x0;
    int y = y0;

    int x1, y1; // the black pixel on the left side of the current vector
    int x2, y2; // the white pixel on the right side of the current vector

    // Convert between-pixel to pixel coordinates.
    x1 = x0 + ( dx + dy - 1) / 2;
    y1 = y0 + (-dx + dy - 1) / 2;
    x2 = x0 + ( dx - dy - 1) / 2;
    y2 = y0 + ( dx + dy - 1) / 2;

    assert(x1 >= 0 && x1 < w
        && y1 >= 0 && y1 < h
        && image[y1 * w + x1] != 0);

    assert(x2 < 0 || x2 >= w
        || y2 < 0 || y2 >= h
        || image[y2 * w + x2] == 0);

    // The main loop
    do
    {
        // Append the (x,y) point to the list
        if (n == allocated)
        {
            allocated *= 2;
            points = (OmmPoint *) realloc(points, allocated * sizeof(OmmPoint));
            if (!points)
                return NULL;
        }
        points[n].x = x;
        points[n].y = y;
        n++;

        x += dx;
        y += dy;

        int x3 = x1 + dx;
        int y3 = y1 + dy;
        int b3 = (x3 >= 0) && (x3 < w)
              && (y3 >= 0) && (y3 < h)
              && (image[y3 * w + x3] != 0);

        if (!b3)
        {
            // turn left
            dx = x1 - x2;
            dy = y1 - y2;
            x2 = x3;
            y2 = y3;
            continue;
        }

        int x4 = x2 + dx;
        int y4 = y2 + dy;

        if ((x4 >= 0) && (x4 < w)
         && (y4 >= 0) && (y4 < h)
         && (image[y4 * w + x4] != 0))
        {
            // turn right
            dx = x2 - x1;  dy = y2 - y1;
            x1 = x4;       y1 = y4;
        }
        else
        {
            // go straight
            x1 = x3; y1 = y3;
            x2 = x4; y2 = y4;
        }
    } while (x != x0 || y != y0 || dx != dx0 || dy != dy0);
    
    *p_n_points = n;
    return (OmmPoint *) realloc(points, n * sizeof(OmmPoint));
}

static void mark(uint8_t *restrict map, int w, 
                 OmmContour *restrict contour, uint8_t val)
{
    int n = contour->n;
    for (int i = 0; i < n; i++)
    {
        OmmPoint p1 = contour->points[i];
        OmmPoint p2 = contour->points[(i + 1) % n];

        if (p1.y == p2.y)
            continue;

        assert(p1.x == p2.x);
        int x = p1.x;

        if (x >= w)
            continue;

        int y = p1.y < p2.y ? p1.y : p2.y;
        assert(map[y * w + x] != val);
        map[y * w + x] = val;
    }
}

static void diff_x(uint8_t *restrict result, 
             const uint8_t *restrict image, int w, int h)
{
    for (int y = 0; y < h; y++)
    {
        result[y * w] = image[y * w];
        for (int x = 1; x < w; x++)
            result[y * w + x] = image[y * w + x] ^ image[y * w + x - 1];
    }
}

static void invert_diff_x(uint8_t *image, int w, int h)
{
    for (int y = 0; y < h; y++)
        for (int x = 1; x < w; x++)
            image[y * w + x] ^= image[y * w + x - 1];
}

OmmContour *omm_contours_trace(
        const uint8_t *restrict image, int w, int h,
        /* out: */ int *restrict p_n_contours)
{
    uint8_t *map = (uint8_t *) malloc(w * h);
    if (!map)
        goto bad;

    int allocated = 32;
    OmmContour *contours = (OmmContour *) malloc(allocated * sizeof(OmmContour));
    if (!contours)
        goto bad;
    int n = 0;

    diff_x(map, image, w, h);

    for (int y = 0; y < h; y++) for (int x = 0; x < w; x++)
    {
        assert((image[y * w + x] & ~1) == 0); // assert(image[y,x] is 0 or 1)

        if (!map[y * w + x])
            continue;

        // found a new contour
        if (allocated == n)
        {
            allocated *= 2;
            contours = (OmmContour *) realloc(contours, allocated * sizeof(OmmContour));
            if (!contours)
                goto bad;
        }

        if (image[y * w + x] != 0)
            contours[n].points = trace(image, w, h, x, y, 0, 1, &contours[n].n);
        else
            contours[n].points = trace(image, w, h, x, y + 1, 0, -1, &contours[n].n);

        if (!contours[n].points)
            goto bad;

        mark(map, w, &contours[n], 0);
        n++;
    }
    free(map);
    *p_n_contours = n;
    allocated = n > 0 ? n : 1;
    return (OmmContour *) realloc(contours, allocated * sizeof(OmmContour));
bad:
    if (map)
        free(map);
    return NULL;
}

void omm_contours_render(uint8_t *restrict image, int w, int h,
                         OmmContour *restrict contours, int n)
{
    memset(image, 0, w * h);
    for (int i = 0; i < n; i++)
        mark(image, w, &contours[i], 1);
    invert_diff_x(image, w, h);
}

void omm_contours_free(OmmContour *contours, int n)
{
    if (!contours)
        return;

    for (int i = 0; i < n; i++)
        free(contours[i].points);

    free(contours);
}


// _____________________________   contour runs   ____________________________


void omm_contour_runs_free(OmmContourRuns *runs)
{
    free(runs->runs);
    free(runs);
}

/**
 * This is a low-level function, and it's a bit tricky.
 * It might increase n by 1, assuming that there's enough memory.
 */
static void append_contour_run(OmmContourRuns *contour_runs, int dx, int dy)
{
    assert((dx == 0) ^ (dy == 0));

    int n = contour_runs->n;
    assert(n > 0);
    int16_t *runs = contour_runs->runs;

    if ((n & 1) ^ !dx) // if (the next run is a Y run) eq. (dx is non-zero)
    {
        // continue the last run - not applicable when n == 0
        runs[n-1] += dx + dy;
    }
    else
    {
        // start the next run
        runs[n] = dx + dy;
        contour_runs->n++;
    }
}

OmmContourRuns *omm_contour_runs_new(OmmContour *contour)
{
    OmmPoint *points = contour->points;

    int i;
    int i_cap = contour->n - 1;
    // skip X run (might be augmented by the last run)
    for (i = 0; i < i_cap; i++)
        if (points[i].x == points[i + 1].x)
            break;

    int16_t start_x_run = points[i].x - points[0].x;
    int end_start_x_run = i;

    // skip Y run (we must start with the X run)
    for (; i < i_cap; i++)
        if (points[i].y == points[i + 1].y)
            break;
    int16_t start_y_run = points[i].y - points[end_start_x_run].y;
    assert(start_y_run); // because X run was skipped previously

    // initialize
    OmmContourRuns *result = (OmmContourRuns *) malloc(sizeof(OmmContourRuns));
    int allocated = 8;
    result->runs = (int16_t *) malloc(allocated * sizeof(int16_t));
    result->n = 1;
    result->runs[0] = 0;
    result->x0 = points[i].x;
    result->y0 = points[i].y;

    // main loop
    for (;i < i_cap; i++)
    {
        // ensure enough memory
        if (result->n + 4 >= allocated) // 1 here and 3 after the loop
        {
            allocated *= 2;
            result->runs = (int16_t *) realloc(result->runs, 
                                               allocated * sizeof(int16_t));
        }

        append_contour_run(result, 
                           points[i + 1].x - points[i].x,
                           points[i + 1].y - points[i].y);
    }
    append_contour_run(result, 
                       points[0].x - points[i_cap].x,
                       points[0].y - points[i_cap].y);

    if (start_x_run)
        append_contour_run(result, start_x_run, 0);
    append_contour_run(result, 0, start_y_run);
    assert((result->n & 1) == 0); // because we just appended a non-zero Y run
    result->runs = (int16_t *) realloc(result->runs,
                                       result->n * sizeof(int16_t));
    return result;
}

int omm_contour_runs_length(const OmmContourRuns *contour_runs)
{
    int n = contour_runs->n;
    int16_t *runs = contour_runs->runs;

    int result = 0;
    for (int i = 0; i < n; i++)
        result += abs(runs[i]); 

    return result;
}

void omm_contour_runs_unfold(const OmmContourRuns *contour_runs, OmmContour *result)
{
    result->n = omm_contour_runs_length(contour_runs);

    // +1 because we fill in (x0, y0) twice
    result->points = (OmmPoint *) malloc((result->n + 1) * sizeof(OmmPoint));

    int n = contour_runs->n;
    int16_t *runs = contour_runs->runs;

    result->points[0].x = contour_runs->x0;
    result->points[0].y = contour_runs->y0;
    int k = 0; // last point index

    for (int i = 0; i < n; i += 2)
    {
        // X run
        int m = abs(runs[i]);
        int dx = runs[i] > 0 ? 1 : -1;
        for (int j = 0; j < m; j++)
        {
            result->points[k + 1].x = result->points[k].x + dx;
            result->points[k + 1].y = result->points[k].y;
            k++;
        }
        // Y run
        m = abs(runs[i + 1]);
        int dy = runs[i + 1] > 0 ? 1 : -1;
        for (int j = 0; j < m; j++)
        {
            result->points[k + 1].x = result->points[k].x;
            result->points[k + 1].y = result->points[k].y + dy;
            k++;
        }
    }
}

static void contour_runs_test_on_image(const uint8_t *image, int width, int height)
{
    int n;
    OmmContour *contours = omm_contours_trace(image, width, height, &n);
    OmmContourRuns **contour_runs = (OmmContourRuns **) malloc(n * sizeof(OmmContourRuns *));
    
    for (int i = 0; i < n; i++)
        contour_runs[i] = omm_contour_runs_new(&contours[i]);
    omm_contours_free(contours, n);
    OmmContour *new_contours = (OmmContour *) malloc(n * sizeof(OmmContour));
    for (int i = 0; i < n; i++)
        omm_contour_runs_unfold(contour_runs[i], &new_contours[i]);

    for (int i = 0; i < n; i++)
        omm_contour_runs_free(contour_runs[i]);
    free(contour_runs);
    uint8_t *new_image = (uint8_t *) malloc(width * height);
    omm_contours_render(new_image, width, height, new_contours, n);
    omm_contours_free(new_contours, n);
    for (int i = 0; i < width * height; i++)
        assert(image[i] == new_image[i]);
    free(new_image);
}

// _________________________________   testing   _____________________________

static void test_on_image(const uint8_t *image, int w, int h)
{
    uint8_t *copy = (uint8_t *) malloc(w * h);
    if (!copy) goto bad;

    int n;
    OmmContour *contours = omm_contours_trace(image, w, h, &n);
    if (!contours)
        goto bad;

    omm_contours_render(copy, w, h, contours, n);
    omm_contours_free(contours, n);

    assert(!memcmp(copy, image, w * h));

    free(copy);
    return;
bad:
    perror("contour tracing test");
    if (copy)
        free(copy);

    exit(1);
}

static void binary_noise(uint8_t *image, int n)
{
    for (int x = 0; x < n; x++)
        image[x] = (uint8_t) (rand() & 1);
}

void omm_contours_test()
{
    const uint8_t sample[] = {
        0, 1, 1, 1,
        1, 1, 0, 1,
        1, 0, 1, 1,
        1, 1, 1, 0
    };
    test_on_image(sample, 4, 4);
    contour_runs_test_on_image(sample, 4, 4);

    const int n_iter = 1000;
    for (int i = 0; i < n_iter; i++)
    {
        int w = 10 + rand() % 5;
        int h = 10 + rand() % 5;
        uint8_t *image = (uint8_t *) malloc(w * h);
        if (!image)
        {
            perror("omm_contours_test");
            exit(1);
        }
        binary_noise(image, w * h);
        test_on_image(image, w, h);
        contour_runs_test_on_image(image, w, h);
        free(image);
    }
}
