/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2014  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */


#ifndef OMM_CONTOURS_FE_H
#define OMM_CONTOURS_FE_H


int *omm_contours_fe(const uint8_t *restrict image, int w, int h, int nx, int ny, int *restrict out_length);


#endif
