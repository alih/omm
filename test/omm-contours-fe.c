/*
 * Ocromancer - an OCR system
 * Copyright (C) 2009-2010  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <limits.h>
#include "omm-contours.h"
#include "omm-features.h"
#include "omm-contours-fe.h"

// XXX: remove duplication!!!

// rose_bin() {{{

/*@ requires x >= 0 && y >= 0;
    requires !(x == y == 0);
    requires x < INT_MAX / 169;
    requires y < INT_MAX / 169;

    ensures 0 <= \result <= 2;
 */
static int rose_bin_quadrant_1(int x, int y)
{
    // we use this approximation: tan(pi/8) = 70./169 * 1.00003...
    return 169 * y <= 70 * x ? 0 :
           169 * x <= 70 * y ? 2 : 1;
}

/*@ requires y >= 0;
    requires !(x == y == 0);
    requires -INT_MAX / 169 < x < INT_MAX / 169;
    requires y < INT_MAX / 169;

    ensures 0 <= \result <= 4;
 */
static int rose_bin_quadrants_1_2(int x, int y)
{
    return x >= 0 ? rose_bin_quadrant_1(x, y) :
                    4 - rose_bin_quadrant_1(-x, y);
}

/*@ requires !(x == y == 0);
    requires -INT_MAX / 169 < x < INT_MAX / 169;
    requires -INT_MAX / 169 < y < INT_MAX / 169;

    ensures 0 <= \result <= 7;
 */
static int rose_bin(int x, int y)
{
    int result;

    if (y >= 0)
        return rose_bin_quadrants_1_2(x, y);

    result = 8 - rose_bin_quadrants_1_2(x, -y);
    return result % 8;
    // return result < 8 ? result : 0;
}

// rose_bin }}}

// the grid {{{

static int cell_index(int z0, int z1, int n_cells, int z_premultiplied)
{
    return (z_premultiplied - n_cells * z0) / (z1 - z0);
}

static int cell_min(int z0, int z1, int n_cells, int i)
{
    return n_cells * z0 + i * (z1 - z0);
}

typedef struct
{
    int x0;
    int x1;
    int nx;
    int y0;
    int y1;
    int ny;
} Grid;

static int grid_x_cell_index(Grid *grid, int x_premultiplied)
{
    return cell_index(grid->x0, grid->x1, grid->nx, x_premultiplied);
}

static int grid_y_cell_index(Grid *grid, int y_premultiplied)
{
    return cell_index(grid->y0, grid->y1, grid->ny, y_premultiplied);
}

static int grid_x_cell_min(Grid *grid, int i)
{
    return cell_min(grid->x0, grid->x1, grid->nx, i);
}

static int grid_y_cell_min(Grid *grid, int i)
{
    return cell_min(grid->y0, grid->y1, grid->ny, i);
}

static void grid_add_horizontal(Grid *grid, int x0, int x1, int y,
                                int *bins)
{
    int *row = bins + omm_dotgrid_cell_index(grid->y0, grid->y1 - grid->y0, grid->ny, y) * grid->nx;

    // coordinate scaling
    x0 *= grid->nx;
    x1 *= grid->nx;
    y *= grid->ny;

    // aspect normalization
    int coef = grid->ny * (grid->y1 - grid->y0);

    int x0i = grid_x_cell_index(grid, x0);
    int x1i = grid_x_cell_index(grid, x1);

    if (x0i == x1i)
        row[x0i] += abs(x1 - x0) * coef;
    else
    {
        int i;
        if (x1 < x0)
        {
            i = x1;
            x1 = x0;
            x0 = i;
            i = x1i;
            x1i = x0i;
            x0i = i;
        }
        row[x0i] += (grid_x_cell_min(grid, x0i + 1) - x0) * coef;
        for (i = x0i + 1; i < x1i; i++)
            row[i] += (grid->x1 - grid->x0) * coef;
        row[x1i] += (x1 - grid_x_cell_min(grid, x1i)) * coef;
    }
}


// TODO: unite grid_add_vertical and grid_add_horizontal?
static void grid_add_vertical(Grid *grid, int x, int y0, int y1,
                                int *bins)
{
    int *col = bins + omm_dotgrid_cell_index(grid->x0, grid->x1 - grid->x0, grid->nx, x);

    // coordinate scaling
    y0 *= grid->ny;
    y1 *= grid->ny;

    // aspect normalization
    int coef = grid->nx * (grid->x1 - grid->x0);

    int y0i = grid_y_cell_index(grid, y0);
    int y1i = grid_y_cell_index(grid, y1);

    if (y0i == y1i)
        col[y0i * grid->nx] += abs(y1 - y0) * coef;
    else
    {
        int i;
        if (y1 < y0)
        {
            i = y1;
            y1 = y0;
            y0 = i;
            i = y1i;
            y1i = y0i;
            y0i = i;
        }
        col[y0i * grid->nx] += (grid_y_cell_min(grid, y0i + 1) - y0) * coef;
        for (i = y0i + 1; i < y1i; i++)
            col[i * grid->nx] += (grid->y1 - grid->y0) * coef;
        col[y1i * grid->nx] += (y1 - grid_y_cell_min(grid, y1i)) * coef;
    }
}

static void grid_add(Grid *grid, int *bins, 
              int x0, int y0, int x1, int y1,
              int dx, int dy)
{
    if (dx == 0 && dy == 0)
        return;

    bins += rose_bin(dx, dy) * grid->nx * grid->ny;
    if (x0 == x1)
        grid_add_vertical(grid, x0, y0, y1, bins);
    else
    {
        assert(y0 == y1);
        grid_add_horizontal(grid, x0, x1, y0, bins);
    }
}

static void grid_init(Grid *grid, int w, int h, int nx, int ny)
{
    grid->x0 = 0;
    grid->x1 = w;
    grid->y0 = 0;
    grid->y1 = h;
    grid->nx = nx;
    grid->ny = ny;
}

/*static void grid_init_from_blob_info(
    Grid *restrict grid, 
    const OmmBlobInfo *restrict blob_info,
    int nx,
    int ny)
{
    grid->x0 = blob_info->min_x;
    grid->x1 = blob_info->max_x + 1;
    grid->y0 = blob_info->min_y;
    grid->y1 = blob_info->max_y + 1;
    grid->nx = nx;
    grid->ny = ny;
}*/

// the grid }}}


// fe using contours {{{

/**
 * Register a run in the feature accumulator.
 * This needs 3 consecutive runs to work, with the middle one being registered.
 * 
 * \param x,y   The coordinates of the corner point between A and B runs.
 */
static void add_contour_run_to_rose(Grid *restrict grid, 
                             int *restrict bins,
                             int ax, int ay,
                             int bx, int by,
                             int cx, int cy,
                             int x, int y)
{
    const int margin = 2;
    const int b_coef = 2;

    assert (ax == 0 || ay == 0);
    assert (bx == 0 || by == 0);
    assert (cx == 0 || cy == 0);
    
    int bb = abs(bx) + abs(by);
    int sx = bx > 0 ?  1 :
             bx < 0 ? -1 : 0;
    int sy = by > 0 ?  1 :
             by < 0 ? -1 : 0;
    int abx = ax + b_coef * bx;
    int aby = ay + b_coef * by;
    int bcx = cx + b_coef * bx; 
    int bcy = cy + b_coef * by;

    if (bb < 2 * margin)
    {
        // there's an area where margins overlap
        int ox = ax + b_coef * bx + cx;
        int oy = ay + b_coef * by + cy;

        if (bb <= margin)
        {
            // the gradient is the same all along
            grid_add(grid, bins, x, y, x + bx, y + by, -oy, ox);
        }
        else
        {
            // margins overlap
            int d = bb - margin;
            grid_add(grid, bins, x, y, x + d * sx, y + d * sy, -aby, abx);
            grid_add(grid, bins, x + d * sx, y + d * sy, 
                     x + margin * sx, y + margin * sy, -oy, ox);
            grid_add(grid, bins, x + margin * sx, y + margin * sy,
                     x + bx, y + by, -bcy, bcx);
        }
    }
    else
    {
        grid_add(grid, bins, x, y, x + margin * sx, y + margin * sy, -aby, abx);
        if (bb > 2 * margin)
        {
            grid_add(grid, bins, x + margin * sx, y + margin * sy, 
                     x + (bb - margin) * sx, y + (bb - margin) * sy,
                     -by, bx);
        }
        grid_add(grid, bins, x + (bb - margin) * sx, y + (bb - margin) * sy,
                                     x + bx, y + by, -bcy, bcx);
    }
}


static void add_contour_runs_to_rose(Grid *restrict grid,
                                     int *restrict bins,
                                     OmmContourRuns *restrict contour_runs)
{
    int n = contour_runs->n;
    int x = contour_runs->x0;
    int y = contour_runs->y0;
    int i;
    for (i = 0; i < n; i++)
    {
        int a = contour_runs->runs[(i + n - 1) % n];
        int b = contour_runs->runs[i];
        int c = contour_runs->runs[(i + 1) % n];
        if (i & 1)
        {
            add_contour_run_to_rose(grid, bins, a, 0, 0, b, c, 0, x, y);
            y += b;
        }
        else
        {
            add_contour_run_to_rose(grid, bins, 0, a, b, 0, 0, c, x, y);
            x += b;
        }
    }
}


int *omm_contours_fe(const uint8_t *restrict image, int w, int h, int nx, int ny, int *restrict length)
{
    const int n_directions = 8;
    *length = nx * ny * n_directions;
    if (!image)
        return NULL;

    int *bins = (int *) calloc(*length, sizeof(int));
    int n_contours;
    OmmContour *contours = omm_contours_trace(image, w, h, &n_contours);

    Grid grid;
    grid_init(&grid, w, h, nx, ny); 

    for (int i = 0; i < n_contours; i++)
    {
        OmmContourRuns *contour_runs = omm_contour_runs_new(&contours[i]);
        add_contour_runs_to_rose(&grid, bins, contour_runs);    
        omm_contour_runs_free(contour_runs);
    }

    omm_contours_free(contours, n_contours);
    return bins;
}

// fe using contours }}}
