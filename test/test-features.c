/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2014  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "omm-features.h"
#include "omm-contours.h"
#include "omm-contours-fe.h"
#include "omm-test-util.h"
#include "omm-test.h"

// ________________________   testing dotgrid   ______________________________

static void dotgrid_count(int a, int w, int n, int *counters)
{
    memset(counters, 0, n * sizeof(*counters));
    int current = 0;
    int count = 0;
    for (int i = 0; i <= w; i++)
    {
        int y = omm_dotgrid_cell_index(a, w, n, a + i);
        assert(y >= current);
        assert(y < n);
        if (y == current)
            count++;
        else
        {
            counters[current] = count;
            current = y;
            count = 1;
        }
    }
    counters[current] = count;
}

static void assert_symmetry(const int *counters, int n)
{
    for (int i = 0; i <= n / 2; i++)
        assert(counters[i] == counters[n - i - 1]);
}

static void assert_uniformness(const int *counters, int n)
{
    for (int i = 0; i <= n / 2; i++)
    {
        int d = counters[i + 1] - counters[i];
        assert(d >= -1 && d <= 1);
    }
}

static void test_dotgrid()
{
    TEST_CASE("features/dotgrid")
        for (int k = 1; k < 5; k++)
        {
            int n = 2 * k + 1;
            for (int w = 1; w < 30; w++)
            {
                int a = rand() % 200;
                int counters[n];
                dotgrid_count(a, w, n, counters);
                assert_symmetry(counters, n);
                assert_uniformness(counters, n);
            }
        }
    MUST_WORK;
}

// ___________________________________________________________________________

static void check_fe_glyph(const uint8_t *image, int w, int h)
{
    int len;
    int *contour_features = omm_contours_fe(image, w, h, 3, 3, &len);
    assert(len == omm_feature_length() - 1);
    
    int features[omm_feature_length()];
    OmmImage *rle = omm_image_new_from_raster(image, w, h);
    omm_extract_features_from_glyph(rle, features);
    assert(!memcmp(features + 1, contour_features, len * sizeof(int)));

    omm_image_free(rle);
    free(contour_features);
}

static void flip_1d(const uint8_t *src, int n, uint8_t *dst)
{
    for (int i = 0; i < n; i++)
        dst[i] = src[n - i - 1];
}

static void flip_x(const uint8_t *src, int w, int h, uint8_t *dst)
{
    for (int y = 0; y < h; y++)
        flip_1d(src + y * w, w, dst + y * w);
}

static void flip_y(const uint8_t *src, int w, int h, uint8_t *dst)
{
    for (int y = 0; y < h; y++)
        memcpy(dst + y * w, src + (h - 1 - y) * w, w);
}

static void flip_xy(const uint8_t *src, int w, int h, uint8_t *dst)
{
    for (int y = 0; y < h; y++)
        flip_1d(src + (h - 1 - y) * w, w, dst + y * w);
}

static void transpose(const uint8_t *src, int src_w, int src_h, uint8_t *dst)
{
    int dst_w = src_h;
    int dst_h = src_w;
    for (int y = 0; y < dst_h; y++)
        for (int x = 0; x < dst_w; x++)
            dst[y * dst_w + x] = src[x * src_w + y];
}

static void rotate_cw(const uint8_t *src, int src_w, int src_h, uint8_t *dst)
{
    uint8_t tmp[src_w * src_h];
    transpose(src, src_w, src_h, tmp);
    flip_x(tmp, src_h, src_w, dst);
}

static void rotate_ccw(const uint8_t *src, int src_w, int src_h, uint8_t *dst)
{
    uint8_t tmp[src_w * src_h];
    transpose(src, src_w, src_h, tmp);
    flip_y(tmp, src_h, src_w, dst);
}

static void check_feature_ops(const uint8_t *image, int w, int h)
{
    int n = omm_feature_length();
    int features[n];
    OmmImage *rle = omm_image_new_from_raster(image, w, h);
    omm_extract_features_from_glyph(rle, features);
    omm_image_free(rle);

    uint8_t new_image[w * h];
    flip_x(image, w, h, new_image);
    int new_features[n];
    rle = omm_image_new_from_raster(new_image, w, h);
    omm_extract_features_from_glyph(rle, new_features);
    omm_image_free(rle);
    
    int flipped_features[n];
    omm_features_flip_x(features, flipped_features);
    /*for (int i = 1; i < n; i++)
        printf("%c%c %d %d %d\n", 'a' + (i-1) / 9, "789456123"[(i-1)%9], features[i], flipped_features[i], new_features[i]); */
    assert(!memcmp(flipped_features, new_features, n));

    // TODO: XXX
    flip_y(image, w, h, new_image);
    rle = omm_image_new_from_raster(new_image, w, h);
    omm_extract_features_from_glyph(rle, new_features);
    omm_image_free(rle);
    omm_features_flip_y(features, flipped_features);
    assert(!memcmp(flipped_features, new_features, n));

    flip_xy(image, w, h, new_image);
    rle = omm_image_new_from_raster(new_image, w, h);
    omm_extract_features_from_glyph(rle, new_features);
    omm_image_free(rle);
    omm_features_flip_xy(features, flipped_features);
    assert(!memcmp(flipped_features, new_features, n));

    transpose(image, w, h, new_image);
    rle = omm_image_new_from_raster(new_image, h, w);
    omm_extract_features_from_glyph(rle, new_features);
    omm_image_free(rle);
    omm_features_transpose(features, flipped_features);
    assert(!memcmp(flipped_features, new_features, n));

    rotate_cw(image, w, h, new_image);
    rle = omm_image_new_from_raster(new_image, h, w);
    omm_extract_features_from_glyph(rle, new_features);
    omm_image_free(rle);
    omm_features_rotate_cw(features, flipped_features);
    assert(!memcmp(flipped_features, new_features, n));

    rotate_ccw(image, w, h, new_image);
    rle = omm_image_new_from_raster(new_image, h, w);
    omm_extract_features_from_glyph(rle, new_features);
    omm_image_free(rle);
    omm_features_rotate_ccw(features, flipped_features);
    assert(!memcmp(flipped_features, new_features, n));
}

int main()
{
    test_dotgrid();

    TEST_CASE("features/extraction")
        run_on_all_images(check_fe_glyph, 1, 4, 1, 3);
        run_on_random_images(check_fe_glyph, 10, 10, 10, 10, 1000);
    MUST_WORK;

    TEST_CASE("features/ops")
        run_on_all_images(check_feature_ops, 1, 4, 1, 3);
        run_on_random_images(check_feature_ops, 8, 8, 10, 10, 1000);
    MUST_WORK;

    return EXIT_SUCCESS;
}
