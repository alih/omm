/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

/**
 * \file test-test.c
 *
 * This file includes tests for the test framework itself.
 * The tests are copied from the documentation.
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "omm-test.h"

int main()
{
    int a = 1;
    TEST_CASE("changing a local variable")
        a = 2;
    MUST_WORK;
    TEST_CASE("change isn't visible here")
        assert(a == 1);
    MUST_WORK;

    TEST_CASE("test for failure")
        fprintf(stderr, "Oh no!\n");
        exit(EXIT_FAILURE);
    MUST_FAIL("Oh no!\n");

    TEST_CASE("division by 0")
        printf("%d", 1 / abs(0)); // abs() to prevent compiler warnings
    MUST_TERMINATE(SIGFPE);

    return 0;
}
