/*
 * OMM - an OCR that sacrifices accuracy for high speed
 * Copyright (C) 2011-2012  Ilya Mezhirov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */



#ifndef OMM_CC_FLOOD_H
#define OMM_CC_FLOOD_H

/** \file omm-cc-flood.h
 *
 * A simple implementation of connected component labeling using flood fill.
 * This algorithm is intended to be easy to understand rather than efficient.
 */

#include <stdint.h>

/**
 * Label connected components.
 * \param[out]  result      Preallocated width x height array of integers.
 *                          On return, the background is set to UINT32_MAX.
 * \returns                 The number of connected components.
 */
uint32_t omm_cc_flood(
    uint32_t *restrict result, 
    const uint8_t *restrict pixels,
    int width, 
    int height);

/**
 * Same as omm_cc_flood(), but lower level.
 * Namely, the background pixels in the result are not initialized
 *     and the pixels array is zeroed as a side effect.
 */
uint32_t omm_cc_flood_raw(
    uint32_t *restrict result, 
    uint8_t *restrict pixels,
    int width, 
    int height);

#endif
