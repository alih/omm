VERSION = '0.2'
APPNAME = 'omm'

top = '.'
out = 'build'

########################################################################

def options(opt):
    opt.load('compiler_c waf_unit_test')

def configure(conf):
    release_env = conf.env
    conf.load('compiler_c waf_unit_test')
    conf.check(header_name='tiffio.h', uselib_store='TIFF')
    conf.check(lib='tiff')
    conf.check(lib='m', uselib_store='M')

    conf.env.append_value('CFLAGS', ['-std=c99', '-pedantic-errors',
        '-D_POSIX_SOURCE',
        '-Wall', '-Wextra', '-Wshadow', '-Waggregate-return', 
        '-Wcast-align', '-Wmissing-prototypes', '-Wpointer-arith', 
        '-Wredundant-decls', '-Wcast-qual', '-Wmissing-declarations'])

    conf.setenv('debug', release_env)
    conf.env.append_value('CFLAGS', ['-g'])

    conf.setenv('coverage', release_env)
    conf.env.append_value('CFLAGS', ['-g', '--coverage'])
    conf.env.append_value('LINKFLAGS', '--coverage')
    
    release_env.append_value('CFLAGS', ['-O3', '-DNDEBUG']) # '-finline-limit=10000', 

# taken from the wafbook
from waflib.Build import BuildContext, CleanContext, \
                         InstallContext, UninstallContext
for x in 'debug coverage'.split():
    for y in (BuildContext, CleanContext, InstallContext, UninstallContext):
        name = y.__name__.replace('Context','').lower()
        class tmp(y): 
            if y == BuildContext:
                cmd = x
            else:
                cmd = name + '_' + x
            variant = x

def build(bld):
    bld.stlib(target = 'omm',
              export_includes = 'core',
              use = 'TIFF',
              source = bld.path.ant_glob('core/omm-*.c'))

    bld.objects(target = 'cli',
                use = 'omm',
                export_includes = 'cli',
                source = bld.path.ant_glob('cli/omm-*.c'))

    bld.program(target = '#/omm', 
                use = 'cli', 
                source = 'cli/main-omm.c')

    bld.objects(target = 'train',
                use = 'omm M',
                source = bld.path.ant_glob('train/omm-*.c'))

    for source in bld.path.ant_glob('train/main-*.c'):
        bld.program(target = source.name[5:-2], 
                    use = 'cli train', 
                    source = [source])

    if bld.variant == 'debug' or bld.variant == 'coverage':
        bld.objects(target = 'test_obj',
                    use = 'omm cli',
                    source = bld.path.ant_glob('test/omm-*.c'))
        from waflib.Tools import waf_unit_test
        bld.add_post_fun(waf_unit_test.summary)
        for source in bld.path.ant_glob('test/test-*.c'):
            bld.program(features='test',
                        target = source.name[:-2],
                        use = 'omm cli test_obj TIFF',
                        source =[source])

        

# vim: set ft=python:
